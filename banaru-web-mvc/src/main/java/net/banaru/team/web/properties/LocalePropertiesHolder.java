package net.banaru.team.web.properties;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LocalePropertiesHolder {

    @Value("${banaru.locale.default-language:ja}")
    private Locale defaultLanguage;

    @Value("${banaru.locale.supported-languages:ja}")
    private List<Locale> supportedLanguages = new ArrayList<>();

    @Value("${banaru.locale.default-country-code:JP}")
    private String defaultCountryCode;

    @Value("${banaru.locale.supported-country-codes:JP}")
    private List<String> supportedCountryCodes = new ArrayList<>();

    @Value("${banaru.locale.time-zone:Asia/Tokyo}")
    private TimeZone timeZone;

    public Locale getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(Locale defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public List<Locale> getSupportedLanguages() {
        if (!supportedLanguages.contains(defaultLanguage)) {
            supportedLanguages.add(defaultLanguage);
        }
        return supportedLanguages;
    }

    public void setSupportedLanguages(List<String> supportedLanguages) {
        this.supportedLanguages = supportedLanguages
            .stream().map(Locale::new).collect(Collectors.toList());
    }

    public String getDefaultCountryCode() {
        return defaultCountryCode;
    }

    public void setDefaultCountryCode(String defaultCountryCode) {
        this.defaultCountryCode = defaultCountryCode;
    }

    public List<String> getSupportedCountryCodes() {
        return supportedCountryCodes;
    }

    public void setSupportedCountryCodes(List<String> supportedCountryCodes) {
        this.supportedCountryCodes = supportedCountryCodes;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

}
