package net.banaru.team.web.configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

import org.h2.server.web.WebServlet;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.context.ServletContextAware;

@Profile({ "h2embedded", "h2server" })
@Configuration
public class H2ConsoleConfiguration implements ServletContextAware {

    @Override
    public void setServletContext(ServletContext servletContext) {
        ServletRegistration.Dynamic registration = servletContext.addServlet("H2Console", new WebServlet());
        registration.setLoadOnStartup(1);
        registration.addMapping("/console/*");
    }

}
