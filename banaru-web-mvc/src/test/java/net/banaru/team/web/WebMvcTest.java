package net.banaru.team.web;

import java.util.Collections;
import java.util.Locale;
import java.util.TimeZone;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import net.banaru.team.web.properties.LocalePropertiesHolder;

@RunWith(SpringRunner.class)
// using the default value of "file:src/main/webapp" for the path to the root off the web application
// https://docs.spring.io/spring/docs/5.1.3.RELEASE/spring-framework-reference/testing.html#spring-testing-annotation-webappconfiguration
@WebAppConfiguration
@ContextConfiguration({
    "classpath*:/spring/test-properties-context.xml"
})
public class WebMvcTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

//    # Default Locale Properties
//    banaru.locale.default-language = ja
//    banaru.locale.supported-languages = ja
//    banaru.locale.default-country-code = JP
//    banaru.locale.supported-country-codes = JP
//    banaru.locale.time-zone = Asia/Tokyo

    @Autowired
    private LocalePropertiesHolder localePropertiesHolder;

    @Test
    public void LocalePropertiesHolderTest() {

        logger.debug("Default Language: {}", localePropertiesHolder.getDefaultLanguage());
        Assert.assertEquals(Locale.JAPANESE, localePropertiesHolder.getDefaultLanguage());

        logger.debug("Supported Languages: {}", localePropertiesHolder.getSupportedLanguages());
        localePropertiesHolder.getSupportedLanguages().forEach(language -> {
            Assert.assertTrue(Collections.singletonList(Locale.JAPANESE).contains(language));
        });

        logger.debug("Default CountryCode: {}", localePropertiesHolder.getDefaultCountryCode());
        Assert.assertEquals(Locale.JAPAN.getCountry(), localePropertiesHolder.getDefaultCountryCode());

        logger.debug("Supported CountryCodes: {}", localePropertiesHolder.getSupportedCountryCodes());
        localePropertiesHolder.getSupportedCountryCodes().forEach(countryCode -> {
            Assert.assertTrue(Collections.singletonList(Locale.JAPAN.getCountry()).contains(countryCode));
        });

        logger.debug("Time Zone: {}", localePropertiesHolder.getTimeZone());
        Assert.assertEquals(TimeZone.getTimeZone("Asia/Tokyo"), localePropertiesHolder.getTimeZone());
    }

}
