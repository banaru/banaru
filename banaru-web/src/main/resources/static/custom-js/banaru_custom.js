// --------------------------------------
// When the page starts loading
// --------------------------------------
$(function() {
  // console.log('test');
});

//--------------------------------------
//When the page loading ended
//--------------------------------------
$(window).on("load", function() {
  initParsleyForms();
});

var BUtils = {
  // e.g. BUtils.injectValidation($('#form'), ${form.constraintJson});
  injectValidation : function(formElement, constraintJson) {
    // var keys = Object.keys(constraintJson);
    // array를 for하면 index가 나오고, json을 for하면 key가 나온다.
    // Inject constraint
    for (var propertyName in constraintJson) {
      var inputs = formElement.find('input[name="' + propertyName + '"], select[name="' + propertyName + '"]');
      if (inputs.length > 0) {
        var constraints = constraintJson[propertyName];
        for (var constraint in constraints) {
          inputs.attr(constraint, constraints[constraint]);
        }
      }
    }

    var parsleyInstance = initParsleyInstance(formElement);

    // Clear each error message when input
    var inputs = formElement.find('input');
    inputs.each(function() {
      var inputElement = $(this);
      inputElement.keyup(function(){
        clearFormError(formElement, inputElement);
      });
    });

    // Clear all error messages when validate
    parsleyInstance.on('form:validate', function(){
      inputs.each(function() {
        clearFormError(formElement, $(this));
      });
    });

  }
};

//--------------------------------------
// Clear server errors
//--------------------------------------
function clearFormError(formElement, inputElement){
  var inputId = inputElement.attr('id');
  var errorElement = formElement.find('div[data-server-errors="' + inputId + '"]');
  if (errorElement.length) {
    console.info('Clear error field ID: ' + inputId);
    errorElement.remove();
  }
}

// --------------------------------------
// Initialize parsley form
// --------------------------------------
function initParsleyForms() {
  var parsleyForms = $('form');
  if (parsleyForms.length < 1) {
    return;
  }
  initParsleyInstance(parsleyForms);
}

function initParsleyInstance(element) {
  var parsleyConfig = {
    errorsContainer : function(parsleyField) {
      var fieldId = parsleyField.$element.attr('id');
      var form = parsleyField.$element.closest("form");
      return form.find('.' + fieldId + '-errors');
    }
  };
  return element.parsley(parsleyConfig);
}
