function initDateSelect(yearId, monthId, dayId) {
  var firstItem = '<option value="" selected>--</option>';

  var yearElement = getDateSelectElement(yearId);
  yearElement.html(firstItem);
  var today = new Date();
  for (var i = today.getFullYear(); i >= today.getFullYear() - 100; i--) {
    yearElement.append('<option value="' + i + '">' + i + '</option>');
  }

  var monthElement = getDateSelectElement(monthId);
  monthElement.html(firstItem);
  for (var i = 1; i <= 12; i++) {
    monthElement.append('<option value="' + i + '">' + i + '</option>');
  }

  var dayElement = getDateSelectElement(dayId);
  dayElement.html(firstItem);
  for (var i = 1; i <= 31; i++) {
    dayElement.append('<option value="' + i + '">' + i + '</option>');
  }
}

function selectDate(selectElementId, value){
  if(value === null || value.length <= 0){
    return;
  }
  var intValue = parseInt(value);
  if(isNaN(intValue)){
    return;
  }
  getDateSelectElement(selectElementId).val(value);
}

function getDateSelectElement (elementId){
  return $('select[id="' + elementId + '"]');
}
