package net.banaru.team.web.configuration.security;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import net.banaru.team.service.dto.member.MemberDto;

import java.util.List;

/**
 * @author banssolin
 */
@SuppressWarnings("unused")
public class UserDetailsImpl implements UserDetails {

    private static final long serialVersionUID = 2655164079012631785L;

    private final Long memberNo;
    private final String username;
    private final String password;
    private List<SimpleGrantedAuthority> authorities;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;

    public UserDetailsImpl(MemberDto member, List<SimpleGrantedAuthority> authorities) {
        this.memberNo = member.getMemberNo();
        this.username = member.getMemberId();
        this.password = member.getPassword();
        this.authorities = authorities;
    }

    public Long getMemberNo() {
        return memberNo;
    }

    @Override
    public List<SimpleGrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return true;
    }

}
