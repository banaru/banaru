package net.banaru.team.web.configuration.security;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import net.banaru.team.service.dto.member.MemberDto;
import net.banaru.team.service.member.MemberService;

/**
 * @author banssolin
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MemberService memberService;

    @Override
    public UserDetails loadUserByUsername(String memberId) throws UsernameNotFoundException {

        if (StringUtils.isEmpty(memberId)) {
            throw new UsernameNotFoundException("Member ID is empty.");
        }

        logger.debug("Try login by memberId: {}", memberId);

        MemberDto member = memberService.getMember(memberId);
        if (member == null) {
            throw new UsernameNotFoundException("Not Found Member - memberId :" + memberId);
        }
        logger.debug("Success, member: {}", member.toJson());

        List<SimpleGrantedAuthority> authorities;
        {
            SimpleGrantedAuthority adminAuth = new SimpleGrantedAuthority(UserRole.ADMIN.getRole());
            SimpleGrantedAuthority memberAuth = new SimpleGrantedAuthority(UserRole.MEMBER.getRole());

            switch (member.getMemberGrade()) {
                case ADMIN:
                    authorities = Arrays.asList(adminAuth, memberAuth);
                    break;
                default:
                    authorities = Arrays.asList(memberAuth);
                    break;
            }
        }

        return new UserDetailsImpl(member, authorities);
    }

}
