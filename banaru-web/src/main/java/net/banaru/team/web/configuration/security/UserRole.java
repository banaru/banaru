package net.banaru.team.web.configuration.security;

/**
 * @author banssolin
 *
 */
public enum UserRole {

    ADMIN("ROLE_ADMIN"),

    MEMBER("ROLE_MEMBER");

    private String role;

    UserRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
