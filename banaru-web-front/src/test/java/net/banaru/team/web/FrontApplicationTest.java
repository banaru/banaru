package net.banaru.team.web;

import java.util.Locale;
import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

import net.banaru.team.logger.LoggerInjector;
import net.banaru.team.utils.JsonUtils;
import net.banaru.team.web.properties.JspViewProperties;
import net.banaru.team.web.properties.LocaleProperties;
import net.banaru.team.web.properties.MessageProperties;
import net.banaru.team.web.properties.MvcProperties;
import net.banaru.team.web.sample.configprop.SampleProperties;
import net.banaru.team.web.utils.MessageUtils;

/**
 * @author banssolin
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
    FrontApplication.class
})
public class FrontApplicationTest {

    // ============================================
    // Bean test
    // ============================================

    @Autowired
    private ApplicationContext applicationContext;

    @LoggerInjector
    private Logger logger;

    @Test
    public void applicationContextTest() throws Exception {
        // String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        // for (String beanName : beanDefinitionNames) {
        // logger.debug("Banaru bean name: {}", beanName);
        // }
        Object loggerPostProcessor = applicationContext.getBean("loggerPostProcessor");
        Assert.assertNotNull("LoggerPostProcessor is null.", loggerPostProcessor);
        Assert.assertNotNull("Injected logger is null.", logger);
    }

    // ============================================
    // Configuration properties test
    // ============================================

    @Autowired
    private SampleProperties sampleProperties;

    @Autowired
    private MessageProperties messageProperties;

    @Autowired
    private LocaleProperties localeProperties;

    @Test
    public void configurationPropertiesTest() throws Exception {
        Assert.assertNotNull("SampleProperties is null.", sampleProperties);
        logger.debug("SampleProperties: {}", JsonUtils.toPrettyJson(sampleProperties));

        // message
        Assert.assertNotNull("MessageProperties is null.", messageProperties);
        logger.debug("MessageProperties: {}", JsonUtils.toPrettyJson(messageProperties));

        // locale
        Assert.assertNotNull("LocaleProperties is null.", localeProperties);
        logger.debug("LocaleProperties: {}", JsonUtils.toPrettyJson(localeProperties));
    }

    @Autowired
    private MvcProperties mvcProperties;

    @Test
    public void getFrontPropertiesTest() throws Exception {
        Assert.assertNotNull("FrontProperties is null.", mvcProperties);
        Assert.assertTrue("Error path is empty", !StringUtils.isEmpty(mvcProperties.getErrorTemplatePath()));
        logger.debug("Error path: {}", mvcProperties.getErrorTemplatePath());

        Assert.assertNotNull("ViewProperties is null.", mvcProperties.getJspView());
        JspViewProperties viewProperties = mvcProperties.getJspView();
        Assert.assertTrue("Prefix is empty", !StringUtils.isEmpty(viewProperties.getPrefix()));
        Assert.assertTrue("Suffix is empty", !StringUtils.isEmpty(viewProperties.getSuffix()));
        logger.debug("View prefix: {}", viewProperties.getPrefix());
        logger.debug("View suffix: {}", viewProperties.getSuffix());
    }

    // ============================================
    // Message source test
    // ============================================

    @Autowired
    private MessageSource messageSource;

    @Test
    public void messageSourceTest() {
        Assert.assertNotNull(messageSource);

        Locale systemLocale = LocaleContextHolder.getLocale();
        String systemLanguage = systemLocale.getLanguage();
        logger.debug("system language: {}", systemLanguage);

        // "message_언어" 파일이 없을경우 message.properties 파일이 불림
        // String defaultMessage = messageSource.getMessage("message.test", null, null);
        // logger.debug(defaultMessage);

        String enMessage = messageSource.getMessage("message.test", null, Locale.ENGLISH);
        logger.debug("en language message: {}", enMessage);
        Assert.assertTrue(enMessage.startsWith("en"));

        String jaMessage = messageSource.getMessage("message.test", null, Locale.JAPANESE);
        logger.debug("ja language message: {}", jaMessage);
        Assert.assertTrue(jaMessage.startsWith("ja"));

        // 현재 뷰 언어, 브라우저로 열면 default locale 이 보여진다. 뷰가 없으므로 시스템언어가 나옴
        String currentLanguage = messageSource.getMessage("message.test", null,
            LocaleContextHolder.getLocale());
        logger.debug("current language message: {}", currentLanguage);
        Assert.assertTrue(currentLanguage.startsWith(systemLanguage));

        // 없는 언어. 디폴트 언어로 나옴
        String notExistLanguage = messageSource.getMessage("message.test", null,
            Locale.FRENCH);
        logger.debug("Not exist language message: {}", notExistLanguage);
        Assert.assertTrue(notExistLanguage.startsWith(systemLanguage));
    }

    // ============================================
    // ETC test
    // ============================================

    @Autowired
    private ServerProperties serverProperties;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private MessageUtils messageUtils;

    @Test
    public void serverPropertiesTest() throws Exception {
        logger.debug(serverProperties.getPort().toString());
        logger.debug(dataSource.getConnection().getMetaData().getURL());
        logger.debug(dataSource.getConnection().getMetaData().getDriverName());

        Assert.assertNotNull(messageUtils);
        logger.debug(messageUtils.getMessage("not.exist.message.key"));
    }

}
