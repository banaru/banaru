package net.banaru.team.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ma.glasnost.orika.MapperFacade;
import net.banaru.team.web.dto.DateForm;
import net.banaru.team.web.dto.MemberRegisterForm;
import net.banaru.team.web.dto.MemberUpdateForm;
import net.banaru.team.web.dto.NameForm;
import net.banaru.team.service.dto.NameDto;
import net.banaru.team.service.dto.member.MemberRegisterDto;
import net.banaru.team.service.dto.member.MemberUpdateDto;
import net.banaru.team.service.enums.Gender;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author banssolin
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
    FrontApplication.class
})
public class FrontMapperTest {

    @Autowired
    private MapperFacade mapperFacade;

    private static final String TEST_NAME1 = "TEST_NAME1";
    private static final String TEST_NAME2 = "TEST_NAME2";
    private static final String TEST_NAME3 = "TEST_NAME3";
    private static final String TEST_NAME4 = "TEST_NAME4";

    private static final int TEST_YEAR = 2019;
    private static final int TEST_MONTH = 2;
    private static final int TEST_DAY = 10;

    // ==============================
    // NameForm to NameDto
    // ==============================
    @Test
    public void nameFormToDto() {
        NameDto nameDto = mapperFacade.map(buildDummyNameForm(), NameDto.class);
        assertNameDto(nameDto);

        NameForm nameForm = mapperFacade.map(nameDto, NameForm.class);
        assertNameForm(nameForm);
    }

    private NameForm buildDummyNameForm() {
        NameForm nameForm = new NameForm();
        nameForm.setName1(TEST_NAME1);
        nameForm.setName2(TEST_NAME2);
        nameForm.setName3(TEST_NAME3);
        nameForm.setName4(TEST_NAME4);
        return nameForm;
    }

    private void assertNameDto(NameDto nameDto) {
        Assert.assertEquals(TEST_NAME1, nameDto.getName1());
        Assert.assertEquals(TEST_NAME2, nameDto.getName2());
        Assert.assertEquals(TEST_NAME3, nameDto.getName3());
        Assert.assertEquals(TEST_NAME4, nameDto.getName4());
    }

    private void assertNameForm(NameForm nameForm) {
        Assert.assertEquals(TEST_NAME1, nameForm.getName1());
        Assert.assertEquals(TEST_NAME2, nameForm.getName2());
        Assert.assertEquals(TEST_NAME3, nameForm.getName3());
        Assert.assertEquals(TEST_NAME4, nameForm.getName4());
    }

    // ==============================
    // DateForm to LocalDate
    // ==============================
    @Test
    public void dateFormToLocalDate() {
        LocalDate localDate = mapperFacade.map(buildDummyDateForm(), LocalDate.class);
        assertLocalDate(localDate);

        DateForm dateForm = mapperFacade.map(localDate, DateForm.class);
        assertDateForm(dateForm);
    }

    private DateForm buildDummyDateForm() {
        DateForm dateForm = new DateForm();
        dateForm.setYear(TEST_YEAR);
        dateForm.setMonth(TEST_MONTH);
        dateForm.setDay(TEST_DAY);
        return dateForm;
    }

    private void assertLocalDate(LocalDate localDate) {
        Assert.assertEquals(TEST_YEAR, localDate.getYear());
        Assert.assertEquals(TEST_MONTH, localDate.getMonthValue());
        Assert.assertEquals(TEST_DAY, localDate.getDayOfMonth());
    }

    private void assertDateForm(DateForm dateForm) {
        Assert.assertEquals(TEST_YEAR, dateForm.getYear().intValue());
        Assert.assertEquals(TEST_MONTH, dateForm.getMonth().intValue());
        Assert.assertEquals(TEST_DAY, dateForm.getDay().intValue());
    }

    // ==============================
    // MemberRegisterForm to MemberRegisterDto
    // ==============================
    @Test
    public void memberRegisterFormToDto() {

        final String memberId = "testtest";
        final String password = "test123!";
        final String confirmPassword = "test123!";
        final String memberCountryCode = "US";
        final String email = "member@banaru.net";
        final Gender gender = Gender.MALE;
        final String phoneNo = "198989898";

        MemberRegisterForm registerForm = new MemberRegisterForm();
        registerForm.setMemberId(memberId);
        registerForm.setPassword(password);
        registerForm.setConfirmPassword(confirmPassword);
        registerForm.setMemberCountryCode(memberCountryCode);
        registerForm.setName(buildDummyNameForm());
        registerForm.setEmail(email);
        registerForm.setBirthday(buildDummyDateForm());
        registerForm.setGender(gender);
        registerForm.setPhoneNo(phoneNo);

        MemberRegisterDto dto;
        {
            dto = mapperFacade.map(registerForm, MemberRegisterDto.class);
            assertNameDto(dto.getName());
            assertLocalDate(dto.getBirthday());

            Assert.assertEquals(memberId, dto.getMemberId());
            Assert.assertEquals(password, dto.getPassword());
            Assert.assertEquals(memberCountryCode, dto.getMemberCountryCode());
            Assert.assertEquals(email, dto.getEmail());
            Assert.assertEquals(gender, dto.getGender());
            Assert.assertEquals(phoneNo, dto.getPhoneNo());
        }

        MemberRegisterForm form;
        {
            form = mapperFacade.map(dto, MemberRegisterForm.class);
            assertNameForm(form.getName());
            assertDateForm(form.getBirthday());

            Assert.assertEquals(memberId, form.getMemberId());
            Assert.assertEquals(password, form.getPassword());
            Assert.assertEquals(memberCountryCode, form.getMemberCountryCode());
            Assert.assertEquals(email, form.getEmail());
            Assert.assertEquals(gender, form.getGender());
            Assert.assertEquals(phoneNo, form.getPhoneNo());
        }
    }

    // ==============================
    // MemberUpdateForm to MemberUpdateDto
    // ==============================
    @Test
    public void memberUpdateFormToDto() {

        final String id = "testtest";
        final String password = "test123!";
        final String confirmPassword = "test123!";
        final String memberCountryCode = "US";
        final String email = "member@banaru.net";
        final Gender gender = Gender.MALE;
        final String phoneNo = "198989898";

        MemberUpdateForm updateForm = new MemberUpdateForm();
        updateForm.setMemberCountryCode("US");
        updateForm.setName(buildDummyNameForm());
        updateForm.setEmail("member@banaru.net");
        updateForm.setBirthday(buildDummyDateForm());
        updateForm.setGender(Gender.MALE);
        updateForm.setPhoneNo("198989898");

        MemberUpdateDto dto;
        {
            dto = mapperFacade.map(updateForm, MemberUpdateDto.class);
            assertNameDto(dto.getName());
            assertLocalDate(dto.getBirthday());

            Assert.assertEquals(memberCountryCode, dto.getMemberCountryCode());
            Assert.assertEquals(email, dto.getEmail());
            Assert.assertEquals(gender, dto.getGender());
            Assert.assertEquals(phoneNo, dto.getPhoneNo());
        }

        MemberUpdateForm form;
        {
            form = mapperFacade.map(dto, MemberUpdateForm.class);
            assertNameForm(form.getName());
            assertDateForm(form.getBirthday());

            Assert.assertEquals(memberCountryCode, form.getMemberCountryCode());
            Assert.assertEquals(email, form.getEmail());
            Assert.assertEquals(gender, form.getGender());
            Assert.assertEquals(phoneNo, form.getPhoneNo());
        }
    }

}
