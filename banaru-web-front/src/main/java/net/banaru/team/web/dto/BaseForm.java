package net.banaru.team.web.dto;

import java.util.Map;

import net.banaru.team.web.constraint.ConstraintInfo;
import net.banaru.team.web.constraint.ConstraintUtils;
import net.banaru.team.service.dto.BaseDto;
import net.banaru.team.utils.JsonUtils;

/**
 * @author banssolin
 */
public abstract class BaseForm implements BaseDto {

    private static final long serialVersionUID = 1L;

    public Map<String, ConstraintInfo> getConstraintMap() {
        return ConstraintUtils.getFieldConstraintMap(getClass());
    }

    public String getConstraintJson() {
        return JsonUtils.toJson(getConstraintMap(), false);
    }

}
