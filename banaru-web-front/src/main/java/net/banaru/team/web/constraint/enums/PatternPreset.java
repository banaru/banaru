package net.banaru.team.web.constraint.enums;

/**
 * @author banssolin
 */
public enum PatternPreset {

    // 역슬러시가 두 개인 이유: 정규 표현식에서 "\"는 다음에 오는 문자가 특수 문자라는 걸 나타내기 위해서 입력. 나머지 역슬러시는 자바 String 에서 역슬러시가 역슬러시라는
    // 특수문자를 나타내기 위해서 입력. 결론은 특수 문자라는 것을 나타내기 위해(정규표현식) 넣은 특수문자를 특수문자라는 것을 나타내기 위해(자바문법) 입력.
    // 위치 관계 없이, "+", "-", "정수"를 허용

    // 정수, 알파벳 소대문자, 특수기호, !, @, #, $, %, ^, ~, * (이 밖의 &와 같은 특수 문자는 submit 시에 자동으로 인코딩 되버림, html? servlet? 기본 사양)

    ALL(null, ".*"),

    SPACE("banaru.preset.space", "[ ]"),

    DIGIT("banaru.preset.digit", "[\\d]"),

    ALPHABET_UPPER("banaru.preset.alphabet.upper", "[A-Z]"),
    ALPHABET_LOWER("banaru.preset.alphabet.lower", "[a-z]"),

    // Hiragana - java regexp: "\p{InHiragana}", Javascript regexp: "\u3040-\u309F"
    HIRAGANA("banaru.preset.hiragana", "[\\u3040-\\u309F]"),

    // Katakana - java regexp: "\p{InKatakana}", Javascript regexp: "\u30A0-\u30FF"
    KATAKANA("banaru.preset.katakana", "[\\u30A0-\\u30FF]"),

    // CJK Unified Ideographs: 한중일 통합 한자
    // Chinese characters - java regexp: "々\p{InCJKUnifiedIdeographs}", Javascript regexp: "\u4E00-\u9FFF"
    CHINESE_CHARACTERS("banaru.preset.cjk_unified_ideographs", "[\\u4E00-\\u9FFF]"),

    PASSWORD("banaru.preset.password", "(?=.*\\d)(?=.*[a-zA-Z])(?=.*[!@#$%^~*])."),

    PHONE_NUMBER("banaru.preset.phone.number", "[\\+0-9\\-]");

    // kanji: 々\\p{InCJKUnifiedIdeographs}

    // public static final String KANJI_CODES = "々\\p{InCJKUnifiedIdeographs}";
    // public static final String HIRAGANA_CODES = "\\p{InHiragana}";
    // public static final String KATAKANA_CODES = "\\p{InKatakana}";
    // public static final String KATAKANA_HANKAKU_CODES = "ｧ-ﾝﾞﾟ";
    // public static final String NUMBER_CODES = "0-9";
    // public static final String NUMBER_ZENKAKU_CODES = "０-９";
    // public static final String ALPHABET_CODES = "A-Za-z";
    // public static final String ALPHABET_ZENKAKU_CODES = "Ａ-Ｚａ-ｚ";

    // HANGUL_SYLLABLES("\\p{InHangul Syllables}", "\\\\uAC00-\\\\uD7AF"),
    // HIRAGANA("\\p{InHiragana}", "\\\\u3040-\\\\u309F"),
    // INCJK_UNIFIED_IDEOGRAPHS("\\p{InCJK Unified Ideographs}", "\\\\u4E00-\\\\u9FFF"),
    // KATANA("\\p{InKatakana}", "\\\\u30A0-\\\\u30FF"),
    // INHALFWIDTH_AND_FULLWIDTH_FORMS("\\p{InHalfwidth and Fullwidth Forms}", "\\\\uFF00-\\\\uFFEF");

    private final String nameKey;
    private final String regexp;

    PatternPreset(String nameKey, String regexp) {
        this.nameKey = nameKey;
        this.regexp = regexp;
    }

    public String getNameKey() {
        return nameKey;
    }

    public String getRegexp() {
        return regexp;
    }

}
