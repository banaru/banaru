package net.banaru.team.web.dto;

import net.banaru.team.web.constraint.annotation.BPattern;
import net.banaru.team.web.constraint.enums.BPatterns;

import javax.validation.constraints.NotBlank;

public class PasswordChangeForm extends BaseForm {

    private static final long serialVersionUID = 383732254221226975L;

    @NotBlank
    private String password;

    @BPattern(BPatterns.PASSWORD)
    private String newPassword;

    @BPattern(BPatterns.PASSWORD)
    private String confirmNewPassword;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }

}
