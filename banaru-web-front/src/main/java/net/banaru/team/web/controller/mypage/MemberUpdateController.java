package net.banaru.team.web.controller.mypage;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ma.glasnost.orika.MapperFacade;
import net.banaru.team.web.constants.PathConstants;
import net.banaru.team.web.controller.AbstractController;
import net.banaru.team.web.dto.MemberUpdateForm;
import net.banaru.team.web.validator.MemberUpdateFormValidator;
import net.banaru.team.service.dto.member.MemberDto;
import net.banaru.team.service.dto.member.MemberUpdateDto;
import net.banaru.team.service.member.MemberService;
import net.banaru.team.web.configuration.security.UserDetailsImpl;

/**
 * @author banssolin
 */
@Controller
public class MemberUpdateController extends AbstractController {

    private static final String MEMBER_UPDATE_FORM = "MEMBER_UPDATE_FORM";

    private static final String MEMBER_UPDATE_TEMPLATE = "/mypage/member/update";

    private static final String UPDATE_SUCCESS_FLAG = "UPDATE_SUCCESS_FLAG";

    private final MemberService memberService;
    private final MemberUpdateFormValidator validator;
    private final MapperFacade mapperFacade;

    public MemberUpdateController(MemberService memberService, MemberUpdateFormValidator validator, MapperFacade mapperFacade) {
        this.memberService = memberService;
        this.validator = validator;
        this.mapperFacade = mapperFacade;
    }

    /**
     * View profile page.
     *
     * @param userDetailsImpl the user detail implements
     * @param modelMap the model map
     * @return the string
     */
    @GetMapping(PathConstants.MYPAGE_MEMBER_UPDATE)
    public String viewInput(@AuthenticationPrincipal UserDetailsImpl userDetailsImpl, ModelMap modelMap) {

        MemberDto member = memberService.getMember(userDetailsImpl.getMemberNo());

        MemberUpdateForm form = mapperFacade.map(member, MemberUpdateForm.class);

        modelMap.addAttribute("memberId", member.getMemberId());
        modelMap.addAttribute(MEMBER_UPDATE_FORM, form);

        return MEMBER_UPDATE_TEMPLATE;
    }

    @PostMapping(PathConstants.MYPAGE_MEMBER_UPDATE)
    public String procInput(@AuthenticationPrincipal UserDetailsImpl userDetailsImpl,
                            @Validated @ModelAttribute MemberUpdateForm form, BindingResult bindingResult,
                            SessionStatus sessionStatus, RedirectAttributes redirectAttributes) {

        validator.validate(form, bindingResult);
        if (bindingResult.hasErrors()) {
            printErrors(bindingResult);
            return MEMBER_UPDATE_TEMPLATE;
        }

        sessionStatus.setComplete();

        MemberUpdateDto updateDto = mapperFacade.map(form, MemberUpdateDto.class);
        updateDto.setMemberNo(userDetailsImpl.getMemberNo());

        memberService.updateMember(updateDto);

        redirectAttributes.addFlashAttribute(UPDATE_SUCCESS_FLAG, true);

        return getRedirectPath(PathConstants.MYPAGE_MEMBER_UPDATE);
    }

}
