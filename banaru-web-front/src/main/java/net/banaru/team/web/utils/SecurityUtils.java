package net.banaru.team.web.utils;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import net.banaru.team.web.constants.CookieConstants;

/**
 * TODO check and remove and change major encrypt
 *
 * @author banssolin
 */
@Component
public class SecurityUtils {

    private static final Logger logger = LoggerFactory.getLogger(CookieUtils.class);

    private final ServerProperties serverProperties;

    /**
     * Save login id cookie max age setting, in second, default : 180(day) * 24 * 60
     * * 60
     */
    // TODO move to config
    private final int saveIdCookieMaxAge = 15552000;

    private static String defaultInitVector = "g!%eC9#6lueL@nd)";
    private static final String encryptKeyOfInitVector = "INIT_VECTOR";
    private static final Pattern pattern = Pattern
        .compile("^\\$\\$([A-Za-z0-9\\+\\/\\=]+)\\$\\$([A-Za-z0-9\\+\\/\\=]+)$");

    public SecurityUtils(ServerProperties serverProperties) {
        this.serverProperties = serverProperties;
    }

    public void saveIdToCookie(HttpServletResponse response, String id) {
        String encryptedUserName = encrypt(CookieConstants.SAVE_ID, id);
        Cookie cookie = new Cookie(CookieConstants.SAVE_ID, encryptedUserName);
        cookie.setHttpOnly(true);
        if (isSsl()) {
            cookie.setSecure(true);
        }
        cookie.setMaxAge(saveIdCookieMaxAge);

        CookieUtils.addCookie(response, cookie);
    }

    public void removeSavedIdFromCookie(HttpServletRequest request, HttpServletResponse response) {
        CookieUtils.removeCookie(request, response, CookieConstants.SAVE_ID);
    }

    /**
     * Encrypt. Caution! Do not use this encryptor for like user password, user
     * password should not be decrypted. Use this encryptor for light encryption
     * (and needed decryption) for block value modification. For example, when save
     * user id at cookie, when save user name at cookie.
     *
     * @param key   the key
     * @param value the value
     * @return the string
     */
    public static String encrypt(String key, String value) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("ddHHmmssSSS");

        try {
            if (logger.isTraceEnabled()) {
                logger.trace(">>>>>>>>>>>>>>>>>>>>>>Start encrypt");
            }

            String newKey = getSuitableKey(key);
            if (logger.isTraceEnabled()) {
                logger.trace("original key:{} -> suitable key:{}", key, newKey);
            }

            String initVector = "ecp#!".concat(dateFormat.format(new Date()));
            if (logger.isTraceEnabled()) {
                logger.trace("initVector:{}", initVector);
            }

            String encryptedInitVector = encryptWithDefaultInitVector(encryptKeyOfInitVector, initVector);
            if (logger.isTraceEnabled()) {
                logger.trace("encryptedInitVector:{}", encryptedInitVector);
            }

            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(StandardCharsets.UTF_8));
            SecretKeySpec skeySpec = new SecretKeySpec(newKey.getBytes(StandardCharsets.UTF_8), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            String result = Base64.encodeBase64String(encrypted);
            if (logger.isTraceEnabled()) {
                logger.trace("plain:{} -> encrypted:{}", value, result);
            }

            return "$$".concat(encryptedInitVector).concat("$$").concat(result);
        } catch (Exception ex) {
            throw new IllegalArgumentException("fail to encrypt", ex);
        }
    }

    private static String getSuitableKey(String key) {
        if (key.getBytes().length < 16) {
            int i = key.length();

            while (key.getBytes().length < 16) {
                if (i > 9) {
                    i = Integer.parseInt(String.valueOf(i).substring(1, 2));
                }

                key = key.concat(String.valueOf(i));

                i = i + key.length();
            }
        }

        return key;
    }

    private static String encryptWithDefaultInitVector(String key, String value) {
        return encrypt(key, defaultInitVector, value);
    }

    private static String encrypt(String key, String initVector, String value) {
        try {
            String newKey = getSuitableKey(key);
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(StandardCharsets.UTF_8));
            SecretKeySpec skeySpec = new SecretKeySpec(newKey.getBytes(StandardCharsets.UTF_8), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());

            String result = Base64.encodeBase64String(encrypted);

            if (logger.isTraceEnabled()) {
                logger.trace(">>>>>[encrypt]");
                logger.trace("original key:{} -> suitable key:{}", key, newKey);
                logger.trace("initVector:{}", initVector);
                logger.trace("plain:{} -> encrypted:{}", value, result);
            }

            return result;
        } catch (Exception ex) {
            throw new IllegalArgumentException("fail to encrypt", ex);
        }
    }

    /**
     * Get saved user id from cookie string.
     *
     * @param request the request
     * @return the string
     */
    public String getSavedIdFromCookie(HttpServletRequest request) {
        String savedId = CookieUtils.getCookieValue(request, CookieConstants.SAVE_ID);

        if (!StringUtils.isEmpty(savedId)) {
            return decrypt(CookieConstants.SAVE_ID, savedId);
        }

        return savedId;
    }

    /**
     * Decrypt.
     *
     * @param key       the key
     * @param encrypted the encrypted
     * @return the string
     */
    public static String decrypt(String key, String encrypted) {
        try {
            if (logger.isTraceEnabled()) {
                logger.trace(">>>>>>>>>>>>>>>>>>>>>>Start decrypt");
            }

            String newKey = getSuitableKey(key);
            if (logger.isTraceEnabled()) {
                logger.trace("original key:{} -> suitable key:{}", key, newKey);
            }

            String encryptedInitVector = null;
            String encryptedData = null;
            {
                Matcher matcher = pattern.matcher(encrypted);

                if (matcher.matches()) {
                    encryptedInitVector = matcher.group(1);
                    encryptedData = matcher.group(2);
                }

                if (logger.isTraceEnabled()) {
                    logger.trace("encrypted:{}", encrypted);
                    logger.trace("-> encryptedInitVector:{}", encryptedInitVector);
                    logger.trace("-> encryptedData:{}", encryptedData);
                }
            }

            String decryptedInitVector = decryptWithDefaultInitVector(encryptKeyOfInitVector, encryptedInitVector);
            if (logger.isTraceEnabled()) {
                logger.trace("decryptedInitVector:{}", decryptedInitVector);
            }

            IvParameterSpec iv = new IvParameterSpec(decryptedInitVector.getBytes(StandardCharsets.UTF_8));
            SecretKeySpec skeySpec = new SecretKeySpec(newKey.getBytes(StandardCharsets.UTF_8), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.decodeBase64(encryptedData));
            String result = new String(original);
            if (logger.isTraceEnabled()) {
                logger.trace("encrypted:{} -> decrypted:{}", encryptedData, result);
            }

            return result;
        } catch (Exception ex) {
            throw new IllegalArgumentException("fail to decrypt", ex);
        }
    }

    private static String decryptWithDefaultInitVector(String key, String encrypted) {
        return decrypt(key, defaultInitVector, encrypted);
    }

    private static String decrypt(String key, String initVector, String encrypted) {
        try {
            String newKey = getSuitableKey(key);

            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(StandardCharsets.UTF_8));
            SecretKeySpec skeySpec = new SecretKeySpec(newKey.getBytes(StandardCharsets.UTF_8), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

            String result = new String(original);

            if (logger.isTraceEnabled()) {
                logger.trace(">>>>>[decrypt]");
                logger.trace("original key:{} -> suitable key:{}", key, newKey);
                logger.trace("initVector:{}", initVector);
                logger.trace("encrypted:{} -> decrypted:{}", encrypted, result);
            }

            return result;
        } catch (Exception ex) {
            throw new IllegalArgumentException("fail to decrypt", ex);
        }
    }

    public boolean isSsl() {
        return (serverProperties.getSsl() != null) && (serverProperties.getSsl().isEnabled());
    }

}
