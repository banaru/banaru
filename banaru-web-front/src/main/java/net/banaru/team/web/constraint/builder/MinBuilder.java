package net.banaru.team.web.constraint.builder;

import java.lang.annotation.Annotation;

import javax.validation.constraints.Min;

import org.springframework.util.Assert;

import net.banaru.team.web.constraint.ConstraintInfo;

/**
 * @author banssolin
 */
public class MinBuilder implements ConstraintBuilder<Min> {

    @Override
    public Class<Min> getConstraintClass() {
        return Min.class;
    }

    @Override
    public int getPrecedence() {
        return 2;
    }

    @Override
    public void build(Annotation constraint, ConstraintInfo info) {
        Assert.notNull(info, "ConstraintInfo must not be null.");
        Min min = (Min) constraint;
        info.setMinlength(new Long(min.value()).intValue());
    }

}
