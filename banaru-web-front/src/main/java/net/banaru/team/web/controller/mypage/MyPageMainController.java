package net.banaru.team.web.controller.mypage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import net.banaru.team.web.constants.PathConstants;
import net.banaru.team.web.controller.AbstractController;

/**
 * @author banssolin
 */
@Controller
public class MyPageMainController extends AbstractController {

    @GetMapping(PathConstants.MYPAGE)
    public String viewMypage() {
        return "/mypage/mypage_main";
    }

}
