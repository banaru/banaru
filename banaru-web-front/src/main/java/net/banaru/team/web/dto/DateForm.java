package net.banaru.team.web.dto;

import java.time.LocalDate;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import net.banaru.team.service.dto.BaseDto;

/**
 * @author banssolin
 */
public class DateForm implements BaseDto {

    private static final long serialVersionUID = -1294073780789362463L;

    // "form"태그의 input의 값이 공백 혹은 null인 경우, Property int에는 ""(공백)이 건네져
    // NumberFormatException가 발생한다.
    @NotNull
    @Min(1)
    @Max(9999)
    private Integer year;

    @NotNull
    @Min(1)
    @Max(12)
    private Integer month;

    @NotNull
    @Min(1)
    @Max(31)
    private Integer day;

    public DateForm() {

    }

    public DateForm(Integer year, Integer month, Integer day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public LocalDate toLocalDate() {
        return LocalDate.of(year, month, day);
    }

}
