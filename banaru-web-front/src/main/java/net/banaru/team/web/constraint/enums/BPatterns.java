package net.banaru.team.web.constraint.enums;

import net.banaru.team.web.constraint.patterns.BanaruPattern;
import net.banaru.team.web.constraint.patterns.MemberIdPattern;
import net.banaru.team.web.constraint.patterns.Name1Pattern;
import net.banaru.team.web.constraint.patterns.Name2Pattern;
import net.banaru.team.web.constraint.patterns.Name3Pattern;
import net.banaru.team.web.constraint.patterns.Name4Pattern;
import net.banaru.team.web.constraint.patterns.PasswordPattern;
import net.banaru.team.web.constraint.patterns.PhoneNoPattern;

/**
 * @author banssolin
 */
public enum BPatterns {

    MEMBER_ID(MemberIdPattern.class),
    PASSWORD(PasswordPattern.class),

    PHONE_NUMBER(PhoneNoPattern.class),

    NAME1(Name1Pattern.class),
    NAME2(Name2Pattern.class),
    NAME3(Name3Pattern.class),
    NAME4(Name4Pattern.class);

    private Class<? extends BanaruPattern> clazz;
    private BanaruPattern pattern;

    BPatterns(Class<? extends BanaruPattern> clazz) {
        this.clazz = clazz;
    }

    public BanaruPattern getPattern() {
        if (pattern == null) {
            try {
                this.pattern = clazz.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new IllegalArgumentException("Fail to create banaru pattern instance: " + clazz.getName(), e);
            }
        }
        return pattern;
    }

}
