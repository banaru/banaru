package net.banaru.team.web.constraint.patterns;

import java.util.Arrays;
import java.util.List;

import net.banaru.team.web.constraint.enums.PatternPreset;

/**
 * @author banssolin
 */
public class PasswordPattern extends BanaruPattern {

    @Override
    public boolean isRequired() {
        return true;
    }

    @Override
    public Integer getMin() {
        return 8;
    }

    @Override
    public Integer getMax() {
        return 24;
    }

    @Override
    public List<PatternPreset> getPresets() {
        return Arrays.asList(PatternPreset.PASSWORD);
    }

    @Override
    public String getGuideMessageKey() {
        return "banaru.pattern.guide.password";
    }

}
