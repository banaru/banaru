
package net.banaru.team.web.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import net.banaru.team.web.constraint.annotation.BPattern;
import net.banaru.team.web.constraint.enums.BPatterns;
import net.banaru.team.service.enums.Gender;

/**
 * @author banssolin
 */
public class MemberEditForm extends BaseForm {

    private static final long serialVersionUID = -5161547416191186184L;

    @Valid
    @NotNull
    private NameForm name;

    @NotBlank
    private String memberCountryCode;

    @NotBlank
    private String email;

    @Valid
    @NotNull
    private DateForm birthday;

    @NotNull
    private Gender gender;

    @BPattern(BPatterns.PHONE_NUMBER)
    private String phoneNo;

    public void setBirthday(Integer year, Integer month, Integer day) {
        this.birthday = new DateForm(year, month, day);
    }

    public void setBirthday(DateForm birthday) {
        this.birthday = birthday;
    }

    public DateForm getBirthday() {
        return birthday;
    }

    public NameForm getName() {
        return name;
    }

    public void setName(NameForm name) {
        this.name = name;
    }

    public String getMemberCountryCode() {
        return memberCountryCode;
    }

    public void setMemberCountryCode(String memberCountryCode) {
        this.memberCountryCode = memberCountryCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

}
