package net.banaru.team.web.constraint.patterns;

import java.util.Collections;
import java.util.List;

import net.banaru.team.web.constraint.enums.PatternPreset;

/**
 * @author banssolin
 */
public class Name4Pattern extends BanaruPattern {

    @Override
    public boolean isRequired() {
        return false;
    }

    @Override
    public Integer getMin() {
        return 1;
    }

    @Override
    public Integer getMax() {
        return 25;
    }

    @Override
    public List<PatternPreset> getPresets() {
        return Collections.singletonList(PatternPreset.ALL);
    }

    @Override
    public String getGuideMessageKey() {
        return null;
    }

}
