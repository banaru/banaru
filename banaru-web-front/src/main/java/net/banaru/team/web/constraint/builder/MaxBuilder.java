package net.banaru.team.web.constraint.builder;

import java.lang.annotation.Annotation;

import javax.validation.constraints.Max;

import org.springframework.util.Assert;

import net.banaru.team.web.constraint.ConstraintInfo;

/**
 * @author banssolin
 */
public class MaxBuilder implements ConstraintBuilder<Max> {

    @Override
    public Class<Max> getConstraintClass() {
        return Max.class;
    }

    @Override
    public int getPrecedence() {
        return 2;
    }

    @Override
    public void build(Annotation constraint, ConstraintInfo info) {
        Assert.notNull(info, "ConstraintInfo must not be null.");
        Max max = (Max) constraint;
        info.setMaxlength(new Long(max.value()).intValue());
    }

}
