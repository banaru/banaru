package net.banaru.team.web.controller.mypage;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.banaru.team.web.constants.PathConstants;
import net.banaru.team.web.controller.AbstractController;
import net.banaru.team.web.dto.PasswordChangeForm;
import net.banaru.team.web.validator.PasswordChangeFormValidator;
import net.banaru.team.service.member.MemberService;
import net.banaru.team.web.configuration.security.UserDetailsImpl;

@Controller
public class PasswordChangeController extends AbstractController {

    private static final String PASSWORD_CHANGE_TEMPLATE = "/mypage/member/password";

    private static final String PASSWORD_CHANGE_FORM = "PASSWORD_CHANGE_FORM";

    private static final String CHANGE_SUCCESS_FLAG = "CHANGE_SUCCESS_FLAG";

    private final MemberService memberService;
    private final PasswordChangeFormValidator validator;

    public PasswordChangeController(MemberService memberService, PasswordChangeFormValidator validator) {
        this.memberService = memberService;
        this.validator = validator;
    }

    @GetMapping(PathConstants.MYPAGE_MEMBER_PASSWORD)
    public String viewInput(Model model) {

        model.addAttribute(PASSWORD_CHANGE_FORM, new PasswordChangeForm());

        return PASSWORD_CHANGE_TEMPLATE;
    }

    @PostMapping(PathConstants.MYPAGE_MEMBER_PASSWORD)
    public String procInput(@AuthenticationPrincipal UserDetailsImpl userDetails,
                            @Validated @ModelAttribute(PASSWORD_CHANGE_FORM) PasswordChangeForm form, BindingResult bindingResult,
                            RedirectAttributes redirectAttributes) {

        validator.validate(form, bindingResult);
        if (bindingResult.hasErrors()) {
            printErrors(bindingResult);
            return PASSWORD_CHANGE_TEMPLATE;
        }

        memberService.changePassword(userDetails.getMemberNo(), form.getNewPassword());

        redirectAttributes.addFlashAttribute(CHANGE_SUCCESS_FLAG, true);

        return getRedirectPath(PathConstants.MYPAGE_MEMBER_PASSWORD);
    }

}
