
package net.banaru.team.web.dto;

import net.banaru.team.web.constraint.annotation.BPattern;
import net.banaru.team.web.constraint.enums.BPatterns;

/**
 * @author banssolin
 */
public class MemberRegisterForm extends MemberEditForm {

    private static final long serialVersionUID = 3083010433493297758L;

    @BPattern(BPatterns.MEMBER_ID)
    private String memberId;

    @BPattern(BPatterns.PASSWORD)
    private String password;

    @BPattern(BPatterns.PASSWORD)
    private String confirmPassword;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

}
