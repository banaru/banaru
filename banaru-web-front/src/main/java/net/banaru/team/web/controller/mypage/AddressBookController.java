package net.banaru.team.web.controller.mypage;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import net.banaru.team.web.constants.PathConstants;
import net.banaru.team.web.controller.AbstractController;
import net.banaru.team.service.dto.member.AddressBookDto;
import net.banaru.team.service.member.AddressBookService;
import net.banaru.team.web.configuration.security.UserDetailsImpl;

import java.util.List;

/**
 * @author banssolin
 */
@Controller
public class AddressBookController extends AbstractController {

    private final AddressBookService addressBookService;

    public AddressBookController(AddressBookService addressBookService) {
        this.addressBookService = addressBookService;
    }

    @GetMapping(PathConstants.MYPAGE_ADDRESS_BOOK)
    public String viewMypage(@AuthenticationPrincipal UserDetailsImpl userDetailsImpl, Model model) {
        long totalCount = addressBookService.getAddressBookCount(userDetailsImpl.getMemberNo());
        model.addAttribute("totalCount", totalCount);

        List<AddressBookDto> addressBooks = addressBookService.getAddressBooks(userDetailsImpl.getMemberNo());
        model.addAttribute("addressBooks", addressBooks);

        return "/mypage/address/list";
    }

}
