package net.banaru.team.web.configuration.mapper;

import java.time.LocalDate;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import net.banaru.team.web.dto.DateForm;

/**
 * Convert dateForm to localDate.
 *
 * @author banssolin
 */
public class DateFormLocalDateConverter extends BidirectionalConverter<DateForm, LocalDate> {

    @Override
    public LocalDate convertTo(DateForm source, Type<LocalDate> destinationType, MappingContext mappingContext) {
        return LocalDate.of(source.getYear(), source.getMonth(), source.getDay());
    }

    @Override
    public DateForm convertFrom(LocalDate source, Type<DateForm> destinationType, MappingContext mappingContext) {
        return new DateForm(source.getYear(), source.getMonthValue(), source.getDayOfMonth());
    }

}
