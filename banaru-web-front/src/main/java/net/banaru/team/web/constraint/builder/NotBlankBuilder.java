package net.banaru.team.web.constraint.builder;

import java.lang.annotation.Annotation;

import javax.validation.constraints.NotBlank;

import org.springframework.util.Assert;

import net.banaru.team.web.constraint.ConstraintInfo;

/**
 * @author banssolin
 */
public class NotBlankBuilder implements ConstraintBuilder<NotBlank> {

    @Override
    public Class<NotBlank> getConstraintClass() {
        return NotBlank.class;
    }

    @Override
    public int getPrecedence() {
        return 1;
    }

    @Override
    public void build(Annotation constraint, ConstraintInfo info) {
        Assert.notNull(info, "ConstraintInfo must not be null.");
        info.setRequired(true);
    }

}
