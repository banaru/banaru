package net.banaru.team.web.configuration.mapper;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import ma.glasnost.orika.MapperFactory;

@Configuration
@DependsOn("webMapperConfiguration")
public class FrontMapperConfiguration {

    public FrontMapperConfiguration(MapperFactory mapperFactory) {
        mapperFactory.getConverterFactory().registerConverter(new DateFormLocalDateConverter());
    }

}
