package net.banaru.team.web.constraint.builder;

import java.lang.annotation.Annotation;

import javax.validation.constraints.NotNull;

import org.springframework.util.Assert;

import net.banaru.team.web.constraint.ConstraintInfo;

/**
 * @author banssolin
 */
public class NotNullBuilder implements ConstraintBuilder<NotNull> {

    @Override
    public Class<NotNull> getConstraintClass() {
        return NotNull.class;
    }

    @Override
    public int getPrecedence() {
        return 1;
    }

    @Override
    public void build(Annotation constraint, ConstraintInfo info) {
        Assert.notNull(info, "ConstraintInfo must not be null.");
        info.setRequired(true);
    }

}
