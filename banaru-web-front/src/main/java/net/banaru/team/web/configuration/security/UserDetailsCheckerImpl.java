package net.banaru.team.web.configuration.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;

/**
 * 
 * AuthenticationProvider 에서 호출되며, 패스워드 인증 전 체크를 담당하는 클래스.
 * 
 * @author banssolin
 *
 */
public class UserDetailsCheckerImpl implements UserDetailsChecker {

    @Override
    public void check(UserDetails toCheck) {
        // TODO Auto-generated method stub

    }

}
