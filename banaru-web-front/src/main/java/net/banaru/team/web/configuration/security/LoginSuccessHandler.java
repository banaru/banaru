package net.banaru.team.web.configuration.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.util.StringUtils;

import net.banaru.team.web.utils.CookieUtils;
import net.banaru.team.web.utils.SecurityUtils;

/**
 * @author banssolin
 */
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final SecurityUtils securityUtils;

    public LoginSuccessHandler(SecurityUtils securityUtils) {
        this.securityUtils = securityUtils;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws ServletException, IOException {

        Object principal = authentication.getPrincipal();
        UserDetailsImpl userDetailsImpl = (UserDetailsImpl) principal;

        if (logger.isDebugEnabled()) {
            logger.debug("====== Log in Success ======");
            logger.debug("Member Number: {}", userDetailsImpl.getMemberNo());
            logger.debug("Member ID: {}", userDetailsImpl.getUsername());
            logger.debug("Authorities: {}", userDetailsImpl.getAuthorities());
        }

        // Process after login success

        // From loginForm
        String saveId = request.getParameter("saveId");
        if (!StringUtils.isEmpty(saveId) && Boolean.valueOf(saveId)) {
            securityUtils.saveIdToCookie(response, userDetailsImpl.getUsername());
        } else {
            securityUtils.removeSavedIdFromCookie(request, response);
        }

        super.onAuthenticationSuccess(request, response, authentication);
    }

    /**
     * Http <-> Https 간에 세션이 끊기는 것을 방지 하기 위해 session cookie를 secure false로 다시 덮어씀
     */
    private void overrideJsessionId(HttpServletRequest request, HttpServletResponse response) {
        final Cookie jsessionid = CookieUtils.createCookie("JSESSIONID", getSessionId(request), -1);
        jsessionid.setHttpOnly(true);
        CookieUtils.addCookie(response, jsessionid);
    }

    public static String getSessionId(HttpServletRequest request) {
        if (request == null) {
            throw new IllegalArgumentException("Request must not be null");
        }
        HttpSession session = request.getSession(false);
        return (session != null ? session.getId() : null);
    }

}
