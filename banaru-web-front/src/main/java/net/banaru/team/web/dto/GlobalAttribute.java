
package net.banaru.team.web.dto;

import java.util.List;


/**
 * @author banssolin
 */
public class GlobalAttribute {

    private String fullRequestUrl;
    private String requestUrl;

    private Language language;
    private List<Language> supportedLanguages;

    private List<Country> supportedCountries;

    public String getFullRequestUrl() {
        return fullRequestUrl;
    }

    public void setFullRequestUrl(String fullRequestUrl) {
        this.fullRequestUrl = fullRequestUrl;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public List<Language> getSupportedLanguages() {
        return supportedLanguages;
    }

    public void setSupportedLanguages(List<Language> supportedLanguages) {
        this.supportedLanguages = supportedLanguages;
    }

    public List<Country> getSupportedCountries() {
        return supportedCountries;
    }

    public void setSupportedCountries(List<Country> supportedCountries) {
        this.supportedCountries = supportedCountries;
    }

}
