package net.banaru.team.web.constraint.patterns;

import java.util.Arrays;
import java.util.List;

import net.banaru.team.web.constraint.enums.PatternPreset;

/**
 * @author banssolin
 */
public class MemberIdPattern extends BanaruPattern {

    @Override
    public boolean isRequired() {
        return true;
    }

    @Override
    public Integer getMin() {
        return 6;
    }

    @Override
    public Integer getMax() {
        return 16;
    }

    @Override
    public List<PatternPreset> getPresets() {
        return Arrays.asList(PatternPreset.ALPHABET_LOWER, PatternPreset.ALPHABET_UPPER, PatternPreset.DIGIT);
    }

    @Override
    public String getGuideMessageKey() {
        return "banaru.pattern.guide.password";
    }

}
