package net.banaru.team.web.constraint.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import net.banaru.team.web.constraint.enums.BPatterns;

/**
 * @author banssolin
 */
@Target({
        ElementType.FIELD
})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {
        BPatternValidator.class
})
public @interface BPattern {

    // Required despite not being used.
    String message() default "";

    // Required despite not being used.
    Class<?>[] groups() default {};

    // Required despite not being used.
    Class<? extends Payload>[] payload() default {};

    BPatterns value();

}
