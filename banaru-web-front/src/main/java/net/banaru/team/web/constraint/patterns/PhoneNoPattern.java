package net.banaru.team.web.constraint.patterns;

import java.util.Arrays;
import java.util.List;

import net.banaru.team.web.constraint.enums.PatternPreset;

/**
 * @author banssolin
 */
public class PhoneNoPattern extends BanaruPattern {

    @Override
    public boolean isRequired() {
        return false;
    }

    @Override
    public Integer getMin() {
        return 0;
    }

    @Override
    public Integer getMax() {
        return 20;
    }

    @Override
    public List<PatternPreset> getPresets() {
        return Arrays.asList(PatternPreset.PHONE_NUMBER);
    }

    @Override
    public String getGuideMessageKey() {
        return "banaru.pattern.guide.phone.number";
    }

}
