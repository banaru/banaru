package net.banaru.team.web.constraint.builder;

import org.springframework.util.Assert;

import net.banaru.team.web.constraint.ConstraintInfo;

import java.lang.annotation.Annotation;

import javax.validation.constraints.Size;

/**
 * @author banssolin
 */
public class SizeBuilder implements ConstraintBuilder<Size> {

    @Override
    public Class<Size> getConstraintClass() {
        return Size.class;
    }

    @Override
    public int getPrecedence() {
        return 2;
    }

    @Override
    public void build(Annotation constraint, ConstraintInfo info) {
        Assert.notNull(info, "ConstraintInfo must not be null.");
        Size size = (Size) constraint;
        info.setMaxlength(size.max());
        info.setMinlength(size.min());
    }
}
