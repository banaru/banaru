package net.banaru.team.web.sample.configprop;

import org.springframework.boot.context.properties.ConfigurationProperties;

// @ConfigurationProperties 만으로는 빈으로 안만들어 진다.
// 1. 이 클래스에 직접 @Component로 빈 등록
// 2. @Configuration 클래스 에서 @Bean 메서드로 등록
// 3. @Configuration 클래스에 @EnableConfigurationProperties로 등록
/**
 * @author banssolin
 */
@ConfigurationProperties(prefix = "sample")
public class SampleProperties {

    /**
     * This is sample1
     */
    private String sample1;

    private String sample2;

    /**
     * This is sample1 getter
     */
    public String getSample1() {
        return sample1;
    }

    /**
     * This is sample1 setter
     */
    public void setSample1(String sample1) {
        this.sample1 = sample1;
    }

    public String getSample2() {
        return sample2;
    }

    public void setSample2(String sample2) {
        this.sample2 = sample2;
    }

}
