package net.banaru.team.web.constraint;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.banaru.team.web.constraint.enums.PatternPreset;

/**
 * @author banssolin
 */
public class RegexpBuilder {

    // regular expression - "regex" or "regexp", They are both OK.
    private static final String MATCH_REGEXP = "(%s*)?";

    public static String buildRegexp(List<PatternPreset> presets) {
        if (presets.contains(PatternPreset.ALL)) {
            return PatternPreset.ALL.getRegexp();
        }

        Set<PatternPreset> targets = new HashSet<>(presets);

        boolean isMultiplePatterns = targets.size() > 1;
        String regexp = "";
        for (PatternPreset target : new HashSet<>(presets)) {
            regexp = isMultiplePatterns ? regexp.concat(buildRegexp(target)) : target.getRegexp();
        }

        if (isMultiplePatterns) {
            StringBuilder builder = new StringBuilder();
            builder.append("[");
            builder.append(regexp);
            builder.append("]");
            regexp = builder.toString();
        }

        return String.format(MATCH_REGEXP, regexp);
    }

    // TODO change method name
    public static String buildRegexp(PatternPreset preset) {
        final String regexp = preset.getRegexp();
        // TODO check
        Matcher matcher = Pattern.compile("\\[(.*)\\]").matcher(regexp);

        if (!matcher.find() || regexp.startsWith("[^")) {
            // TODO Check how to use logger in static class.
            // log.error("preset combination only support positive bracket expression");
            throw new IllegalArgumentException("invalid preset combination");
        } else {
            return matcher.group(1);
        }
    }

}
