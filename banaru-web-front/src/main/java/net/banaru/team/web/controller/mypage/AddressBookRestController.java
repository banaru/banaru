package net.banaru.team.web.controller.mypage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import net.banaru.team.web.constants.PathConstants;
import net.banaru.team.web.controller.AbstractController;
import net.banaru.team.service.member.AddressBookService;

/**
 * @author banssolin
 */
@RestController
public class AddressBookRestController extends AbstractController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private final AddressBookService addressBookService;

    public AddressBookRestController(AddressBookService addressBookService) {
        this.addressBookService = addressBookService;
    }

    @DeleteMapping(PathConstants.API_MYPAGE_ADDRESS_BOOK_DELETE + "/{addressBookNo}")
    public void deleteAddressBook(@PathVariable Long addressBookNo) {
        logger.debug("Delete address target number: {}", addressBookNo);
        addressBookService.deleteAddressBook(addressBookNo);
    }

}
