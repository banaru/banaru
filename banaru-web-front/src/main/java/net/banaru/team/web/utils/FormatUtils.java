package net.banaru.team.web.utils;

import java.util.IllformedLocaleException;
import java.util.Locale;

/**
 * @author banssolin
 */
public class FormatUtils {

    public static String getCountryName(String countryCode, Locale locale) {
        String countryName;
        try {
            countryName = new Locale.Builder()
                    .setRegion(countryCode)
                    .build().getDisplayCountry(locale);
        } catch (IllformedLocaleException e) {
            return null;
        }
        return countryName;
    }

}
