package net.banaru.team.web.constraint.builder;

import java.lang.annotation.Annotation;

import org.springframework.core.annotation.AnnotationUtils;

import net.banaru.team.web.constraint.ConstraintInfo;

/**
 * @author banssolin
 */
public interface ConstraintBuilder<T extends Annotation> {

    Class<T> getConstraintClass();

    int getPrecedence();

    void build(Annotation constraint, ConstraintInfo info);

    default Object getValue(Annotation constraint) {
        if (!constraint.annotationType().isAssignableFrom(getConstraintClass())) {
            return null;
        }
        return AnnotationUtils.getValue(constraint);
    }

}
