package net.banaru.team.web.validator;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import net.banaru.team.web.dto.PasswordChangeForm;

@Component
public class PasswordChangeFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return PasswordChangeForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PasswordChangeForm form = (PasswordChangeForm) target;

        if (form.getPassword().equals(form.getNewPassword())) {
            errors.rejectValue("newPassword", "error.password.not.changed");
        }

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String id;
        if (principal instanceof UserDetails) {
            id = ((UserDetails) principal).getUsername();
        } else {
            id = principal.toString();
        }

        if (form.getNewPassword().equals(id)) {
            errors.rejectValue("newPassword", "error.duplicated.password.with.member.id");
        }

        if (!form.getNewPassword().equals(form.getConfirmNewPassword())) {
            errors.rejectValue("confirmNewPassword", "error.not.match.password");
        }
    }

}
