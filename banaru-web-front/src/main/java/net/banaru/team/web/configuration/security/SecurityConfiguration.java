package net.banaru.team.web.configuration.security;

import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import net.banaru.team.web.constants.PathConstants;
import net.banaru.team.web.utils.SecurityUtils;

/**
 * 일반 회원용
 *
 * @author banssolin
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final UserDetailsServiceImpl userDetailsServiceImpl;
    private final SecurityUtils securityUtils;
    // from serviceConfiguration
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    // TODO 용도 파악 필요
    private final ServerProperties serverProperties;

    public SecurityConfiguration(UserDetailsServiceImpl userDetailsServiceImpl, SecurityUtils securityUtils,
                                 BCryptPasswordEncoder bCryptPasswordEncoder, ServerProperties serverProperties) {
        this.userDetailsServiceImpl = userDetailsServiceImpl;
        this.securityUtils = securityUtils;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.serverProperties = serverProperties;
    }

    private static final String[] AUTHENTICATION_PATHS = {
        PathConstants.MYPAGE.concat("/**"),
        PathConstants.LOGOUT
    };

    /**
     * <pre>
     * 1. 「HttpSecurity」에 대한 설정
     *     1. 로그인
     *     2. 로그아웃
     *     3. URL에 대한 권한 설정
     * </pre>
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 순서 중요, 반드시 필터 등록이 먼저인지 확인 필요
        configureLogin(http);
        configureLogout(http);
        configureAuthorize(http);

        http.csrf().disable();
        // for csrf same token
        // http.headers().frameOptions().sameOrigin();
        http.headers().frameOptions().disable();
    }

    // ====================================
    // Login
    // ====================================

    private void configureLogin(HttpSecurity http) throws Exception {
        http
            .addFilterBefore(usernamePasswordAuthenticationFilter(), AnonymousAuthenticationFilter.class);
    }

    /**
     * <p>
     * "/login/process"로 들어오는 처리에 대해 필터링 설정
     * </p>
     *
     * @return {@link UsernamePasswordAuthenticationFilter}
     */
    public UsernamePasswordAuthenticationFilter usernamePasswordAuthenticationFilter() throws Exception {
        UsernamePasswordAuthenticationFilter filter = new UsernamePasswordAuthenticationFilter();
        filter.setAuthenticationManager(authenticationManagerBean());
        filter.setFilterProcessesUrl(PathConstants.LOGIN_PROCESS);
        filter.setUsernameParameter("memberId"); // default: username
        filter.setPasswordParameter("password"); // default: password
        filter.setAuthenticationSuccessHandler(loginSuccessHandler());
        filter.setAuthenticationFailureHandler(LoginFailHandler());
        return filter;
    }

    public LoginSuccessHandler loginSuccessHandler() {
        LoginSuccessHandler loginSuccessHandler = new LoginSuccessHandler(securityUtils);
        loginSuccessHandler.setDefaultTargetUrl(PathConstants.MYPAGE);
        // loginSuccessHandler.setAlwaysUseDefaultTargetUrl(true);
        return loginSuccessHandler;
    }

    public LoginFailureHandler LoginFailHandler() {
        LoginFailureHandler loginFailureHandler = new LoginFailureHandler();
        loginFailureHandler.setDefaultFailureUrl(PathConstants.LOGIN);
        return loginFailureHandler;
    }

    // ====================================
    // Logout
    // ====================================

    private void configureLogout(HttpSecurity http) throws Exception {
        http.logout()
            .logoutRequestMatcher(new AntPathRequestMatcher(PathConstants.LOGOUT))
            // .addLogoutHandler(new CookieClearingLogoutHandler(LOGOUT_CLEAR_COOKIES))
            .logoutSuccessUrl(PathConstants.INDEX)
            .logoutSuccessHandler(new LogoutSuccessHandler());
        // .invalidateHttpSession(true)

    }

    // ====================================
    // URL Authorize
    // ====================================

    private void configureAuthorize(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            // 권한 필요 페이지 권한 지정
            .antMatchers(AUTHENTICATION_PATHS)
            .hasRole(UserRole.MEMBER.name())
            // 그 외 모든 페이지, 모든 권한 허용
            .antMatchers("/**")
            .permitAll()
            // .anyRequest().permitAll()
            // .antMatchers(PathConstants.LOGIN.concat("/**"))
            // .anonymous()

            // 권한 필요 페이지 진입 시, 권한 없으면, 보낼 페이지 지정
            .and()
            .exceptionHandling()
            .authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint(PathConstants.LOGIN));
    }

    // ====================================
    // 회원 정보 취득 및 패스워드 인증
    // ====================================

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .authenticationProvider(daoAuthenticationProvider());
    }

    /**
     * 1. ID로 userDetailsServiceImpl에서 Member정보 획득
     * 2. 획득한 정보로 Password 확인
     * 3. userDetails 체크(TODO 확인필요)
     *
     * @return {@link DaoAuthenticationProvider}
     */
    private DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsServiceImpl);
        provider.setPreAuthenticationChecks(new UserDetailsCheckerImpl());
        // TODO 여러 기본 제공 인코더가 있는듯
        provider.setPasswordEncoder(bCryptPasswordEncoder);
        // AuthenticationProvider 에서 호출되며, 패스워드 「인증 후」 체크를 담당하는 클래스.
        // provider.setPostAuthenticationChecks(postAuthenticationChecks);
        return provider;
    }

}
