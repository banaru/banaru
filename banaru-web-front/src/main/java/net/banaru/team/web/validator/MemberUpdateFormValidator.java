package net.banaru.team.web.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import net.banaru.team.web.dto.MemberUpdateForm;

/**
 * @author banssolin
 */
@Component
public class MemberUpdateFormValidator extends MemberEditFormValidator {

    @Override
    public boolean supports(Class<?> clazz) {
        return super.supports(clazz) && MemberUpdateForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        super.validate(target, errors);
    }

}
