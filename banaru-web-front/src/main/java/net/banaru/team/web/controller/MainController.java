package net.banaru.team.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author banssolin
 */
@Controller
public class MainController extends AbstractController {

    @GetMapping("/")
    public String index() {
        return "/main";
    }

}
