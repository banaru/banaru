package net.banaru.team.web.constraint;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import net.banaru.team.web.constraint.builder.BPatternBuilder;
import net.banaru.team.web.constraint.builder.ConstraintBuilder;
import net.banaru.team.web.constraint.builder.MaxBuilder;
import net.banaru.team.web.constraint.builder.MinBuilder;
import net.banaru.team.web.constraint.builder.NotBlankBuilder;
import net.banaru.team.web.constraint.builder.NotNullBuilder;
import net.banaru.team.web.constraint.builder.SizeBuilder;
import net.banaru.team.web.dto.BaseForm;
import net.banaru.team.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Constraint;
import javax.validation.Valid;

/**
 * @author banssolin
 */
public class ConstraintUtils {

    private static Logger logger = LoggerFactory.getLogger(ConstraintUtils.class);

    /** The Constant CONSTRAINT_BUILDER_MAP. */
    private static final Map<Class<? extends Annotation>, ConstraintBuilder<? extends Annotation>> CONSTRAINT_BUILDER_MAP;

    static {
        Set<ConstraintBuilder<? extends Annotation>> builders = new HashSet<>();

        builders.add(new BPatternBuilder());
        builders.add(new NotNullBuilder());
        builders.add(new NotBlankBuilder());
        builders.add(new SizeBuilder());
        builders.add(new MaxBuilder());
        builders.add(new MinBuilder());

        CONSTRAINT_BUILDER_MAP = builders.stream()
                                         .sorted(new Comparator<ConstraintBuilder<? extends Annotation>>() {

                                             @Override
                                             public int compare(ConstraintBuilder<? extends Annotation> builder1,
                                                                ConstraintBuilder<? extends Annotation> builder2) {
                                                 return Integer.valueOf(builder1.getPrecedence())
                                                               .compareTo(Integer.valueOf(builder2.getPrecedence()));
                                             }
                                         })
                                         .collect(Collectors
                                             .toMap(builder -> builder.getConstraintClass(),
                                                 builder -> builder,
                                                 (u, v) -> {
                                                     throw new IllegalStateException(String.format("Duplicate key %s", u));
                                                 }, LinkedHashMap::new));

        logger.debug(JsonUtils.toPrettyJson(CONSTRAINT_BUILDER_MAP));
    }

    /**
     * Gets the field constraint map.
     *
     * @param formClass the form class
     * @return the field constraint map
     */
    public static Map<String, ConstraintInfo> getFieldConstraintMap(Class<? extends BaseForm> formClass) {
        Map<String, ConstraintInfo> fieldConstraintMap = new HashMap<>();
        buildFieldConstraintMap(formClass, fieldConstraintMap, null);
        return fieldConstraintMap;
    }

    /**
     * Builds the field constraint map.
     *
     * @param clazz the clazz
     * @param fieldConstraintMap the field constraint map
     * @param parentFieldName the parent field name
     */
    private static void buildFieldConstraintMap(Class<?> clazz, Map<String, ConstraintInfo> fieldConstraintMap,
                                                String parentFieldName) {
        Class<?> superClass = clazz.getSuperclass();
        if (superClass != null) {
            buildFieldConstraintMap(superClass, fieldConstraintMap, null);
        }

        for (Field field : clazz.getDeclaredFields()) {
            final String fieldName = StringUtils.isEmpty(parentFieldName) ? field.getName()
                : parentFieldName.concat(".").concat(field.getName());
            // get nested pattern
            if (field.isAnnotationPresent(Valid.class)) {
                buildFieldConstraintMap(field.getType(), fieldConstraintMap, fieldName);
            } else {
                Set<Annotation> fieldConstraints = null;
                for (Annotation annotation : field.getAnnotations()) {
                    if (isTargetAnnotation(annotation)) {
                        fieldConstraints = CollectionUtils.isEmpty(fieldConstraints) ? new HashSet<>()
                            : fieldConstraints;
                        fieldConstraints.add(annotation);
                    }
                }
                if (!CollectionUtils.isEmpty(fieldConstraints)) {
                    fieldConstraintMap.put(fieldName, buildConstraintInfo(fieldConstraints));
                }
            }
        }
    }

    /**
     * Checks if is constraint annotation.
     *
     * @param annotation the annotation
     * @return true, if is constraint annotation
     */
    private static boolean isConstraintAnnotation(Annotation annotation) {
        Class<? extends Annotation> annotationClass = annotation.annotationType();
        return annotationClass.isAnnotationPresent(Constraint.class);
    }

    /**
     * Checks if is target annotation.
     *
     * @param annotation the annotation
     * @return true, if is target annotation
     */
    private static boolean isTargetAnnotation(Annotation annotation) {
        return isConstraintAnnotation(annotation) && CONSTRAINT_BUILDER_MAP.containsKey(annotation.annotationType());
    }

    /**
     * Builds the constraint info.
     *
     * @param fieldConstraints the field constraints
     * @return the constraint info
     */
    private static ConstraintInfo buildConstraintInfo(Set<Annotation> fieldConstraints) {
        Map<Class<? extends Annotation>, Annotation> constraintMap = new HashMap<>();
        fieldConstraints.forEach(constraint -> constraintMap.put(constraint.annotationType(), constraint));

        ConstraintInfo info = new ConstraintInfo();
        CONSTRAINT_BUILDER_MAP.entrySet().forEach(entry -> {
            Class<? extends Annotation> targetClass = entry.getKey();
            if (constraintMap.containsKey(entry.getKey())) {
                entry.getValue().build(constraintMap.get(targetClass), info);
            }
        });
        return info;
    }

}
