package net.banaru.team.web.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import net.banaru.team.web.dto.MemberRegisterForm;
import net.banaru.team.service.member.MemberService;

/**
 * @author banssolin
 */
@Component
public class MemberRegisterFormValidator extends MemberEditFormValidator {

    private final MemberService memberService;

    public MemberRegisterFormValidator(MemberService memberService) {
        this.memberService = memberService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return super.supports(clazz) && MemberRegisterForm.class.isAssignableFrom(clazz);
    }

    /*
     * 1. Global error>
     * -> errors.reject("messageCode", new Object[]{}, "defaultMessage");
     * 2. Field error>
     * -> errors.rejectValue("fieldName", "messageCode", new Object[]{}, "defaultMessage");
     */
    @Override
    public void validate(Object target, Errors errors) {
        super.validate(target, errors);

        MemberRegisterForm form = (MemberRegisterForm) target;

        boolean existMemberId = memberService.existMemberId(form.getMemberId());
        if (existMemberId) {
            errors.rejectValue("id", "error.exist.member.id");
        }

        if (form.getPassword().equals(form.getMemberId())) {
            errors.rejectValue("password", "error.duplicated.password.with.member.id");
        }

        if (!form.getPassword().equals(form.getConfirmPassword())) {
            errors.rejectValue("confirmPassword", "error.not.match.password");
        }

    }

}
