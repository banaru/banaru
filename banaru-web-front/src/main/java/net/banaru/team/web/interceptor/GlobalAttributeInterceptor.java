package net.banaru.team.web.interceptor;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import net.banaru.team.web.dto.Country;
import net.banaru.team.web.dto.GlobalAttribute;
import net.banaru.team.web.dto.Language;
import net.banaru.team.web.properties.LocaleProperties;

/**
 * @author banssolin
 */
public class GlobalAttributeInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private LocaleProperties localeProperties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        // Exclude resources(ResourceHttpRequestHandler)
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        GlobalAttribute globalAttribute = new GlobalAttribute();

        // URL
        {
            // request.getRequestURL(); // "http://localhost:8080/${contextPath}/mypage/dashboard"
            globalAttribute.setFullRequestUrl(request.getRequestURL().toString());
            // request.getRequestURI(); // "/${contextPath}/mypage/dashboard"
            globalAttribute.setRequestUrl(request.getRequestURI().substring(request.getContextPath().length()));
        }

        // Language
        {
            globalAttribute.setLanguage(buildLanguage(LocaleContextHolder.getLocale()));

            List<Language> supportedLanguages = new ArrayList<>();
            localeProperties.getSupportedLanguages()
                .forEach(lang -> supportedLanguages.add(buildLanguage(lang)));
            globalAttribute.setSupportedLanguages(supportedLanguages);
        }

        // Country
        {
            List<Country> supportedCountries = new ArrayList<>();

            for (String countryCode : localeProperties.getSupportedCountryCodes()) {

                String displayCountry = new Locale.Builder()
                    .setRegion(countryCode)
                    .build()
                    .getDisplayCountry(LocaleContextHolder.getLocale());

                supportedCountries.add(new Country(countryCode, displayCountry));
            }

            globalAttribute.setSupportedCountries(supportedCountries);
        }

        request.setAttribute("global", globalAttribute);
        return true;
    }

    private Language buildLanguage(Locale locale) {
        return new Language(locale.getDisplayName(LocaleContextHolder.getLocale()), locale);
    }

}
