package net.banaru.team.web.controller.member;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ma.glasnost.orika.MapperFacade;

import net.banaru.team.service.enums.MemberGrade;
import net.banaru.team.service.enums.MemberStatus;
import net.banaru.team.web.constants.PathConstants;
import net.banaru.team.web.constants.SessionConstants;
import net.banaru.team.web.controller.AbstractController;
import net.banaru.team.web.dto.MemberRegisterForm;
import net.banaru.team.web.dto.NameForm;
import net.banaru.team.web.validator.MemberRegisterFormValidator;
import net.banaru.team.service.dto.member.MemberRegisterDto;
import net.banaru.team.service.enums.Gender;
import net.banaru.team.service.member.MemberService;

/**
 * @author banssolin
 */
@Controller
@SessionAttributes(SessionConstants.MEMBER_REGISTER_FORM)
public class MemberRegisterController extends AbstractController {

    private static final String REGISTERED_MEMBER_NO = "REGISTERED_MEMBER_NO";

    private static final String MEMBER_REGISTER_INPUT_TEMPLATE = "/member/register/member_register_input";
    private static final String MEMBER_REGISTER_CONFIRM_TEMPLATE = "/member/register/member_register_confirm";
    private static final String MEMBER_REGISTER_COMPLETE_TEMPLATE = "/member/register/member_register_complete";

    private final MemberService memberService;
    private final MemberRegisterFormValidator validator;
    private final MapperFacade mapperFacade;

    public MemberRegisterController(MemberService memberService, MemberRegisterFormValidator validator, MapperFacade mapperFacade) {
        this.memberService = memberService;
        this.validator = validator;
        this.mapperFacade = mapperFacade;
    }

    @GetMapping(PathConstants.MEMBER_REGISTER)
    public String initInputView(Model model) {
        MemberRegisterForm form;
        {
            // set member register form default value
            form = buildDummyRegisterMemberForm();
        }

        model.addAttribute(SessionConstants.MEMBER_REGISTER_FORM, form);
        return getRedirectPath(PathConstants.MEMBER_REGISTER_INPUT);
    }

    @GetMapping(PathConstants.MEMBER_REGISTER_INPUT)
    public String viewInput() {
        return MEMBER_REGISTER_INPUT_TEMPLATE;
    }

    @PostMapping(PathConstants.MEMBER_REGISTER_INPUT)
    public String processInput(
        @Validated @ModelAttribute(SessionConstants.MEMBER_REGISTER_FORM) MemberRegisterForm form,
        BindingResult bindingResult) {

        validator.validate(form, bindingResult);
        if (bindingResult.hasErrors()) {
            printErrors(bindingResult);
            return MEMBER_REGISTER_INPUT_TEMPLATE;
        }

        return getRedirectPath(PathConstants.MEMBER_REGISTER_CONFIRM);
    }

    @GetMapping(PathConstants.MEMBER_REGISTER_CONFIRM)
    public String viewConfirm(
        @Validated @ModelAttribute(SessionConstants.MEMBER_REGISTER_FORM) MemberRegisterForm form,
        BindingResult bindingResult) {

        validator.validate(form, bindingResult);
        if (bindingResult.hasErrors()) {
            printErrors(bindingResult);
            return MEMBER_REGISTER_INPUT_TEMPLATE;
        }

        return MEMBER_REGISTER_CONFIRM_TEMPLATE;
    }

    @PostMapping(PathConstants.MEMBER_REGISTER_CONFIRM)
    public String processConfirm(
        @Validated @ModelAttribute(SessionConstants.MEMBER_REGISTER_FORM) MemberRegisterForm form,
        BindingResult bindingResult, SessionStatus sessionStatus, RedirectAttributes redirectAttributes) {

        validator.validate(form, bindingResult);
        if (bindingResult.hasErrors()) {
            printErrors(bindingResult);
            return MEMBER_REGISTER_INPUT_TEMPLATE;
        }

        sessionStatus.setComplete();

        MemberRegisterDto dto = mapperFacade.map(form, MemberRegisterDto.class);
        // Set Front default value
        {
            dto.setMemberGrade(MemberGrade.GENERAL);
            dto.setMemberStatus(MemberStatus.GENERAL);
        }
        Long registeredMemberNo = memberService.registerMember(dto);
        // logger.debug("register success: {}", member.toJson());

        redirectAttributes.addFlashAttribute(REGISTERED_MEMBER_NO, registeredMemberNo);

        return getRedirectPath(PathConstants.LOGIN);
    }

    @GetMapping(PathConstants.MEMBER_REGISTER_COMPLETE)
    public String viewComplete(ModelMap modelMap) {
        Long memberNo = (Long) modelMap.get(REGISTERED_MEMBER_NO);
        if (memberNo == null) {
            return getRedirectPath(PathConstants.LOGIN);
        }

        return MEMBER_REGISTER_COMPLETE_TEMPLATE;
    }

    private MemberRegisterForm buildDummyRegisterMemberForm() {
        MemberRegisterForm registerForm = new MemberRegisterForm();
        registerForm.setMemberId("testtest");
        registerForm.setPassword("test123!");
        registerForm.setConfirmPassword("test123!");
        registerForm.setMemberCountryCode("US");
        registerForm.setName(buildDummyNameDto());
        registerForm.setEmail("member@banaru.net");
        registerForm.setBirthday(1985, 7, 14);
        registerForm.setGender(Gender.MALE);
        registerForm.setPhoneNo("198989898");

        return registerForm;
    }

    private NameForm buildDummyNameDto() {
        NameForm nameForm = new NameForm();
        nameForm.setName1("TEST_NAME1");
        return nameForm;
    }

}
