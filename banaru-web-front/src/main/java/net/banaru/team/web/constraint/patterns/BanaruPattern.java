
package net.banaru.team.web.constraint.patterns;

import java.util.List;

import org.springframework.util.CollectionUtils;

import net.banaru.team.web.constraint.RegexpBuilder;
import net.banaru.team.web.constraint.enums.PatternPreset;

/**
 * @author banssolin
 */
public abstract class BanaruPattern {

    public abstract boolean isRequired();

    public abstract Integer getMin();

    public abstract Integer getMax();

    public abstract List<PatternPreset> getPresets();

    public abstract String getGuideMessageKey();

    public String getRegexp() {
        if (CollectionUtils.isEmpty(getPresets())) {
            return null;
        }
        return RegexpBuilder.buildRegexp(getPresets());
    }

}
