package net.banaru.team.web.constraint.annotation;

import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import net.banaru.team.web.constraint.patterns.BanaruPattern;
import net.banaru.team.web.utils.MessageUtils;

/**
 * Generic<>의 뒤 타입 "String"은 @BanaruPattern를 String 필드 위에 붙이지 않으면 에러가 발생한다.
 *
 * @author banssolin
 */
public class BPatternValidator implements ConstraintValidator<BPattern, String> {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private final MessageUtils messageUtils;

    private BanaruPattern pattern;

    /**
     * Constructor와 initialize()는
     * 어플리케이션이 기동 된 후에 최초 실행 시, "constraintAnnotation.value()"의 값 별로 한 번 만 실행 한다.
     * 같은 value가 여러 개 이여도 한 번만 실행 된다.
     * 실행할 때 Bean 주입이 가능하다.
     */
    public BPatternValidator(MessageUtils messageUtils) {
        this.messageUtils = messageUtils;
    }

    // 어플리케이션이 기동 된 후에 최초 실행시 한 번 만 실행 한다.
    @Override
    public void initialize(BPattern constraintAnnotation) {
        pattern = constraintAnnotation.value().getPattern();
    }

    // http://highcode.tistory.com/6
    // (): 소괄호 안의 문자를 하나의 문자로 인식
    // []: 문자의 집합이나 범위를 나타내며, 두 문자 사이는 - 기호로 범위를 나타낸다. []내에서 ^가 선행하여 존재하면 not 을 나타낸다.
    // *: 앞 문자가 없을 수도 무한정 많을 수도 있음
    // $: 문자열의 종료
    // ?: 앞 문자가 없거나 하나있음
    // .: 임의의 한 문자 (문자의 종류 가리지 않음). 단, \ 는 넣을 수 없음

    // e.g.>
    // URL: "^(https?):\\/\\/([^:\\/\\s]+)(:([^\\/]*))?((\\/[^\\s/\\/]+)*)?\\/?([^#\\s\\?]*)(\\?([^#\\s]*))?(#(\\w*))?$"
    // e-mail: "^([a-zA-Z0-9_\\.\\-+])+@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+$"
    // match-bluesea: "[문자열]*"
    // match-blueland: preset 있는경우: "(문자열*)?", 없는 경우 "(문자열)?"
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (pattern.isRequired() && StringUtils.isEmpty(value)) {
            // TODO Check return false or exception.
            buildMessageTemplate(context, "banaru.pattern.required");
            return false;
        }

        // 필수가 아니지만 null 인 경우 체크하지 않는다
        if (value == null) {
            return true;
        }

        if (!value.matches(pattern.getRegexp())) {
            buildRegexpMessageTemplate(context);
            return false;
        }

        if (value.length() < pattern.getMin() || value.length() > pattern.getMax()) {
            // 최소, 최대 둘 중 하나라도 1보다 작은 경우
            if (pattern.getMin() < 1 || pattern.getMax() < 1) {
                // 최소, 최대 하나라도 1보다 작은 경우(둘 다 0인 경우): 입력 불가
                if (pattern.getMin() < 1 && pattern.getMax() < 1) {
                    buildMessageTemplate(context, "banaru.pattern.no.size", pattern.getMax());
                }
                // 최소, 최대 중 하나만 설정 된 경우
                else {
                    if (pattern.getMin() >= 1) {
                        buildMessageTemplate(context, "banaru.pattern.min", pattern.getMin());
                    } else if (pattern.getMax() >= 1) {
                        buildMessageTemplate(context, "banaru.pattern.max", pattern.getMax());
                    }
                }
            } else {
                // 최소, 최대치가 같은 경우
                if (pattern.getMin().equals(pattern.getMax())) {
                    buildMessageTemplate(context, "banaru.pattern.specified.size", pattern.getMin());
                }
                // 보통의 경우
                else {
                    buildMessageTemplate(context, "banaru.pattern.size", pattern.getMin(), pattern.getMax());
                }
            }
            return false;
        }

        return true;
    }

    private void buildRegexpMessageTemplate(ConstraintValidatorContext context) {
        boolean isCombination = pattern.getPresets().size() > 1;
        if (isCombination) {
            List<String> presetNameKeys = new ArrayList<>();
            pattern.getPresets().forEach(preset -> {
                presetNameKeys.add(messageUtils.getMessage(preset.getNameKey()));
            });
            buildMessageTemplate(context, "banaru.pattern.regexp", String.join(", ", presetNameKeys));
        } else {
            buildMessageTemplate(context, pattern.getGuideMessageKey());
        }
    }

    private void buildMessageTemplate(ConstraintValidatorContext context, String messageKey, Object... messageArgs) {
        final String message = messageUtils.getMessage(messageKey, messageArgs);
        logger.error(message);
        context.disableDefaultConstraintViolation();
        context
            .buildConstraintViolationWithTemplate(message)
            .addConstraintViolation();
    }

}
