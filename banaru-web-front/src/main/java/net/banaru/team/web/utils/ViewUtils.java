package net.banaru.team.web.utils;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;

import net.banaru.team.web.properties.LocaleProperties;


/**
 * @author banssolin
 */
public class ViewUtils {

    @Autowired
    private LocaleProperties localeProperties;

    public Map<String, String> getSupportedCountryNameMap() {
        Map<String, String> countryMap = new HashMap<>();
        for (String countryCode : localeProperties.getSupportedCountryCodes()) {
            countryMap.put(countryCode, getCountryName(countryCode));
        }
        return countryMap;
    }

    public String getCountryName(String countryCode) {
        return getCountryName(countryCode, LocaleContextHolder.getLocale());
    }

    public String getCountryName(String countryCode, Locale locale) {
        return FormatUtils.getCountryName(countryCode, locale);
    }

}
