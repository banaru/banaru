package net.banaru.team.web.configuration;

import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import net.banaru.team.logger.EnableLoggerInjector;
import net.banaru.team.service.configuration.JpaConfiguration;
import net.banaru.team.web.configuration.security.SecurityConfiguration;
import net.banaru.team.web.interceptor.GlobalAttributeInterceptor;
import net.banaru.team.web.utils.ViewUtils;

/**
 * @author banssolin
 */
@Configuration
// @ComponentScan의 범위는 「net.banaru.team.web」의 아래 패키지기 때문에 수동으로 설정파일을 읽어들인다.
@Import({
        // MybatisConfiguration.class,
        SecurityConfiguration.class, JpaConfiguration.class
})
@EnableConfigurationProperties({
        ServerProperties.class
})
@EnableLoggerInjector
// @ComponentScan("net.banaru.team.web")
public class FrontConfiguration implements WebMvcConfigurer {

    // for external tomcat
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    // ===================================
    // Resources
    // ===================================


    private final String RESOURCE_LOCATION_PATTERN = "/static/**";

    private final String[] RESOURCE_LOCATIONS = { "classpath:/static/" };

    // For exclude path patterns of Interceptors.
    // Default: org.springframework.boot.autoconfigure.web.ResourceProperties, 
    // "/js/**"(Default), "/static/js/**"(Additional)의 두 패턴을 모두 핸들링 할 수 있게 된다.
    // Override을 하지 않으면 "/static"이 prefix인 주소를 핸들링 하지 못한다.
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // Add resources handler to default resources handlers.
        registry
                .addResourceHandler(RESOURCE_LOCATION_PATTERN)
                .addResourceLocations(RESOURCE_LOCATIONS);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry
                .addInterceptor(globalAttributeInterceptor())
                .excludePathPatterns(RESOURCE_LOCATION_PATTERN);
    }

    // ===================================
    // Beans
    // ===================================

    @Bean
    public GlobalAttributeInterceptor globalAttributeInterceptor() {
        return new GlobalAttributeInterceptor();
    }

    @Bean
    public ViewUtils viewUtils() {
        return new ViewUtils();
    }

}
