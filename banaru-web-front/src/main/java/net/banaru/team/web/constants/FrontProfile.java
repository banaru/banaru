package net.banaru.team.web.constants;

/**
 * @author banssolin
 */
public class FrontProfile {

    // ==========================================
    // STAGING
    // ==========================================
    public static final String STANDALONE = "standalone";
    public static final String STAGING = "staging";

    // ==========================================
    // DATABASE
    // ==========================================
    public static final String H2 = "h2";

    // ==========================================
    // CONNECTOR
    // ==========================================
    public static final String JPA = "jpa";
    public static final String MYBATIS = "mybatis";

}
