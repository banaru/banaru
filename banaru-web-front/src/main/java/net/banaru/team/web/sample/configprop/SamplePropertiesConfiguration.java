package net.banaru.team.web.sample.configprop;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author banssolin
 */
@Configuration
@EnableConfigurationProperties(SampleProperties.class)
public class SamplePropertiesConfiguration {

}
