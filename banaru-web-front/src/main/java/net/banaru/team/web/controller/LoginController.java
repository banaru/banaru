package net.banaru.team.web.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import net.banaru.team.web.constants.PathConstants;
import net.banaru.team.web.dto.LoginForm;
import net.banaru.team.web.utils.SecurityUtils;
import net.banaru.team.web.configuration.security.UserDetailsImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author banssolin
 */
@Controller
public class LoginController extends AbstractController {

    private static final String LOGIN_FORM = "LOGIN_FORM";

    private final SecurityUtils securityUtils;

    public LoginController(SecurityUtils securityUtils) {
        this.securityUtils = securityUtils;
    }

    /**
     * 로그인이 실패하면 다시 이 url로 들어온다.
     *
     * @param userDetail the user detail implements
     * @return the string
     */
    @GetMapping(PathConstants.LOGIN)
    public String viewLogin(@ModelAttribute LoginForm loginForm, @AuthenticationPrincipal UserDetailsImpl userDetail,
                            HttpServletRequest request, Model model) {

        // If it has already been logged in.
        if (userDetail != null) {
            logger.debug("Login page is allowed anonymous ");
            return getRedirectPath(PathConstants.MYPAGE);
        }

        // Get saved id and set to form.
        {
            String savedId = securityUtils.getSavedIdFromCookie(request);
            if (!StringUtils.isEmpty(savedId)) {
                loginForm.setMemberId(savedId);
                loginForm.setSaveId(true);
            }
        }

        // Exception Object가 반환, 어느 에러인지 판단 해서 화면에 알려줄 수 있다.
        //Object attribute = getSessionAttribute(request, WebAttributes.AUTHENTICATION_EXCEPTION);

        model.addAttribute(LOGIN_FORM, loginForm);

        return "/login";
    }

    // TODO to utils
    private Object getSessionAttribute(HttpServletRequest request, String name) {
        if (request == null) {
            throw new IllegalArgumentException("Request must not be null");
        }
        HttpSession session = request.getSession(false);
        return (session != null ? session.getAttribute(name) : null);
    }

}
