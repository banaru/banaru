
package net.banaru.team.web.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;

/**
 * @author banssolin
 */
public class LoginForm extends BaseForm {

    private static final long serialVersionUID = -1754806873250707738L;

    @Max(100)
    @NotBlank
    private String memberId;

    @Max(100)
    @NotBlank
    private String password;

    private boolean saveId;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isSaveId() {
        return saveId;
    }

    public void setSaveId(boolean saveId) {
        this.saveId = saveId;
    }

}
