
package net.banaru.team.web.dto;

import net.banaru.team.web.constraint.annotation.BPattern;
import net.banaru.team.web.constraint.enums.BPatterns;
import net.banaru.team.service.dto.BaseDto;

/**
 * @author banssolin
 */
public class NameForm implements BaseDto {

    private static final long serialVersionUID = 51546827954793148L;

    @BPattern(BPatterns.NAME1)
    private String name1;
    @BPattern(BPatterns.NAME2)
    private String name2;
    @BPattern(BPatterns.NAME3)
    private String name3;
    @BPattern(BPatterns.NAME4)
    private String name4;

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getName3() {
        return name3;
    }

    public void setName3(String name3) {
        this.name3 = name3;
    }

    public String getName4() {
        return name4;
    }

    public void setName4(String name4) {
        this.name4 = name4;
    }

}
