package net.banaru.team.web.controller.sample;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import net.banaru.team.web.controller.AbstractController;

/**
 * @author banssolin
 */
@Controller
public class ParsleySampleController extends AbstractController {

    @GetMapping("/sample/parsley")
    public String viewParsleySample() {
        return "/sample/parsley/parsley_sample";
    }

}
