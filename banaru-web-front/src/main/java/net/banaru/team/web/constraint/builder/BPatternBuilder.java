package net.banaru.team.web.constraint.builder;

import java.lang.annotation.Annotation;

import org.springframework.util.Assert;

import net.banaru.team.web.constraint.ConstraintInfo;
import net.banaru.team.web.constraint.annotation.BPattern;
import net.banaru.team.web.constraint.enums.BPatterns;
import net.banaru.team.web.constraint.patterns.BanaruPattern;

/**
 * @author banssolin
 */
public class BPatternBuilder implements ConstraintBuilder<BPattern> {

    @Override
    public Class<BPattern> getConstraintClass() {
        return BPattern.class;
    }

    @Override
    public int getPrecedence() {
        return 5;
    }

    @Override
    public void build(Annotation constraint, ConstraintInfo info) {
        Assert.notNull(info, "ConstraintInfo must not be null.");
        BanaruPattern pattern = BPatterns.valueOf(getValue(constraint).toString()).getPattern();
        info.setRequired(pattern.isRequired());
        info.setMinlength(pattern.getMin());
        info.setMaxlength(pattern.getMax());
        // result.replaceAll(java.util.regex.Pattern.quote(block.javaBlockExpression), block.jsBlockExpression);
        info.setPattern(pattern.getRegexp());
    }

}
