package net.banaru.team.web.configuration.error;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.ModelAndView;

import net.banaru.team.web.properties.MvcProperties;

/**
 * @author banssolin
 */
@Configuration // for autowired
public class ErrorViewResolverImpl implements ErrorViewResolver {

    private final MvcProperties mvcProperties;

    public ErrorViewResolverImpl(MvcProperties mvcProperties) {
        this.mvcProperties = mvcProperties;
    }

    @Override
    public ModelAndView resolveErrorView(HttpServletRequest request, HttpStatus status, Map<String, Object> model) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("status", status);

        switch (status) {
            case NOT_FOUND:
                mv.setViewName(getPath(status));
                break;
            default:
                mv.setViewName(getPath(HttpStatus.INTERNAL_SERVER_ERROR));
                break;
        }

        mv.addAllObjects(model);
        return mv;
    }

    private String getPath(HttpStatus error) {
        return mvcProperties.getErrorTemplatePath()
            .concat("/")
            .concat(String.valueOf(error.value()));
    }

}
