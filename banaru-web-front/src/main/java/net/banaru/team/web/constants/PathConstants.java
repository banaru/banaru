package net.banaru.team.web.constants;

public class PathConstants {

    // ==============================
    // Prefix
    // ==============================
    private static final String API = "/api";

    public static final String INDEX = "/";

    // ==============================
    // Login
    // ==============================

    public static final String LOGIN = "/login";

    public static final String LOGIN_PROCESS = LOGIN + "/process";

    public static final String LOGOUT = "/logout";

    // ==============================
    // Member
    // ==============================

    private static final String MEMBER = "/member";

    public static final String MEMBER_REGISTER = MEMBER + "/register";
    public static final String MEMBER_REGISTER_INPUT = MEMBER_REGISTER + "/input";
    public static final String MEMBER_REGISTER_CONFIRM = MEMBER_REGISTER + "/confirm";
    public static final String MEMBER_REGISTER_COMPLETE = MEMBER_REGISTER + "/complete";

    // ==============================
    // My page
    // ==============================

    public static final String MYPAGE = "/mypage";

    public static final String MYPAGE_MEMBER = MYPAGE + MEMBER;

    public static final String MYPAGE_PROFILE = MYPAGE + "/profile";

    public static final String MYPAGE_MEMBER_UPDATE = MYPAGE_MEMBER + "/update";

    public static final String MYPAGE_MEMBER_PASSWORD = MYPAGE_MEMBER + "/password";

    public static final String MYPAGE_ORDER = MYPAGE + "/order";

    public static final String MYPAGE_WISHLIST = MYPAGE + "/wishlist";

    // ==============================
    // My page - Address book
    // ==============================

    public static final String MYPAGE_ADDRESS_BOOK = MYPAGE + "/address";

    public static final String MYPAGE_ADDRESS_BOOK_EDIT = MYPAGE_ADDRESS_BOOK + "/edit";

    public static final String API_MYPAGE_ADDRESS_BOOK = API + MYPAGE_ADDRESS_BOOK;

    public static final String API_MYPAGE_ADDRESS_BOOK_DELETE = API_MYPAGE_ADDRESS_BOOK + "/delete";

}
