package net.banaru.team.web.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import net.banaru.team.web.dto.MemberEditForm;

/**
 * @author banssolin
 */
@Component
public class MemberEditFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return MemberEditForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        // MemberEditForm form = (MemberEditForm) target;
    }

}
