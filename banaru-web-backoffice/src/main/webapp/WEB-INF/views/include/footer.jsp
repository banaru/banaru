<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="footer">
  <div class="pull-right">
    10GB of
    <strong>250GB</strong>
    Free.
  </div>
  <div>
    <strong>Copyright</strong>
    Example Company &copy; 2014-2017
  </div>
</div>