<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link href="<c:url value="/static/css/bootstrap.min.css"/>" rel="stylesheet"/>
<link href="<c:url value="/static/font-awesome/css/font-awesome.css"/>" rel="stylesheet"/>
<!-- Toastr style -->
<link href="<c:url value="/static/css/plugins/toastr/toastr.min.css"/>" rel="stylesheet"/>
<!-- Gritter -->
<link href="<c:url value="/static/js/plugins/gritter/jquery.gritter.css"/>" rel="stylesheet"/>
<link href="<c:url value="/static/css/animate.css"/>" rel="stylesheet"/>
<link href="<c:url value="/static/css/style.css"/>" rel="stylesheet"/>
