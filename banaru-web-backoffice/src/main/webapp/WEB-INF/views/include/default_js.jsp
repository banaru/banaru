<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script src="<c:url value="/static/js/jquery-3.1.1.min.js"/>"></script>
<script src="<c:url value="/static/js/bootstrap.min.js"/>"></script>
<!-- Mainly scripts -->
<script src="<c:url value="/static/js/jquery-3.1.1.min.js"/>"></script>
<script src="<c:url value="/static/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/static/js/plugins/metisMenu/jquery.metisMenu.js"/>"></script>
<script src="<c:url value="/static/js/plugins/slimscroll/jquery.slimscroll.min.js"/>"></script>
<!-- Custom and plugin javascript -->
<script src="<c:url value="/static/js/inspinia.js"/>"></script>
<script src="<c:url value="/static/js/plugins/pace/pace.min.js"/>"></script>

<!-- Banaru Custom Plugin Javascript -->
<script src="<c:url value="/static/custom-plugins/parsleyjs/parsley.min.js"/>"></script>
<script src="<c:url value="/static/custom-js/banaru_custom.js"/>"></script>
