<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script src="<c:url value="/static/js/jquery-3.1.1.min.js"/>"></script>
<script src="<c:url value="/static/js/bootstrap.min.js"/>"></script>
<!-- Mainly scripts -->
<script src="<c:url value="/static/js/plugins/metisMenu/jquery.metisMenu.js"/>"></script>
<script src="<c:url value="/static/js/plugins/slimscroll/jquery.slimscroll.min.js"/>"></script>
<!-- Flot -->
<script src="<c:url value="/static/js/plugins/flot/jquery.flot.js"/>"></script>
<script src="<c:url value="/static/js/plugins/flot/jquery.flot.tooltip.min.js"/>"></script>
<script src="<c:url value="/static/js/plugins/flot/jquery.flot.spline.js"/>"></script>
<script src="<c:url value="/static/js/plugins/flot/jquery.flot.resize.js"/>"></script>
<script src="<c:url value="/static/js/plugins/flot/jquery.flot.pie.js"/>"></script>
<!-- Peity -->
<script src="<c:url value="/static/js/plugins/peity/jquery.peity.min.js"/>"></script>
<script src="<c:url value="/static/js/demo/peity-demo.js"/>"></script>
<!-- Custom and plugin javascript -->
<script src="<c:url value="/static/js/inspinia.js"/>"></script>
<script src="<c:url value="/static/js/plugins/pace/pace.min.js"/>"></script>
<!-- jQuery UI -->
<script src="<c:url value="/static/js/plugins/jquery-ui/jquery-ui.min.js"/>"></script>
<!-- GITTER -->
<script src="<c:url value="/static/js/plugins/gritter/jquery.gritter.min.js"/>"></script>
<!-- Sparkline -->
<script src="<c:url value="/static/js/plugins/sparkline/jquery.sparkline.min.js"/>"></script>
<!-- Sparkline demo data -->
<script src="<c:url value="/static/js/demo/sparkline-demo.js"/>"></script>
<!-- ChartJS-->
<script src="<c:url value="/static/js/plugins/chartJs/Chart.min.js"/>"></script>
<!-- Toastr -->
<script src="<c:url value="/static/js/plugins/toastr/toastr.min.js"/>"></script>
