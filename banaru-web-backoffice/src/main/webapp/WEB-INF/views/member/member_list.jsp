<%@ page import="net.banaru.team.service.constants.MemberSort" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:import url="/WEB-INF/views/layout/default_layout.jsp">

  <c:param name="css">
    <link href="<c:url value="/static/css/plugins/dataTables/datatables.min.css"/>" rel="stylesheet">
  </c:param>

  <c:param name="content">
    <div class="wrapper wrapper-content animated fadeInRight">
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Basic Data Tables example with responsive plugin</h5>
              <div class="ibox-tools">
                <a class="collapse-link">
                  <i class="fa fa-chevron-up"></i>
                </a>
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  <i class="fa fa-wrench"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                  <li>
                    <a href="#">Config option 1</a>
                  </li>
                  <li>
                    <a href="#">Config option 2</a>
                  </li>
                </ul>
                <a class="close-link">
                  <i class="fa fa-times"></i>
                </a>
              </div>
            </div>
            <div class="ibox-content">

              <div class="table-responsive ">
                <div class="dataTables_wrapper form-inline dt-bootstrap">

                  <div class="html5buttons">
                    <div class="dt-buttons btn-group">
                      <a class="btn btn-default buttons-copy buttons-html5" tabindex="0"
                         aria-controls="DataTables_Table_0" href="#">
                        <span>Copy</span>
                      </a>
                      <a class="btn btn-default buttons-csv buttons-html5" tabindex="0"
                         aria-controls="DataTables_Table_0" href="#">
                        <span>CSV</span>
                      </a>
                      <a class="btn btn-default buttons-excel buttons-html5" tabindex="0"
                         aria-controls="DataTables_Table_0" href="#">
                        <span>Excel</span>
                      </a>
                      <a class="btn btn-default buttons-pdf buttons-html5" tabindex="0"
                         aria-controls="DataTables_Table_0" href="#">
                        <span>PDF</span>
                      </a>
                      <a class="btn btn-default buttons-print" tabindex="0" aria-controls="DataTables_Table_0" href="#">
                        <span>Print</span>
                      </a>
                    </div>
                  </div>

                  <div class="dataTables_length" id="DataTables_Table_0_length">
                    <label>Show
                      <select id="table_size" name="DataTables_Table_0_length" aria-controls="DataTables_Table_0"
                              class="form-control form-control-sm">
                        <option
                          data-list-size-url="<c:url value="${membersPage.getSizeUrl(10)}" />" ${membersPage.size == 10 ? 'selected' : ''}>
                          10
                        </option>
                        <option
                          data-list-size-url="<c:url value="${membersPage.getSizeUrl(25)}" />" ${membersPage.size == 25 ? 'selected' : ''}>
                          25
                        </option>
                        <option
                          data-list-size-url="<c:url value="${membersPage.getSizeUrl(50)}" />" ${membersPage.size == 50 ? 'selected' : ''}>
                          50
                        </option>
                        <option
                          data-list-size-url="<c:url value="${membersPage.getSizeUrl(100)}" />" ${membersPage.size == 100 ? 'selected' : ''}>
                          100
                        </option>
                      </select>
                    </label>
                  </div>

                  <div class="dataTables_filter">
                    <label>Search:
                      <input type="search" class="form-control input-sm" placeholder=""
                             aria-controls="DataTables_Table_0">
                    </label>
                  </div>

                  <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">
                    <c:set var="offset" value="${membersPage.pageable.offset}"/>
                    <c:set var="lastElement"
                           value="${offset + membersPage.size > membersPage.totalElements ? membersPage.totalElements: offset + membersPage.size}"/>
                    Showing ${offset + 1} to ${lastElement} of ${membersPage.totalElements} entries
                  </div>

                  <table class="table table-striped table-bordered table-hover dataTables-example dataTable">
                    <thead>
                    <tr>
                      <c:set var="memberNoDirection" value="${sortMap.get(MemberSort.MEMBER_NUMBER)}"/>
                      <th class="sorting${memberNoDirection == null ? '' : '_'.concat(memberNoDirection)}"
                          data-list-sort="${MemberSort.MEMBER_NUMBER}"
                          data-list-sort-direction="${memberNoDirection}"
                          style="width: 10%">
                        <spring:message code="label.member.no"/>
                      </th>
                      <c:set var="idDirection" value="${sortMap.get(MemberSort.MEMBER_ID)}"/>
                      <th class="sorting${idDirection == null ? '' : '_'.concat(idDirection)}"
                          data-list-sort="${MemberSort.MEMBER_ID}"
                          data-list-sort-direction="${idDirection}">
                        <spring:message code="label.member.id"/>
                      </th>
                      <c:set var="nameDirection" value="${sortMap.get(MemberSort.NAME)}"/>
                      <th class="sorting${nameDirection == null ? '' : '_'.concat(nameDirection)}"
                          data-list-sort="${MemberSort.NAME}"
                          data-list-sort-direction="${nameDirection}">
                        <spring:message code="label.member.name"/>
                      </th>
                      <c:set var="emailDirection" value="${sortMap.get(MemberSort.EMAIL)}"/>
                      <th class="sorting${emailDirection == null ? '' : '_'.concat(emailDirection)}"
                          data-list-sort="${MemberSort.EMAIL}"
                          data-list-sort-direction="${emailDirection}">
                        <spring:message code="label.email"/>
                      </th>
                      <c:set var="memberStatusDirection" value="${sortMap.get(MemberSort.MEMBER_STATUS)}"/>
                      <th class="sorting${memberStatusDirection == null ? '' : '_'.concat(memberStatusDirection)}"
                          data-list-sort="${MemberSort.MEMBER_STATUS}"
                          data-list-sort-direction="${memberStatusDirection}">
                        <spring:message code="label.member.status"/>
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="member" items="${membersPage.content}" varStatus="varStatus">
                      <tr class="gradeA ${varStatus.index % 2 == 0 ? 'odd' : 'even'}" role="row">
                        <td>${member.memberNo}</td>
                        <td>${member.memberId}</td>
                        <td>${member.name.name1}</td>
                        <td>${member.email}</td>
                        <td>${member.memberStatus}</td>
                      </tr>
                    </c:forEach>
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>
                        <spring:message code="label.mobile.no"/>
                      </th>
                      <th>
                        <spring:message code="label.member.id"/>
                      </th>
                      <th>
                        <spring:message code="label.member.name"/>
                      </th>
                      <th>
                        <spring:message code="label.email"/>
                      </th>
                      <th>
                        <spring:message code="label.member.status"/>
                      </th>
                    </tr>
                    </tfoot>
                  </table>

                  <div class="dataTables_paginate paging_simple_numbers">
                    <ul class="pagination">
                      <c:if test="${membersPage.hasPreviousChapter}">
                        <%-- <li class="paginate_button previous disabled"> --%>
                        <li class="paginate_button previous">
                          <a href="<c:url value="${membersPage.getPageUrl(membersPage.chapterStartPage - 1)}" />"
                             tabindex="0">Previous
                          </a>
                        </li>
                      </c:if>
                      <c:forEach var="page" begin="${membersPage.chapterStartPage}" end="${membersPage.chapterEndPage}">
                        <c:choose>
                          <c:when test="${(membersPage.number + 1) == page}">
                            <li class="paginate_button active">
                              <a tabindex="0">${page}</a>
                            </li>
                          </c:when>
                          <c:otherwise>
                            <li class="paginate_button">
                              <a href="<c:url value="${membersPage.getPageUrl(page)}" />" tabindex="0">${page}</a>
                            </li>
                          </c:otherwise>
                        </c:choose>
                      </c:forEach>
                      <c:if test="${membersPage.hasNextChapter}">
                        <li class="paginate_button next">
                          <a href="<c:url value="${membersPage.getPageUrl(membersPage.chapterEndPage + 1)}" />"
                             tabindex="0">
                            <spring:message code="btn.next"/>
                          </a>
                        </li>
                      </c:if>
                    </ul>
                  </div>

                </div>

              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </c:param>

  <c:param name="script">

    <script src="<c:url value="/static/js/plugins/dataTables/datatables.min.js"/>"></script>

    <!-- Page-Level Scripts -->
    <script>
      $(function () {

        $('#table_size').change(function () {
          location.href = $(this).children(':selected').data('list-size-url');
        });

        $('[data-list-sort]').click(function () {

          var sortDirection = $(this).data('list-sort-direction');

          if ('asc' === sortDirection) {
            // 'asc' to 'desc'
            sortDirection = 'desc';

          } else if ('desc' === sortDirection) {
            // 'desc' to 'none'
            sortDirection = '';

          } else {
            // 'none' to 'asc'
            sortDirection = 'asc';
          }

          $(this).data('list-sort-direction', sortDirection);

          var paramArray = [];

          $('[data-list-sort]').each(function () {
            var $this = $(this);
            var sortDirection = $this.data('list-sort-direction');
            if (sortDirection) {
              var sortValue = $this.data('list-sort');
              sortValue += ',';
              sortValue += sortDirection;

              paramArray.push(sortValue);
            }
          });

          var baseUrl = '<c:url value="${membersPage.getBaseUrl()}" />';
          baseUrl += '?';
          baseUrl += $.param({sort: paramArray}, true);

          location.href = baseUrl;
        });

      });
    </script>
  </c:param>

</c:import>
