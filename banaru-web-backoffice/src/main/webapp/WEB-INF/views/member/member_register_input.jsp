<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/views/layout/simple_layout.jsp">

  <c:param name="title" value="INSPINIA | Register"/>

  <c:param name="content">
    <h3>Register to IN+</h3>
    <p>Create account to see it in action.</p>
    <form action="<c:url value="/member/register/input" />"
          method="post"
          class="m-t"
          role="form">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Name" required="">
      </div>
      <div class="form-group">
        <input type="email" class="form-control" placeholder="Email" required="">
      </div>
      <div class="form-group">
        <input type="password" class="form-control" placeholder="Password" required="">
      </div>
      <div class="form-group">
        <div class="checkbox i-checks">
          <label>
            <input type="checkbox">
            <i></i>
            Agree the terms and policy
          </label>
        </div>
      </div>
      <button type="submit" class="btn btn-primary block full-width m-b">Register</button>

      <p class="text-muted text-center">
        <small>Already have an account?</small>
      </p>
      <a href="<c:url value="/login"/>"
         class="btn btn-sm btn-white btn-block">Login</a>
    </form>
  </c:param>

  <c:param name="script">
    <!-- iCheck -->
    <script src="<c:url value="/static/js/plugins/iCheck/icheck.min.js"/>"></script>
    <script>
      $(document).ready(function () {
        $('.i-checks').iCheck({
          checkboxClass: 'icheckbox_square-green',
          radioClass: 'iradio_square-green',
        });
      });
    </script>
  </c:param>

</c:import>
