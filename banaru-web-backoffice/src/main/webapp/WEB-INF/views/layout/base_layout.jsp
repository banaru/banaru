<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  ${param.meta}

  <title>${param.title}</title>

  ${param.css}

</head>

<body ${param.body_class}>

  ${param.body}

  <%-- It's work only 1depth --%>
  ${param.script}

</body>

</html>
