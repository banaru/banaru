<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/views/layout/base_layout.jsp">

  <c:param name="meta">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  </c:param>

  <c:param name="title" value="${param.title}"/>

  <c:param name="css">
    <c:import url="/WEB-INF/views/include/default_css.jsp"/>
    ${param.css}
  </c:param>

  <c:param name="body">
    <div id="wrapper">

      <c:import url="/WEB-INF/views/include/navbar.jsp"/>

      <div id="page-wrapper" class="gray-bg">
        <c:import url="/WEB-INF/views/include/header.jsp"/>
        <c:import url="/WEB-INF/views/include/subheader.jsp"/>

          ${param.content}

        <c:import url="/WEB-INF/views/include/footer.jsp"/>
      </div>

      <c:import url="/WEB-INF/views/include/small_chat_box.jsp"/>
      <c:import url="/WEB-INF/views/include/right_sidebar.jsp"/>

    </div>
  </c:param>

  <c:param name="script">
    <c:import url="/WEB-INF/views/include/default_js.jsp"/>
    ${param.script}
  </c:param>

</c:import>
