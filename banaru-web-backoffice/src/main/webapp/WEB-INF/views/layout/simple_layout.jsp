<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/views/layout/base_layout.jsp">

  <c:param name="title" value="${param.title}"/>

  <c:param name="css">
    <c:import url="/WEB-INF/views/include/default_css.jsp"/>
    ${param.css}
  </c:param>

  <c:param name="body_class" value="class=\"gray-bg\""/>
  <c:param name="body">
    <div class="middle-box text-center loginscreen animated fadeInDown">
      <div>
        <div>
          <h1 class="logo-name">IN+</h1>
        </div>
          ${param.content}
        <p class="m-t">
          <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small>
        </p>
      </div>
    </div>
  </c:param>

  <c:param name="script">
    <c:import url="/WEB-INF/views/include/simple_js.jsp"/>
    ${param.script}
  </c:param>

</c:import>
