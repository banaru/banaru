<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/views/layout/default_layout.jsp">

  <c:param name="css">
  </c:param>

  <c:param name="script">
  </c:param>

  <c:param name="content">
  </c:param>

</c:import>
