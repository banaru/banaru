<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/views/layout/simple_layout.jsp">

  <c:param name="title" value="INSPINIA | Login"/>

  <c:param name="content">

    <h3>Welcome to IN+</h3>
    <p>Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
      <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
    </p>
    <p>Login in. To see it in action.</p>
    <form action="<c:url value="/login/process" />"
          method="post"
          class="m-t"
          role="form">
      <div class="form-group">
        <input type="text" name="memberId" class="form-control" placeholder="Member ID" required=""/>
      </div>
      <div class="form-group">
        <input type="password" name="password" class="form-control" placeholder="Password" required="">
      </div>
      <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

      <a href="#"><small>Forgot password?</small></a>
      <p class="text-muted text-center"><small>Do not have an account?</small></p>
      <a href="<c:url value="/member/register/input" />"
         class="btn btn-sm btn-white btn-block">
        Create an account
      </a>
    </form>
  </c:param>

</c:import>
