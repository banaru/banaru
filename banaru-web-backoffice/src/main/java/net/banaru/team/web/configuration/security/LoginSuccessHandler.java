package net.banaru.team.web.configuration.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import net.banaru.team.web.utils.CookieUtils;

/**
 * @author banssolin
 */
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws ServletException, IOException {

        Object principal = authentication.getPrincipal();
        UserDetailsImpl userDetailsImpl = (UserDetailsImpl) principal;

        if (logger.isDebugEnabled()) {
            logger.debug("====== Log in Success ======");
            logger.debug("Member Number: {}", userDetailsImpl.getMemberNo());
            logger.debug("Member ID: {}", userDetailsImpl.getUsername());
            logger.debug("Authorities: {}", userDetailsImpl.getAuthorities());
        }

        // Process after login success

        // 1. Spring security에서 권한이 강제된 페이지 진입 시에, 그 페이지의 정보를 Spring security가 저장해 둠.
        // 2. LoginSuccessHandler에 설정한 defaultTargetUrl로 보냄.
        // 3. defaultTargetUrl가 없을 시 "/"로 보냄
        super.onAuthenticationSuccess(request, response, authentication);
    }

    /**
     * Http <-> Https 간에 세션이 끊기는 것을 방지 하기 위해 session cookie를 secure false로 다시 덮어씀
     */
    private void overrideJsessionId(HttpServletRequest request, HttpServletResponse response) {
        final Cookie jsessionid = CookieUtils.createCookie("JSESSIONID", getSessionId(request), -1);
        jsessionid.setHttpOnly(true);
        CookieUtils.addCookie(response, jsessionid);
    }

    public static String getSessionId(HttpServletRequest request) {
        if (request == null) {
            throw new IllegalArgumentException("Request must not be null");
        }
        HttpSession session = request.getSession(false);
        return (session != null ? session.getId() : null);
    }

}
