package net.banaru.team.web.constants;

public class BackofficePathConstants {

    // ==============================
    // Prefix
    // ==============================
    private static final String API = "/api";

    // ==============================
    // Login
    // ==============================

    public static final String LOGIN = "/login";

    // ==============================
    // Member
    // ==============================

    private static final String MEMBER = "/member";

    public static final String MEMBER_LIST = MEMBER + "/list";

    public static final String MEMBER_REGISTER = MEMBER + "/register";
    public static final String MEMBER_REGISTER_INPUT = MEMBER_REGISTER + "/input";
    public static final String MEMBER_REGISTER_CONFIRM = MEMBER_REGISTER + "/confirm";
    public static final String MEMBER_REGISTER_COMPLETE = MEMBER_REGISTER + "/complete";

}
