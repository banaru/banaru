package net.banaru.team.web.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import net.banaru.team.service.constants.MemberSort;
import net.banaru.team.service.dto.ViewPage;
import net.banaru.team.service.dto.member.MemberDto;
import net.banaru.team.service.member.MemberService;
import net.banaru.team.web.constants.BackofficePathConstants;

@Controller
public class MemberController extends AbstractController {

    private final MemberService memberService;

    public MemberController(MemberService memberService) {
        this.memberService = memberService;
    }

    @GetMapping(BackofficePathConstants.MEMBER_REGISTER_INPUT)
    public String viewRegister() {
        return "/member/member_register_input";
    }

    @PostMapping(BackofficePathConstants.MEMBER_REGISTER_INPUT)
    public String procRegister() {
        return getRedirectPath(BackofficePathConstants.LOGIN);
    }

    @GetMapping(BackofficePathConstants.MEMBER_LIST)
    // https://docs.spring.io/spring-data/jpa/docs/2.1.2.RELEASE/reference/html/#core.web
    public String viewList(Model model, @PageableDefault(page = 1, size = 25) Pageable pageable) {

        Page<MemberDto> membersPage = memberService
            .getMembers(PageRequest.of(pageable.getPageNumber() - 1, pageable.getPageSize(), pageable.getSort()));
        ViewPage<MemberDto> viewPage = ViewPage.of(membersPage, BackofficePathConstants.MEMBER_LIST);
        model.addAttribute("membersPage", viewPage);

        Map<MemberSort, String> sortMap = new HashMap<>();
        for (Sort.Order order : viewPage.getSort()) {

            MemberSort memberSort = MemberSort.valueOf(order.getProperty());
            String orderDirection = order.getDirection().name().toLowerCase();
            sortMap.put(memberSort, orderDirection);
        }
        model.addAttribute("sortMap", sortMap);

        return "/member/member_list";
    }

}
