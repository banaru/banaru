package net.banaru.team.web.controller;

import net.banaru.team.web.configuration.security.UserDetailsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @GetMapping("/")
    public String viewIndex() {
        return "forward:/main";
    }

    @GetMapping("/main")
    public String viewMain(@AuthenticationPrincipal UserDetailsImpl userDetails) {
        if (userDetails != null) {
            logger.debug("Logined ID: {}", userDetails.getUsername());
        }
        return "/index";
    }

}
