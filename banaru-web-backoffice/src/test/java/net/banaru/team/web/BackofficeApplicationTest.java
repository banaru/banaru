package net.banaru.team.web;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import net.banaru.team.web.properties.LocalePropertiesHolder;

@RunWith(SpringRunner.class)
// To avoid h2 configuration
@ActiveProfiles("test")
// using the default value of "file:src/main/webapp" for the path to the root of
// the web application
// https://docs.spring.io/spring/docs/5.1.3.RELEASE/spring-framework-reference/testing.html#spring-testing-annotation-webappconfiguration
@WebAppConfiguration
@ContextConfiguration({
    "classpath*:/spring/*.xml"
})
@TestPropertySource("classpath:backoffice-test.properties")
public class BackofficeApplicationTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MessageSource messageSource;

    @Test
    public void messageSourceTest() {
        Assert.assertNotNull(messageSource);

        Locale systemLocale = LocaleContextHolder.getLocale();
        String systemLanguage = systemLocale.getLanguage();
        logger.debug("system language: {}", systemLanguage);

        // "message_언어" 파일이 없을경우 message.properties 파일이 불림
        // String defaultMessage = messageSource.getMessage("message.test", null, null);
        // logger.debug(defaultMessage);

        String enMessage = messageSource.getMessage("message.test", null, Locale.ENGLISH);
        logger.debug("en language message: {}", enMessage);
        Assert.assertTrue(enMessage.startsWith("en"));

        String jaMessage = messageSource.getMessage("message.test", null, Locale.KOREAN);
        logger.debug("ko language message: {}", jaMessage);
        Assert.assertTrue(jaMessage.startsWith("ko"));

        // 현재 뷰 언어, 브라우저로 열면 default locale이 보여진다. 뷰가 없으므로 시스템언어가 나옴
        String currentLanguage = messageSource.getMessage("message.test", null,
            LocaleContextHolder.getLocale());
        logger.debug("current language message: {}", currentLanguage);
        Assert.assertTrue(currentLanguage.startsWith(systemLanguage));

        // 없는 언어. 디폴트 언어로 나옴
        String notExistLanguage = messageSource.getMessage("message.test", null,
            Locale.FRENCH);
        logger.debug("Not exist language message: {}", notExistLanguage);
        Assert.assertTrue(notExistLanguage.startsWith(systemLanguage));
    }

//    # Override Locale Properties
//    banaru.locale.default-language = ja
//    banaru.locale.supported-languages = ja, en, ko
//    banaru.locale.default-country-code = JP
//    banaru.locale.supported-country-codes = JP, US, KR
//    banaru.locale.time-zone = Asia/Tokyo

    @Autowired
    private LocalePropertiesHolder localePropertiesHolder;

    @Test
    public void LocalePropertiesHolderTest() {

        logger.debug("Default Language: {}", localePropertiesHolder.getDefaultLanguage());
        Assert.assertEquals(Locale.JAPANESE, localePropertiesHolder.getDefaultLanguage());

        logger.debug("Supported Languages: {}", localePropertiesHolder.getSupportedLanguages());
        List<Locale> expectedSupportedLanguages = Arrays
            .asList(Locale.JAPANESE, Locale.ENGLISH, Locale.KOREAN);
        localePropertiesHolder.getSupportedLanguages().forEach(language -> {
            Assert.assertTrue(expectedSupportedLanguages.contains(language));
        });

        logger.debug("Default CountryCode: {}", localePropertiesHolder.getDefaultCountryCode());
        Assert.assertEquals(Locale.JAPAN.getCountry(), localePropertiesHolder.getDefaultCountryCode());

        logger.debug("Supported CountryCodes: {}", localePropertiesHolder.getSupportedCountryCodes());
        List<String> expectedSupportedCountryCodes = Arrays
            .asList(Locale.JAPAN.getCountry(), Locale.US.getCountry(), Locale.KOREA.getCountry());
        localePropertiesHolder.getSupportedCountryCodes().forEach(countryCode -> {
            Assert.assertTrue(expectedSupportedCountryCodes.contains(countryCode));
        });

        logger.debug("Time Zone: {}", localePropertiesHolder.getTimeZone());
        Assert.assertEquals(TimeZone.getTimeZone("Asia/Tokyo"), localePropertiesHolder.getTimeZone());
    }

}
