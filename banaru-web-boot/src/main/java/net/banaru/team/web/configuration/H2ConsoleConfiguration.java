package net.banaru.team.web.configuration;

import javax.servlet.http.HttpServlet;

import org.h2.server.web.WebServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author banssolin
 */
@Profile({ "h2embedded", "h2server" })
@Configuration
public class H2ConsoleConfiguration {

    // application.yml에서 설정하면 security에 걸림
    @Bean
    public ServletRegistrationBean<HttpServlet> h2servletRegistration() {
        ServletRegistrationBean<HttpServlet> registrationBean = new ServletRegistrationBean<>(new WebServlet());
        registrationBean.addUrlMappings("/console/*");
        registrationBean.setLoadOnStartup(1);
        return registrationBean;
    }

}
