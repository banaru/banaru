package net.banaru.team.web.configuration.mapper.converter;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import net.banaru.team.service.constants.YN;

public class YNConverter extends BidirectionalConverter<Boolean, YN> {

    @Override
    public YN convertTo(Boolean source, Type<YN> destinationType, MappingContext mappingContext) {
        return source ? YN.Y : YN.N;
    }

    @Override
    public Boolean convertFrom(YN source, Type<Boolean> destinationType, MappingContext mappingContext) {
        return YN.Y == source;
    }
}
