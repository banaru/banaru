package net.banaru.team.service.enums;

/**
 * @author banssolin
 *
 */
public enum Gender {

    MALE,

    FEMALE

}
