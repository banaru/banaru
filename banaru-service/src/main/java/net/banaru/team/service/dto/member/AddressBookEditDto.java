package net.banaru.team.service.dto.member;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import net.banaru.team.service.dto.AddressDto;
import net.banaru.team.service.dto.BaseDto;
import net.banaru.team.service.dto.NameDto;

/**
 * @author banssolin
 */
public class AddressBookEditDto implements BaseDto {

    @NotNull
    private Long memberNo;

    private String addressBookName;

    @NotNull
    private boolean repAddress;

    @NotNull
    private boolean billingAddress;

    @Valid
    @NotNull
    private NameDto name;

    @Valid
    @NotNull
    private AddressDto address;

    private String countryCode;

    private String phoneNo;

    private String mobileNo;

    public Long getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(Long memberNo) {
        this.memberNo = memberNo;
    }

    public String getAddressBookName() {
        return addressBookName;
    }

    public void setAddressBookName(String addressBookName) {
        this.addressBookName = addressBookName;
    }

    public boolean getRepAddress() {
        return repAddress;
    }

    public void setRepAddress(boolean repAddress) {
        this.repAddress = repAddress;
    }

    public boolean getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(boolean billingAddress) {
        this.billingAddress = billingAddress;
    }

    public NameDto getName() {
        return name;
    }

    public void setName(NameDto name) {
        this.name = name;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

}
