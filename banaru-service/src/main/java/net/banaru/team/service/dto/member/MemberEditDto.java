package net.banaru.team.service.dto.member;

import java.time.LocalDate;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import net.banaru.team.service.dto.BaseDto;
import net.banaru.team.service.dto.NameDto;
import net.banaru.team.service.enums.Gender;

public class MemberEditDto implements BaseDto {

    private static final long serialVersionUID = 8636162470351084300L;

    @NotNull
    private String memberCountryCode;

    @NotNull
    @Valid
    private NameDto name;

    @NotBlank
    private String email;

    private LocalDate birthday;

    private Gender gender;

    private String phoneNo;

    public String getMemberCountryCode() {
        return memberCountryCode;
    }

    public void setMemberCountryCode(String memberCountryCode) {
        this.memberCountryCode = memberCountryCode;
    }

    public NameDto getName() {
        return name;
    }

    public void setName(NameDto name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

}
