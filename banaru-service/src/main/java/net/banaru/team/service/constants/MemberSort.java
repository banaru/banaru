package net.banaru.team.service.constants;

public enum MemberSort {

    MEMBER_NUMBER("memberNo", "member_no"),

    MEMBER_ID("memberId", "member_id"),

    NAME("name.name1", "name1"),

    EMAIL("email", "email"),

    MEMBER_STATUS("memberStatus", "member_status");

    private final String propertyName;

    private final String columnName;

    MemberSort(String propertyName, String columnName) {
        this.propertyName = propertyName;
        this.columnName = columnName;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public String getColumnName() {
        return columnName;
    }

    @Override
    public String toString() {
        return propertyName;
    }

    public static MemberSort valueOfByPropertyName(String propertyName) {
        for (MemberSort memberSort : values()) {
            if (memberSort.getPropertyName().equals(propertyName)) {
                return memberSort;
            }
        }
        throw new IllegalArgumentException("No enum constant: " + propertyName);
    }

    public static MemberSort valueOfByColumnName(String columnName) {
        for (MemberSort memberSort : values()) {
            if (memberSort.getColumnName().equals(columnName)) {
                return memberSort;
            }
        }
        throw new IllegalArgumentException("No enum constant: " + columnName);
    }

}
