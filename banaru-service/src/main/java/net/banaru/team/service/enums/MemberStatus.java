package net.banaru.team.service.enums;

/**
 * @author banssolin
 *
 */
public enum MemberStatus {

    WAIT,

    GENERAL,

    WITHDRAW

}
