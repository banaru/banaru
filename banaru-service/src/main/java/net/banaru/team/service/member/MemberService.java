package net.banaru.team.service.member;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

import net.banaru.team.service.dto.member.MemberDto;
import net.banaru.team.service.dto.member.MemberRegisterDto;
import net.banaru.team.service.dto.member.MemberUpdateDto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author banssolin
 */
/*
 * class에 @Validated
 * method의 parameter에 @Valid(Object의 경우)가 붙어야 체크한다.
 * ConstraintViolationException not working in junit
 */
@Validated
public interface MemberService {

    Long registerMember(@NotNull @Valid MemberRegisterDto member);

    void updateMember(@NotNull @Valid MemberUpdateDto member);

    MemberDto getMember(@NotNull Long memberNo);

    MemberDto getMember(@NotBlank String memberId);

    Page<MemberDto> getMembers(@NotNull Pageable pageable);

    boolean existMemberId(@NotBlank String memberId);

    void changePassword(@NotNull Long memberNo, @NotBlank String password);

}
