
package net.banaru.team.service.dto.member;

import javax.validation.constraints.NotNull;

/**
 * @author banssolin
 */
public class AddressBookUpdateDto extends AddressBookEditDto {

    @NotNull
    private Long addressBookNo;

    public Long getAddressBookNo() {
        return addressBookNo;
    }

    public void setAddressBookNo(Long addressBookNo) {
        this.addressBookNo = addressBookNo;
    }
}
