package net.banaru.team.service.dto;

import java.io.Serializable;

import net.banaru.team.utils.JsonUtils;

/**
 * DTO들의 상속관계를 위해 interface로 생성
 *
 * @author banssolin
 */
public interface BaseDto extends Serializable {

    default String toJson() {
        return JsonUtils.toJson(this, true);
    }

    default String toJson(boolean isPretty) {
        return JsonUtils.toJson(this, isPretty);
    }

}
