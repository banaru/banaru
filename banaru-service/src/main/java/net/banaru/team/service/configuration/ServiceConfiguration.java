package net.banaru.team.service.configuration;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.converter.ConverterFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import net.banaru.team.web.configuration.mapper.converter.YNConverter;

/**
 * @author banssolin
 */
@Configuration
public class ServiceConfiguration {

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public MapperFactory mapperFactory() {
        DefaultMapperFactory.Builder factoryBuilder = new DefaultMapperFactory.Builder();

        ConverterFactory converterFactory = factoryBuilder.build().getConverterFactory();
        // TODO this is necessary?
        // converterFactory.registerConverter(new PassThroughConverter(LocalDate.class));
        converterFactory.registerConverter(new YNConverter());

        return factoryBuilder.build();
    }

    @Bean
    public MapperFacade mapperFacade(MapperFactory mapperFactory) {
        return mapperFactory.getMapperFacade();
    }

}
