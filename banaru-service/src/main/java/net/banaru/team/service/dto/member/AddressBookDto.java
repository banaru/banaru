
package net.banaru.team.service.dto.member;

import java.util.Date;

import net.banaru.team.service.constants.YN;
import net.banaru.team.service.dto.AddressDto;
import net.banaru.team.service.dto.BaseDto;
import net.banaru.team.service.dto.NameDto;

/**
 * @author banssolin
 */
public class AddressBookDto implements BaseDto {

    private static final long serialVersionUID = -8090078875257212502L;

    private Long addressBookNo;

    private Long memberNo;

    private String addressBookName;

    private YN repAddress;

    private YN billingAddress;

    private NameDto name;

    private AddressDto address;

    private String countryCode;

    private String phoneNo;

    private String mobileNo;

    private Date registerDate;

    public Long getAddressBookNo() {
        return addressBookNo;
    }

    public void setAddressBookNo(Long addressBookNo) {
        this.addressBookNo = addressBookNo;
    }

    public Long getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(Long memberNo) {
        this.memberNo = memberNo;
    }

    public String getAddressBookName() {
        return addressBookName;
    }

    public void setAddressBookName(String addressBookName) {
        this.addressBookName = addressBookName;
    }

    public YN getRepAddress() {
        return repAddress;
    }

    public void setRepAddress(YN repAddress) {
        this.repAddress = repAddress;
    }

    public YN getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(YN billingAddress) {
        this.billingAddress = billingAddress;
    }

    public NameDto getName() {
        return name;
    }

    public void setName(NameDto name) {
        this.name = name;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

}
