package net.banaru.team.service.dto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.Assert;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ViewPage<T> extends PageImpl<T> {

    private static final long serialVersionUID = -7499567262082689159L;

    private static final int DEFAULT_PER_PAGE_NUM = 5;
    private final String path;

    public ViewPage(List<T> content, Pageable pageable, long total, String path) {
        super(content, pageable, total);
        this.path = path;
    }

    public ViewPage(List<T> content, String path) {
        super(content);
        this.path = path;
    }

    public static <T> ViewPage<T> of(Page<T> page, String path) {
        return new ViewPage<>(page.getContent(), page.getPageable(), page.getTotalElements(), path);
    }

    public int getCurrentChapter() {

        int number = getNumber() + 1;
        int currentChapter = number / DEFAULT_PER_PAGE_NUM;
        if (number % DEFAULT_PER_PAGE_NUM != 0) {
            currentChapter = currentChapter + 1;
        }

        return currentChapter;
    }

    public boolean getHasPreviousChapter() {
        return getChapterStartPage() > 1;
    }

    public int getChapterStartPage() {

        final int chapterStartPage;
        if (getTotalPages() < DEFAULT_PER_PAGE_NUM) {
            chapterStartPage = 1;
        } else {
            chapterStartPage = (getCurrentChapter() - 1) * DEFAULT_PER_PAGE_NUM + 1;
        }

        return chapterStartPage;
    }

    public int getChapterEndPage() {

        int chapterEndPage = getCurrentChapter() * DEFAULT_PER_PAGE_NUM;
        if (getTotalPages() < chapterEndPage) {
            chapterEndPage = getTotalPages();
        }

        return chapterEndPage;
    }

    public boolean getHasNextChapter() {
        return getChapterEndPage() < getTotalPages();
    }

    public List<String> getSortParams(Sort sort) {

        List<String> sortParams = new ArrayList<>();

        sort.forEach(order -> {
            String sortParam = new StringBuilder()
                .append(order.getProperty())
                .append(",")
                .append(order.getDirection())
                .toString();
            sortParams.add(sortParam);
        });

        return sortParams;
    }

    public String getBaseUrl() {
        return UriComponentsBuilder.fromPath(path).toUriString();
    }

    public String getPageUrl(int page) {
        return getUrl(page, getSize(), getSort());
    }

    public String getSizeUrl(int size) {
        return getUrl(1, size, getSort());
    }

    public String getSortUrl(String sortValue) {
        return getSortUrl(sortValue, Sort.Direction.ASC);
    }

    public String getSortUrl(String sortValue, String direction) {

        Optional<Sort.Direction> optional = Sort.Direction.fromOptionalString(direction);
        return getSortUrl(sortValue, optional.orElse(Sort.Direction.ASC));
    }

    public String getSortUrl(String sortValue, Sort.Direction direction) {

        Assert.hasText(sortValue, "SortValue must not be empty.");

        Sort sort = getSort().and(Sort.by(direction, sortValue));
        return getUrl(1, getSize(), sort);
    }

    private String getUrl(int page, int size, Sort sort) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromPath(path)
                                                           .queryParam("page", page)
                                                           .queryParam("size", size);

        getSortParams(sort).forEach(param -> builder.queryParam("sort", param));

        return builder.toUriString();
    }

    /**
     * For JSP
     *
     * @see org.springframework.data.domain.Slice#hasPrevious()
     */
    public boolean getHasPrevious() {
        return hasPrevious();
    }

    /**
     * For JSP
     *
     * @see org.springframework.data.domain.Slice#hasNext()
     */
    public boolean getHasNext() {
        return hasNext();
    }

}
