
package net.banaru.team.service.dto.member;

import java.time.LocalDate;
import java.util.Date;

import net.banaru.team.service.dto.BaseDto;
import net.banaru.team.service.dto.NameDto;
import net.banaru.team.service.enums.Gender;
import net.banaru.team.service.enums.MemberGrade;
import net.banaru.team.service.enums.MemberStatus;

/**
 * @author banssolin
 */
public class MemberDto implements BaseDto {

    private static final long serialVersionUID = 3522537053988242919L;

    private Long memberNo;

    private String memberId;

    private NameDto name;

    private String password;

    private String email;

    private MemberGrade memberGrade;

    private LocalDate birthday;

    private Gender gender;

    private String phoneNo;

    private String mobileNo;

    private MemberStatus memberStatus;

    private String memberCountryCode;

    private Date registerDate;

    public Long getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(Long memberNo) {
        this.memberNo = memberNo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public NameDto getName() {
        return name;
    }

    public void setName(NameDto name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public MemberGrade getMemberGrade() {
        return memberGrade;
    }

    public void setMemberGrade(MemberGrade memberGrade) {
        this.memberGrade = memberGrade;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public MemberStatus getMemberStatus() {
        return memberStatus;
    }

    public void setMemberStatus(MemberStatus memberStatus) {
        this.memberStatus = memberStatus;
    }

    public String getMemberCountryCode() {
        return memberCountryCode;
    }

    public void setMemberCountryCode(String memberCountryCode) {
        this.memberCountryCode = memberCountryCode;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

}
