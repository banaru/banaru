package net.banaru.team.service.dto.member;

import javax.validation.constraints.NotNull;

/**
 * @author banssolin
 */
public class MemberUpdateDto extends MemberEditDto {

    private static final long serialVersionUID = -1774719187571057895L;

    @NotNull
    private Long memberNo;

    public Long getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(Long memberNo) {
        this.memberNo = memberNo;
    }

}
