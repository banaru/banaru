package net.banaru.team.service.exception.runtime;

/**
 * @author banssolin
 */
public class NotFoundDataException extends RuntimeException {

    private static final long serialVersionUID = -4298355517838004324L;

    public NotFoundDataException() {
    }

    public NotFoundDataException(String message) {
        super(message);
    }

    public NotFoundDataException(String message, Throwable cause) {
        super(message, cause);
    }

}
