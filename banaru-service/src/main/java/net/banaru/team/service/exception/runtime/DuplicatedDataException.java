package net.banaru.team.service.exception.runtime;

/**
 * @author banssolin
 */
public class DuplicatedDataException extends RuntimeException {

    private static final long serialVersionUID = -7702464913951224474L;

    public DuplicatedDataException() {
    }

    public DuplicatedDataException(String message) {
        super(message);
    }

    public DuplicatedDataException(String message, Throwable cause) {
        super(message, cause);
    }

}
