package net.banaru.team.service.enums;

/**
 * @author banssolin
 *
 */
public enum MemberGrade {

    ADMIN,

    GENERAL

}
