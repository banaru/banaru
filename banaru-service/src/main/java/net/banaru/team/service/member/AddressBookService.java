package net.banaru.team.service.member;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;

import net.banaru.team.service.dto.member.AddressBookDto;
import net.banaru.team.service.dto.member.AddressBookRegisterDto;

/**
 * @author banssolin
 */
@Validated
public interface AddressBookService {

    Long registerAddressBook(@Valid @NotNull AddressBookRegisterDto registerDto);

    AddressBookDto getAddressBook(@NotNull Long addressBookNo);

    List<AddressBookDto> getAddressBooks(@NotNull Long memberNo);

    long getAddressBookCount(@NotNull Long memberNo);

    Page<AddressBookDto> getAddressBooks(@NotNull Long memberNo, @NotNull Pageable pageable);

    void deleteAddressBook(@NotNull Long addressBookNo);

}
