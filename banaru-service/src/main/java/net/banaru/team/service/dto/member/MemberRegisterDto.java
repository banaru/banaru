
package net.banaru.team.service.dto.member;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import net.banaru.team.service.enums.MemberGrade;
import net.banaru.team.service.enums.MemberStatus;

/**
 * @author banssolin
 */
public class MemberRegisterDto extends MemberEditDto {

    private static final long serialVersionUID = 1826652685631535858L;

    @NotBlank
    private String memberId;

    @NotBlank
    private String password;

    @NotNull
    private MemberGrade memberGrade;

    @NotNull
    private MemberStatus memberStatus;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public MemberGrade getMemberGrade() {
        return memberGrade;
    }

    public void setMemberGrade(MemberGrade memberGrade) {
        this.memberGrade = memberGrade;
    }

    public MemberStatus getMemberStatus() {
        return memberStatus;
    }

    public void setMemberStatus(MemberStatus memberStatus) {
        this.memberStatus = memberStatus;
    }
}
