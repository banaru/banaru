package net.banaru.team.service.constants;

/**
 * @author banssolin
 */
public class Path {

    /*
     * =============================
     * Common
     * =============================
     */
    public static final String API = "/api";

    /*
     * =============================
     * Member
     * =============================
     */
    public static final String MEMBER = "/member";

    /*
     * =============================
     * API Member
     * =============================
     */
    public static final String API_MEMBER = API + MEMBER;

    public static final String API_MEMBER_REGISTER = API_MEMBER + "/register";

    public static final String API_MEMBER_UPDATE = API_MEMBER + "/update";

    public static final String API_MEMBER_GET = API_MEMBER + "/get";

}
