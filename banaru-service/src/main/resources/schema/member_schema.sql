

/* Create Sequences */

CREATE SEQUENCE SEQ_address_book_address_book_no INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_member_member_no INCREMENT BY 1 START WITH 1;



/* Create Tables */

CREATE TABLE address_book
(
	address_book_no number(19,0) NOT NULL,
	member_no number(19,0) NOT NULL,
	address_book_name varchar2(100),
	rep_address varchar2(1) DEFAULT 'N' NOT NULL,
	billing_address varchar2(1) DEFAULT 'N' NOT NULL,
	name1 varchar2(100) NOT NULL,
	name2 varchar2(100),
	name3 varchar2(100),
	name4 varchar2(100),
	address1 varchar2(200) NOT NULL,
	address2 varchar2(200) NOT NULL,
	address3 varchar2(200) NOT NULL,
	address4 varchar2(200),
	zip_code varchar2(20) NOT NULL,
	country_code varchar2(30),
	phone_no varchar2(20),
	mobile_no varchar2(20),
	register_date timestamp NOT NULL,
	PRIMARY KEY (address_book_no)
);


CREATE TABLE member
(
	member_no number(19,0) NOT NULL,
	member_id varchar2(100) NOT NULL UNIQUE,
	-- BCryptPasswordEncoderを使うためには、少なくとも60桁が必要
	password varchar2(100) NOT NULL,
	email varchar2(100),
	name1 varchar2(100) NOT NULL,
	name2 varchar2(100),
	name3 varchar2(100),
	name4 varchar2(100),
	member_grade varchar2(10),
	birthday date,
	-- - MALE（男性）
	-- - FEMALE（女性）
	gender varchar2(10),
	-- 有線電話、携帯電話2つとも利用
	phone_no varchar2(20),
	mobile_no varchar2(20),
	-- - WAIT（登録待機）
	-- - GENERAL（登録完了）
	-- - WITHDRWA（脱会会員）
	member_status varchar2(10),
	member_country_code varchar2(2),
	register_date timestamp NOT NULL,
	PRIMARY KEY (member_no)
);


CREATE TABLE message
(
	message_no number(19,0) NOT NULL,
	sender_member_no number(19,0) NOT NULL,
	receiver_member_no number(19,0) NOT NULL,
	-- -수신
	-- -발신
	message_type varchar2(10) NOT NULL,
	message_content clob NOT NULL,
	send_date timestamp NOT NULL,
	PRIMARY KEY (message_no)
);



/* Create Foreign Keys */

ALTER TABLE address_book
	ADD FOREIGN KEY (member_no)
	REFERENCES member (member_no)
;


ALTER TABLE message
	ADD FOREIGN KEY (sender_member_no)
	REFERENCES member (member_no)
;


ALTER TABLE message
	ADD FOREIGN KEY (receiver_member_no)
	REFERENCES member (member_no)
;



/* Comments */

COMMENT ON COLUMN member.password IS 'BCryptPasswordEncoderを使うためには、少なくとも60桁が必要';
COMMENT ON COLUMN member.gender IS '- MALE（男性）
- FEMALE（女性）';
COMMENT ON COLUMN member.phone_no IS '有線電話、携帯電話2つとも利用';
COMMENT ON COLUMN member.member_status IS '- WAIT（登録待機）
- GENERAL（登録完了）
- WITHDRWA（脱会会員）';
COMMENT ON COLUMN message.message_type IS '-수신
-발신';



