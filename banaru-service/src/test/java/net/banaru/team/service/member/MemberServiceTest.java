package net.banaru.team.service.member;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import net.banaru.team.service.constants.MemberSort;
import net.banaru.team.service.dto.member.MemberDto;
import net.banaru.team.service.dto.member.MemberRegisterDto;
import net.banaru.team.service.dto.member.MemberUpdateDto;
import net.banaru.team.service.exception.runtime.DuplicatedDataException;
import net.banaru.team.service.exception.runtime.NotFoundDataException;
import net.banaru.team.test.utils.DummyBuilder;

@Transactional
public abstract class MemberServiceTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MemberService memberService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Test
    public void registerMemberTest() {
        registerMember();
    }

    @Test
    public void registerMemberTest_defaultValue() {
        MemberRegisterDto registerDto = DummyBuilder.buildMemberRegisterDto();
        registerDto.setMemberGrade(null);
        registerDto.setMemberStatus(null);
        registerMember(registerDto);
    }

    @Test(expected = DuplicatedDataException.class)
    public void registerMember_duplicatedDataException() {
        registerMember();
        // register by same member id
        registerMember();
    }

    private Long registerMember() {
        MemberRegisterDto registerDto = DummyBuilder.buildMemberRegisterDto();
        return registerMember(registerDto);
    }

    private Long registerMember(MemberRegisterDto registerDto) {

        Long registeredMemberNo = memberService.registerMember(registerDto);
        MemberDto result = memberService.getMember(registeredMemberNo);

        DummyBuilder.assertMemberRegisterDto(bCryptPasswordEncoder, registerDto, result);

        return registeredMemberNo;
    }

    @Test
    public void getMember() {

        MemberRegisterDto registerDto = DummyBuilder.buildMemberRegisterDto();
        Long registeredMemberNo = registerMember(registerDto);

        // Check get member by member number
        MemberDto resultByMemberNo = memberService.getMember(registeredMemberNo);
        Assert.assertNotNull(resultByMemberNo);

        // Check get member by member id
        MemberDto resultById = memberService.getMember(registerDto.getMemberId());
        Assert.assertNotNull(resultById);
    }

    @Test
    public void getMembers() {

        List<MemberRegisterDto> memberRegisterDtos = DummyBuilder.buildMemberRegisterDtos(3);
        PageRequest pageRequest1 = PageRequest.of(0, 2);

        // Member Count: 0
        // Expected page:0, page elements:0, total elements: 0
        Page<MemberDto> result1 = memberService.getMembers(pageRequest1);
        Assert.assertEquals(0, result1.getNumber());
        // page.getNumberOfElements() == page..etContent().size()
        Assert.assertEquals(0, result1.getNumberOfElements());
        Assert.assertEquals(0, result1.getContent().size());
        Assert.assertEquals(0L, result1.getTotalElements());

        // Member Count: 1
        registerMember(memberRegisterDtos.get(0));
        // Expected page:0, page elements:1, total elements: 1
        Page<MemberDto> result2 = memberService.getMembers(pageRequest1);
        Assert.assertEquals(0, result2.getNumber());
        Assert.assertEquals(1, result2.getNumberOfElements());
        Assert.assertEquals(1, result2.getContent().size());
        Assert.assertEquals(1L, result2.getTotalElements());

        // Member Count: 2
        registerMember(memberRegisterDtos.get(1));
        // Expected page:0, page elements:2, total elements: 2
        Page<MemberDto> result3 = memberService.getMembers(pageRequest1);
        Assert.assertEquals(0, result3.getNumber());
        Assert.assertEquals(2, result3.getNumberOfElements());
        Assert.assertEquals(2, result3.getContent().size());
        Assert.assertEquals(2L, result3.getTotalElements());

        // Member Count: 3, Page1
        registerMember(memberRegisterDtos.get(2));
        // Expected page:0, page elements:2, total elements: 3
        Page<MemberDto> result4 = memberService.getMembers(pageRequest1);
        Assert.assertEquals(0, result4.getNumber());
        Assert.assertEquals(2, result4.getNumberOfElements());
        Assert.assertEquals(2, result4.getContent().size());
        Assert.assertEquals(3L, result4.getTotalElements());

        // Member Count: 3, Page2
        PageRequest pageRequest2 = PageRequest.of(1, 2);
        // Expected page:1, page elements:1, total elements: 3
        Page<MemberDto> result5 = memberService.getMembers(pageRequest2);
        Assert.assertEquals(1, result5.getNumber());
        Assert.assertEquals(1, result5.getNumberOfElements());
        Assert.assertEquals(1, result5.getContent().size());
        Assert.assertEquals(3L, result5.getTotalElements());
    }

    @Test
    public void getMembers_sort() {

        // Register members: count 5
        List<MemberRegisterDto> memberRegisterDtos = DummyBuilder.buildMemberRegisterDtos(5);
        memberRegisterDtos.forEach(this::registerMember);

        // Check by Sort.Direction.ASC, MemberSort.NAME
        {
            Sort sort = Sort.by(Sort.Direction.ASC, MemberSort.NAME.getPropertyName());
            Page<MemberDto> result = getMembersBySort(sort);

            memberRegisterDtos.sort(Comparator.comparing(dto -> dto.getName().getName1()));

            logger.debug("Check get member by sort: Sort.Direction.ASC, MemberSort.NAME");
            for (int i = 0; i < memberRegisterDtos.size(); i++) {

                logger.debug("Check sort: target member name1 {}, result member name1 {}",
                    memberRegisterDtos.get(i).getName().getName1(),
                    result.getContent().get(i).getName().getName1());

                Assert.assertEquals(memberRegisterDtos.get(i).getName().getName1(),
                    result.getContent().get(i).getName().getName1());
            }
        }

        // Check by Sort.Direction.DESC, MemberSort.NAME
        {
            Sort sort = Sort.by(Sort.Direction.DESC, MemberSort.NAME.getPropertyName());
            Page<MemberDto> result = getMembersBySort(sort);

            memberRegisterDtos.sort(Comparator.comparing(dto -> dto.getName().getName1()));
            Collections.reverse(memberRegisterDtos);

            logger.debug("Check get member by sort: Sort.Direction.DESC, MemberSort.NAME");
            for (int i = 0; i < memberRegisterDtos.size(); i++) {

                logger.debug("Check sort: target member name1 {}, result member name1 {}",
                    memberRegisterDtos.get(i).getName().getName1(),
                    result.getContent().get(i).getName().getName1());

                Assert.assertEquals(memberRegisterDtos.get(i).getName().getName1(),
                    result.getContent().get(i).getName().getName1());
            }
        }

    }

    private Page<MemberDto> getMembersBySort(Sort sort) {

        PageRequest pageRequest = PageRequest.of(0, 5, sort);
        Page<MemberDto> result = memberService.getMembers(pageRequest);
        // Member Count: 5, Page1
        // Expected page:0, page elements:5, total elements: 5
        Assert.assertEquals(0, result.getNumber());
        Assert.assertEquals(5, result.getNumberOfElements());
        Assert.assertEquals(5, result.getContent().size());
        Assert.assertEquals(5L, result.getTotalElements());

        return result;
    }

    @Test
    public void updateMember() {

        Long registeredMemberNo = registerMember();

        MemberUpdateDto updateDto = DummyBuilder.buildMemberUpdateDto(registeredMemberNo);
        memberService.updateMember(updateDto);

        MemberDto updatedMember = memberService.getMember(registeredMemberNo);

        DummyBuilder.assertMemberUpdateDto(updateDto, updatedMember);
    }

    @Test(expected = NotFoundDataException.class)
    public void updateMember_NotFoundDataException() {
        MemberRegisterDto registerDto = DummyBuilder.buildMemberRegisterDto();
        registerMember(registerDto);

        MemberUpdateDto updateDto = DummyBuilder.buildMemberUpdateDto(999L);
        memberService.updateMember(updateDto);
    }

    @Test
    public void changePassword() {

        final String newPassword = "NEW_PASSWORD";

        MemberRegisterDto registerDto = DummyBuilder.buildMemberRegisterDto();
        Long registeredMemberNo = registerMember(registerDto);

        memberService.changePassword(registeredMemberNo, newPassword);

        MemberDto updatedMember = memberService.getMember(registeredMemberNo);
        Assert.assertTrue("Password not matched.",
            bCryptPasswordEncoder.matches(newPassword, updatedMember.getPassword()));
    }

    @Test(expected = NotFoundDataException.class)
    public void changePassword_NotFoundDataException() {

        MemberRegisterDto registerDto = DummyBuilder.buildMemberRegisterDto();
        registerMember(registerDto);

        memberService.changePassword(999L, "NEW_PASSWORD");
    }

    @Test
    public void existMemberId() {
        Long registeredMemberNo = registerMember();
        MemberDto member = memberService.getMember(registeredMemberNo);
        Assert.assertNotNull(member);

        boolean existMemberId = memberService.existMemberId(member.getMemberId());
        Assert.assertTrue(existMemberId);
    }

}
