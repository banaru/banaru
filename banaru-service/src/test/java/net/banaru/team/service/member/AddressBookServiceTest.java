package net.banaru.team.service.member;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import net.banaru.team.service.constants.YN;
import net.banaru.team.service.dto.member.AddressBookDto;
import net.banaru.team.service.dto.member.AddressBookRegisterDto;
import net.banaru.team.test.utils.DummyBuilder;

@Transactional
public abstract class AddressBookServiceTest {

    @Autowired
    private MemberService memberService;

    @Autowired
    private AddressBookService addressBookService;

    @Test
    public void registerAddressBookTest() {
        registerAddressBook();
    }

    @Test
    public void registerAddressBookTest_repReverse() {
        AddressBookRegisterDto registerDto = DummyBuilder.buildAddressBookRegisterDto(registerMember());
        registerDto.setRepAddress(false);
        registerDto.setBillingAddress(true);
        registerAddressBook(registerDto);
    }

    /**
     * Register member and address-book.
     *
     * @return registeredAddressBookNo
     */
    private Long registerAddressBook() {
        AddressBookRegisterDto registerDto = DummyBuilder.buildAddressBookRegisterDto(registerMember());
        return registerAddressBook(registerDto);
    }

    private Long registerAddressBook(AddressBookRegisterDto target) {

        Long registeredAddressBookNo = addressBookService.registerAddressBook(target);

        AddressBookDto result = addressBookService.getAddressBook(registeredAddressBookNo);

        Assert.assertNotNull(result);

        // Check AddressBookEditDto's properties.
        Assert.assertEquals(target.getMemberNo(), result.getMemberNo());
        Assert.assertEquals(target.getAddressBookName(), result.getAddressBookName());

        Assert.assertEquals(target.getRepAddress(), result.getRepAddress() == YN.Y);
        Assert.assertEquals(target.getBillingAddress(), result.getBillingAddress() == YN.Y);

        Assert.assertEquals(target.getName().getName1(), result.getName().getName1());
        Assert.assertEquals(target.getName().getName2(), result.getName().getName2());
        Assert.assertEquals(target.getName().getName3(), result.getName().getName3());
        Assert.assertEquals(target.getName().getName4(), result.getName().getName4());

        Assert.assertEquals(target.getAddress().getAddress1(), result.getAddress().getAddress1());
        Assert.assertEquals(target.getAddress().getAddress2(), result.getAddress().getAddress2());
        Assert.assertEquals(target.getAddress().getAddress3(), result.getAddress().getAddress3());
        Assert.assertEquals(target.getAddress().getAddress4(), result.getAddress().getAddress4());
        Assert.assertEquals(target.getAddress().getZipCode(), result.getAddress().getZipCode());

        Assert.assertEquals(target.getCountryCode(), result.getCountryCode());
        Assert.assertEquals(target.getPhoneNo(), result.getPhoneNo());
        Assert.assertEquals(target.getMobileNo(), result.getMobileNo());

        return registeredAddressBookNo;
    }

    protected Long registerMember() {
        Long registeredMemberNo = memberService.registerMember(DummyBuilder.buildMemberRegisterDto());
        return memberService.getMember(registeredMemberNo).getMemberNo();
    }

    @Test
    public void getAddressBooksAndCountTest() {
        final int addressBookCount = 7;
        final Long memberNo = registerMember();

        // register
        List<AddressBookRegisterDto> registerDtos = DummyBuilder.buildAddressBookRegisterDtos(memberNo,
            addressBookCount);
        registerDtos.forEach(this::registerAddressBook);

        // check
        List<AddressBookDto> addressBooks = addressBookService.getAddressBooks(memberNo);
        Assert.assertFalse(CollectionUtils.isEmpty(addressBooks));
        Assert.assertEquals(addressBookCount, addressBooks.size());

        long count = addressBookService.getAddressBookCount(memberNo);
        Assert.assertEquals(addressBookCount, count);
    }

    @Test
    public void getAddressBooksByPageTest() {
        final int addressBookCount = 3;
        final Long memberNo = registerMember();

        List<AddressBookRegisterDto> registerDtos = DummyBuilder.buildAddressBookRegisterDtos(memberNo,
            addressBookCount);


        PageRequest pageRequest1 = PageRequest.of(0, 2);

        // AddressBook Count: 0
        // Expected page:0, page elements:0, total elements: 0
        Page<AddressBookDto> result1 = addressBookService.getAddressBooks(memberNo, pageRequest1);
        Assert.assertEquals(0, result1.getNumber());
        // page.getNumberOfElements() == page..etContent().size()
        Assert.assertEquals(0, result1.getNumberOfElements());
        Assert.assertEquals(0, result1.getContent().size());
        Assert.assertEquals(0L, result1.getTotalElements());

        // AddressBook Count: 1
        registerAddressBook(registerDtos.get(0));
        // Expected page:0, page elements:1, total elements: 1
        Page<AddressBookDto> result2 = addressBookService.getAddressBooks(memberNo, pageRequest1);
        Assert.assertEquals(0, result2.getNumber());
        Assert.assertEquals(1, result2.getNumberOfElements());
        Assert.assertEquals(1, result2.getContent().size());
        Assert.assertEquals(1L, result2.getTotalElements());

        // AddressBook Count: 2
        registerAddressBook(registerDtos.get(1));
        // Expected page:0, page elements:2, total elements: 2
        Page<AddressBookDto> result3 = addressBookService.getAddressBooks(memberNo, pageRequest1);
        Assert.assertEquals(0, result3.getNumber());
        Assert.assertEquals(2, result3.getNumberOfElements());
        Assert.assertEquals(2, result3.getContent().size());
        Assert.assertEquals(2L, result3.getTotalElements());

        // AddressBook Count: 3, Page1
        registerAddressBook(registerDtos.get(2));
        // Expected page:0, page elements:2, total elements: 3
        Page<AddressBookDto> result4 = addressBookService.getAddressBooks(memberNo, pageRequest1);
        Assert.assertEquals(0, result4.getNumber());
        Assert.assertEquals(2, result4.getNumberOfElements());
        Assert.assertEquals(2, result4.getContent().size());
        Assert.assertEquals(3L, result4.getTotalElements());

        // Member Count: 3, Page2
        PageRequest pageRequest2 = PageRequest.of(1, 2);
        // Expected page:1, page elements:1, total elements: 3
        Page<AddressBookDto> result5 = addressBookService.getAddressBooks(memberNo, pageRequest2);
        Assert.assertEquals(1, result5.getNumber());
        Assert.assertEquals(1, result5.getNumberOfElements());
        Assert.assertEquals(1, result5.getContent().size());
        Assert.assertEquals(3L, result5.getTotalElements());
    }

    @Test
    public void deleteAddressBookTest() {
        Long registeredAddressBookNo = registerAddressBook();
        addressBookService.deleteAddressBook(registeredAddressBookNo);

        AddressBookDto result = addressBookService.getAddressBook(registeredAddressBookNo);
        Assert.assertNull(result);
    }

}
