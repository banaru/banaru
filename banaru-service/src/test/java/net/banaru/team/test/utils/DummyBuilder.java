package net.banaru.team.test.utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import net.banaru.team.service.dto.AddressDto;
import net.banaru.team.service.dto.NameDto;
import net.banaru.team.service.dto.member.AddressBookRegisterDto;
import net.banaru.team.service.dto.member.MemberDto;
import net.banaru.team.service.dto.member.MemberRegisterDto;
import net.banaru.team.service.dto.member.MemberUpdateDto;
import net.banaru.team.service.enums.Gender;
import net.banaru.team.service.enums.MemberGrade;
import net.banaru.team.service.enums.MemberStatus;

/**
 * @author banssolin
 */
public final class DummyBuilder {

    //===============================
    // Member Values
    //===============================
    public final static String TEST_MEMBER_ID = "TEST_MEMBER_ID";
    public final static String TEST_MEMBER_PASSWORD = "TEST_MEMBER_PASSWORD";

    //===============================
    // Address Book Values
    //===============================
    public final static String TEST_ADDRESS_BOOK_NAME = "TEST_ADDRESS_BOOK_NAME";

    //===============================
    // Member DTOs
    //===============================
    public static MemberRegisterDto buildMemberRegisterDto() {

        MemberRegisterDto registerDto = new MemberRegisterDto();

        // Set MemberRegisterDto's properties(4 properties).
        registerDto.setMemberId(TEST_MEMBER_ID);
        registerDto.setPassword(TEST_MEMBER_PASSWORD);
        registerDto.setMemberGrade(MemberGrade.GENERAL);
        registerDto.setMemberStatus(MemberStatus.GENERAL);

        // Set MemberEditDto's properties(5 properties).
        registerDto.setMemberCountryCode("JP");
        registerDto.setEmail("member@register.com");
        registerDto.setBirthday(LocalDate.of(1999, 1, 1));
        registerDto.setGender(Gender.MALE);
        registerDto.setPhoneNo("111-2222-3333");

        // Set NameDto's properties(4 properties).
        registerDto.setName(buildNameDto());
        return registerDto;
    }

    public static List<MemberRegisterDto> buildMemberRegisterDtos(int count) {
        List<MemberRegisterDto> registerDtos = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            MemberRegisterDto buildMemberRegisterDto = buildMemberRegisterDto();
            buildMemberRegisterDto.setMemberId(buildMemberRegisterDto.getMemberId().concat(String.valueOf(i)));
            buildMemberRegisterDto.getName()
                .setName1(buildMemberRegisterDto.getName().getName1().concat("_").concat(String.valueOf(i)));
            registerDtos.add(buildMemberRegisterDto);
        }
        return registerDtos;
    }

    public static void assertMemberRegisterDto(BCryptPasswordEncoder bCryptPasswordEncoder,
                                               MemberRegisterDto target, MemberDto result) {

        Assert.assertNotNull("Registered member is null.", result);

        // Check MemberRegisterDto's properties(4 properties).
        Assert.assertEquals(target.getMemberId(), result.getMemberId());
        Assert.assertTrue("Password not matched.",
            bCryptPasswordEncoder.matches(target.getPassword(), result.getPassword()));
        if (target.getMemberGrade() == null) {
            // default value
            Assert.assertEquals(MemberGrade.GENERAL, result.getMemberGrade());
        } else {
            Assert.assertEquals(target.getMemberGrade(), result.getMemberGrade());
        }
        if (target.getMemberStatus() == null) {
            // default value
            Assert.assertEquals(MemberStatus.GENERAL, result.getMemberStatus());
        } else {
            Assert.assertEquals(target.getMemberStatus(), result.getMemberStatus());
        }

        // Check MemberEditDto's properties(5 properties).
        Assert.assertEquals(target.getMemberCountryCode(), result.getMemberCountryCode());
        Assert.assertEquals(target.getEmail(), result.getEmail());
        Assert.assertEquals(target.getBirthday(), result.getBirthday());
        Assert.assertEquals(target.getGender(), result.getGender());
        Assert.assertEquals(target.getPhoneNo(), result.getPhoneNo());

        // Check NameDto's properties(4 properties).
        Assert.assertEquals(target.getName().getName1(), result.getName().getName1());
        Assert.assertEquals(target.getName().getName2(), result.getName().getName2());
        Assert.assertEquals(target.getName().getName3(), result.getName().getName3());
        Assert.assertEquals(target.getName().getName4(), result.getName().getName4());
    }

    public static MemberUpdateDto buildMemberUpdateDto(Long memberNo) {

        MemberUpdateDto updateDto = new MemberUpdateDto();
        updateDto.setMemberNo(memberNo);
        updateDto.setMemberCountryCode("KR");
        NameDto nameDto;
        {
            nameDto = new NameDto();
            nameDto.setName1("UPDATE_NAME1");
            nameDto.setName2("UPDATE_NAME2");
            nameDto.setName3("UPDATE_NAME3");
            nameDto.setName4("UPDATE_NAME4");
        }
        updateDto.setName(nameDto);
        updateDto.setEmail("member@update.com");
        updateDto.setBirthday(LocalDate.of(2000, 12, 31));
        updateDto.setGender(Gender.FEMALE);
        updateDto.setPhoneNo("999-8888-7777");

        return updateDto;
    }

    public static void assertMemberUpdateDto(MemberUpdateDto target, MemberDto result) {

        Assert.assertNotNull(result);

        // Check MemberUpdateDto's properties(1 properties).
        Assert.assertEquals(target.getMemberNo(), result.getMemberNo());

        // Check MemberEditDto's properties(5 properties).
        Assert.assertEquals(target.getMemberCountryCode(), result.getMemberCountryCode());
        Assert.assertEquals(target.getEmail(), result.getEmail());
        Assert.assertEquals(target.getBirthday(), result.getBirthday());
        Assert.assertEquals(target.getGender(), result.getGender());
        Assert.assertEquals(target.getPhoneNo(), result.getPhoneNo());

        // Check NameDto's properties(4 properties).
        Assert.assertEquals(target.getName().getName1(), result.getName().getName1());
        Assert.assertEquals(target.getName().getName2(), result.getName().getName2());
        Assert.assertEquals(target.getName().getName3(), result.getName().getName3());
        Assert.assertEquals(target.getName().getName4(), result.getName().getName4());
    }

    //===============================
    // Address Book DTOs
    //===============================
    public static AddressBookRegisterDto buildAddressBookRegisterDto(Long memberNo) {

        AddressBookRegisterDto registerDto = new AddressBookRegisterDto();
        registerDto.setMemberNo(memberNo);
        registerDto.setAddressBookName(TEST_ADDRESS_BOOK_NAME);
        registerDto.setRepAddress(true);
        registerDto.setBillingAddress(false);
        registerDto.setName(buildNameDto());
        registerDto.setAddress(buildAddressDto());
        registerDto.setCountryCode("JP");
        registerDto.setPhoneNo("1112223333");
        registerDto.setMobileNo("44455556666");

        return registerDto;
    }

    public static List<AddressBookRegisterDto> buildAddressBookRegisterDtos(Long memberNo, int count) {
        List<AddressBookRegisterDto> registerDtos = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            AddressBookRegisterDto buildAddressBookRegisterDto = buildAddressBookRegisterDto(memberNo);
            buildAddressBookRegisterDto.setAddressBookName(buildAddressBookRegisterDto.getAddressBookName().concat(String.valueOf(i)));
            registerDtos.add(buildAddressBookRegisterDto);
        }
        return registerDtos;
    }

    //===============================
    // Name DTO
    //===============================
    public static NameDto buildNameDto() {
        NameDto nameDto = new NameDto();
        nameDto.setName1("TEST_NAME1");
        nameDto.setName2("TEST_NAME2");
        nameDto.setName3("TEST_NAME3");
        nameDto.setName4("TEST_NAME4");
        return nameDto;
    }

    //===============================
    // Address DTO
    //===============================
    private static AddressDto buildAddressDto() {
        AddressDto address = new AddressDto();
        address.setAddress1("TEST_ADDRESS1");
        address.setAddress2("TEST_ADDRESS2");
        address.setAddress3("TEST_ADDRESS3");
        address.setAddress4("TEST_ADDRESS4");
        address.setZipCode("TEST123-456");
        return address;
    }

}
