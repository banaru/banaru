package net.banaru.team.service;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import net.banaru.team.service.configuration.JpaConfiguration;
import net.banaru.team.web.configuration.mapper.JpaMapperConfiguration;

/**
 * @author banssolin
 */
@SpringBootApplication
@Import({JpaConfiguration.class, JpaMapperConfiguration.class})
public class JpaTestApplication {
}
