package net.banaru.team.service.member;

import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import net.banaru.team.service.JpaTestApplication;

/**
 * @author banssolin
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest(classes = {JpaTestApplication.class})
// To flush after each transaction
// @Transactional(propagation = Propagation.REQUIRES_NEW)
public class MemberJpaServiceTest extends MemberServiceTest {

}
