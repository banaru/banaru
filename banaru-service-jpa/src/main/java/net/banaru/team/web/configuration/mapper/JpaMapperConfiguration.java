package net.banaru.team.web.configuration.mapper;

import ma.glasnost.orika.MapperFactory;
import org.springframework.context.annotation.Configuration;

import net.banaru.team.service.dto.member.AddressBookDto;
import net.banaru.team.service.entity.AddressBookEntity;

@Configuration
public class JpaMapperConfiguration {

    public JpaMapperConfiguration(MapperFactory mapperFactory) {

        mapperFactory.classMap(AddressBookEntity.class, AddressBookDto.class)
            .fieldAToB("member.memberNo", "memberNo")
            .byDefault()
            .register();
    }

}
