
package net.banaru.team.service.entity;

import javax.persistence.Embeddable;

import net.banaru.team.service.dto.BaseDto;

/**
 * @author banssolin
 */
@Embeddable
public class AddressEntity implements BaseDto {

    private static final long serialVersionUID = 5914886891045608644L;

    private String address1;
    private String address2;
    private String address3;
    private String address4;

    private String zipCode;

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress4() {
        return address4;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
