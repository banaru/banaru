package net.banaru.team.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import ma.glasnost.orika.MapperFacade;
import net.banaru.team.service.dto.BaseDto;
import net.banaru.team.service.entity.BaseEntity;

import java.util.ArrayList;
import java.util.List;

public class BaseJpaService {

    protected <D extends BaseDto, E extends BaseEntity> Page<D> mapAsPage(MapperFacade mapperFacade,
                                                                          Page<E> page, Class<D> clazz) {

        List<D> dtos = new ArrayList<>();
        mapperFacade.mapAsCollection(page.getContent(), dtos, clazz);

        return new PageImpl<>(dtos, page.getPageable(), page.getTotalElements());
    }

}
