package net.banaru.team.service.member;

import java.util.List;
import java.util.Optional;
import javax.validation.constraints.NotNull;

import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.banaru.team.service.BaseJpaService;
import net.banaru.team.service.dto.member.AddressBookDto;
import net.banaru.team.service.dto.member.AddressBookRegisterDto;
import net.banaru.team.service.entity.AddressBookEntity;
import net.banaru.team.service.entity.MemberEntity;
import net.banaru.team.service.exception.runtime.NotFoundDataException;
import net.banaru.team.service.repository.member.AddressBookRepository;
import net.banaru.team.service.repository.member.MemberRepository;

/**
 * @author banssolin
 */
@Service
public class AddressBookJpaService extends BaseJpaService implements AddressBookService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final MapperFacade mapperFacade;

    private final MemberRepository memberRepository;
    private final AddressBookRepository addressBookRepository;

    public AddressBookJpaService(MapperFacade mapperFacade, MemberRepository memberRepository,
                                 AddressBookRepository addressBookRepository) {
        this.mapperFacade = mapperFacade;

        this.memberRepository = memberRepository;
        this.addressBookRepository = addressBookRepository;
    }

    @Override
    public Long registerAddressBook(AddressBookRegisterDto registerDto) {
        AddressBookEntity addressBookEntity;
        {
            addressBookEntity = mapperFacade.map(registerDto, AddressBookEntity.class);

            Optional<MemberEntity> result = memberRepository.findById(registerDto.getMemberNo());
            result.orElseThrow(() ->
                new NotFoundDataException("Not found member by member number: " + registerDto.getMemberNo()));

            addressBookEntity.setMember(result.get());
        }
        logger.debug("Register target: {}", addressBookEntity.toJson());
        return addressBookRepository.save(addressBookEntity).getAddressBookNo();
    }

    @Override
    public AddressBookDto getAddressBook(@NotNull Long addressBookNo) {
        Optional<AddressBookEntity> result = addressBookRepository.findById(addressBookNo);
        result.ifPresent(addressBook -> logger.debug("AddressBookEntity by addressBookNo: {}", addressBook.toJson()));
        return mapperFacade.map(result.orElse(null), AddressBookDto.class);
    }

    @Override
    public List<AddressBookDto> getAddressBooks(@NotNull Long memberNo) {
        return mapperFacade.mapAsList(addressBookRepository.findByMemberMemberNo(memberNo), AddressBookDto.class);
    }

    @Override
    public long getAddressBookCount(@NotNull Long memberNo) {
        return addressBookRepository.countByMemberMemberNo(memberNo);
    }

    @Override
    public Page<AddressBookDto> getAddressBooks(@NotNull Long memberNo, @NotNull Pageable pageable) {
        Page<AddressBookEntity> page = addressBookRepository.findByMemberMemberNo(memberNo, pageable);
        return mapAsPage(mapperFacade, page, AddressBookDto.class);
    }

    @Override
    public void deleteAddressBook(@NotNull Long addressBookNo) {
        addressBookRepository.deleteById(addressBookNo);
    }

}
