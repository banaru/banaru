package net.banaru.team.service.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import net.banaru.team.service.dto.BaseDto;

@MappedSuperclass
public class BaseEntity implements BaseDto {

    private static final long serialVersionUID = 1L;

    @Column(name = "register_date", nullable = false, updatable = false)
    private Date registerDate;

    @PrePersist
    public void preInsert() {
        Date now = new Date();
        registerDate = now;
        // updateDate = now;
        // registId = getUserId();
        // updateId = getUserId();
    }

    @PreUpdate
    public void preUpdate() {
        Date now = new Date();
        if (registerDate == null) {
            registerDate = now;
        }
        // updateDate = now;
        // if (registId == null) {
        //   registId = getUserId();
        // }
        // updateId = getUserId();
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

}
