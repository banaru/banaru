package net.banaru.team.service.repository.member.impl;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import net.banaru.team.service.entity.MemberEntity;
import net.banaru.team.service.repository.member.CustomizedMemberRepository;

import static net.banaru.team.service.entity.QMemberEntity.memberEntity;

// Repositories custom implementations: https://docs.spring.io/spring-data/jpa/docs/2.1.2.RELEASE/reference/html/#repositories.custom-implementations
// Postfix Imlp은 rule로 정해져, Bean등록을 자동으로 해준다.
public class CustomizedMemberRepositoryImpl extends QuerydslRepositorySupport implements CustomizedMemberRepository {

    public CustomizedMemberRepositoryImpl() {
        super(MemberEntity.class);
    }

    @Override
    public MemberEntity getMember(String memberId) {
        return from(memberEntity)
            .where(memberEntity.memberId.eq(memberId))
            .fetchOne();
    }

    @Override
    public boolean existMemberId(String memberId) {
        return !from(memberEntity)
            .where(memberEntity.memberId.eq(memberId))
            .fetchResults().isEmpty();
    }

}
