package net.banaru.team.service.repository.member;

import net.banaru.team.service.entity.MemberEntity;

public interface CustomizedMemberRepository {

    MemberEntity getMember(String memberId);

    boolean existMemberId(String memberId);

}
