package net.banaru.team.service.repository.member;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import net.banaru.team.service.entity.AddressBookEntity;

/**
 * @author banssolin
 */
public interface AddressBookRepository
    extends JpaRepository<AddressBookEntity, Long>, QuerydslPredicateExecutor<AddressBookEntity> {

    List<AddressBookEntity> findByMemberMemberNo(Long memberNo);

    long countByMemberMemberNo(Long memberNo);

    Page<AddressBookEntity> findByMemberMemberNo(Long memberNo, Pageable pageable);

}
