
package net.banaru.team.service.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import net.banaru.team.service.enums.Gender;
import net.banaru.team.service.enums.MemberGrade;
import net.banaru.team.service.enums.MemberStatus;

/**
 * @author banssolin
 */
@Entity
@Table(name = "member")
public class MemberEntity extends BaseEntity {

    private static final long serialVersionUID = -2626925485714445078L;

    @Id
    @SequenceGenerator(name = "memberNoSeq", sequenceName = "SEQ_member_member_no", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "memberNoSeq")
    @Column(name = "member_no")
    private Long memberNo;

    private String memberId;

    private String password;

    @Embedded
    private NameEntity name;

    private String email;

    @Enumerated(EnumType.STRING)
    private MemberGrade memberGrade;

    private LocalDate birthday;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private String phoneNo;

    private String mobileNo;

    @Enumerated(EnumType.STRING)
    private MemberStatus memberStatus;

    private String memberCountryCode;

    @OneToMany(cascade = {CascadeType.REMOVE}, fetch = FetchType.LAZY, mappedBy = "member", orphanRemoval = true)
    private List<AddressBookEntity> addressBooks = new ArrayList<>();

    public Long getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(Long memberNo) {
        this.memberNo = memberNo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public NameEntity getName() {
        return name;
    }

    public void setName(NameEntity name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public MemberGrade getMemberGrade() {
        return memberGrade;
    }

    public void setMemberGrade(MemberGrade memberGrade) {
        this.memberGrade = memberGrade;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public MemberStatus getMemberStatus() {
        return memberStatus;
    }

    public void setMemberStatus(MemberStatus memberStatus) {
        this.memberStatus = memberStatus;
    }

    public String getMemberCountryCode() {
        return memberCountryCode;
    }

    public void setMemberCountryCode(String memberCountryCode) {
        this.memberCountryCode = memberCountryCode;
    }

    public List<AddressBookEntity> getAddressBooks() {
        return addressBooks;
    }

    public void setAddressBooks(List<AddressBookEntity> addressBooks) {
        this.addressBooks = addressBooks;
    }

}
