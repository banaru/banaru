package net.banaru.team.service.member;

import java.util.Optional;

import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.banaru.team.service.BaseJpaService;
import net.banaru.team.service.dto.member.MemberDto;
import net.banaru.team.service.dto.member.MemberRegisterDto;
import net.banaru.team.service.dto.member.MemberUpdateDto;
import net.banaru.team.service.entity.MemberEntity;
import net.banaru.team.service.enums.MemberGrade;
import net.banaru.team.service.enums.MemberStatus;
import net.banaru.team.service.exception.runtime.DuplicatedDataException;
import net.banaru.team.service.exception.runtime.NotFoundDataException;
import net.banaru.team.service.repository.member.MemberRepository;

/**
 * @author banssolin
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MemberJpaService extends BaseJpaService implements MemberService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final MapperFacade mapperFacade;
    private final MemberRepository memberRepository;
    // from serviceConfiguration
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public MemberJpaService(MapperFacade mapperFacade, MemberRepository memberRepository,
                            BCryptPasswordEncoder bCryptPasswordEncoder) {

        this.mapperFacade = mapperFacade;
        this.memberRepository = memberRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public Long registerMember(MemberRegisterDto registerDto) {
        if (existMemberId(registerDto.getMemberId())) {
            throw new DuplicatedDataException("Duplicated Member id: " + registerDto.getMemberId());
        }
        MemberEntity memberEntity;
        {
            memberEntity = mapperFacade.map(registerDto, MemberEntity.class);
            memberEntity.setPassword(bCryptPasswordEncoder.encode(registerDto.getPassword()));

            setRegisterDefaultValue(memberEntity);
        }
        logger.debug("Register target: {}", memberEntity.toJson());
        return memberRepository.save(memberEntity).getMemberNo();
    }

    private void setRegisterDefaultValue(MemberEntity memberEntity) {
        if (memberEntity.getMemberGrade() == null) {
            memberEntity.setMemberGrade(MemberGrade.GENERAL);
        }
        // MemberStatus memberStatus = policyProperties.isMailAuth() ? MemberStatus.WAIT
        // : MemberStatus.GENERAL;
        if (memberEntity.getMemberStatus() == null) {
            memberEntity.setMemberStatus(MemberStatus.GENERAL);
        }
    }

    @Override
    public void updateMember(MemberUpdateDto updateDto) {
        MemberEntity memberEntity = getMemberOrElseThrow(updateDto.getMemberNo());
        mapperFacade.map(updateDto, memberEntity);

        memberRepository.save(memberEntity);
    }

    @Override
    public MemberDto getMember(Long memberNo) {
        Optional<MemberEntity> result = memberRepository.findById(memberNo);
        result.ifPresent(member -> logger.debug("MemberEntity by memberNo: {}", member.toJson()));
        return mapperFacade.map(result.orElse(null), MemberDto.class);
    }

    @Override
    public MemberDto getMember(String memberId) {
        return mapperFacade.map(memberRepository.getMember(memberId), MemberDto.class);
    }

    private MemberEntity getMemberOrElseThrow(Long memberNo) {
        Optional<MemberEntity> optional = memberRepository.findById(memberNo);
        return optional.orElseThrow(() ->
            new NotFoundDataException("Not found member by member number: " + memberNo));
    }

    @Override
    public Page<MemberDto> getMembers(Pageable pageable) {
        Page<MemberEntity> page = memberRepository.findAll(pageable);
        return mapAsPage(mapperFacade, page, MemberDto.class);
    }

    @Override
    public boolean existMemberId(String memberId) {
        return memberRepository.existMemberId(memberId);
    }

    @Override
    public void changePassword(Long memberNo, String password) {

        MemberEntity memberEntity = getMemberOrElseThrow(memberNo);
        memberEntity.setPassword(bCryptPasswordEncoder.encode(password));

        memberRepository.save(memberEntity);
    }

}
