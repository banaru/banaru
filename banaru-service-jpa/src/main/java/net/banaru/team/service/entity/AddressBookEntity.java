package net.banaru.team.service.entity;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import net.banaru.team.service.constants.YN;

/**
 * @author banssolin
 */
@Entity
@Table(name = "address_book")
public class AddressBookEntity extends BaseEntity {

    private static final long serialVersionUID = 6100459235038185700L;

    @Id
    @SequenceGenerator(name = "addressBookNoSeq", sequenceName = "SEQ_address_book_address_book_no", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "addressBookNoSeq")
    private Long addressBookNo;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "member_no", nullable = false)
    private MemberEntity member;

    @Column(length = 100)
    private String addressBookName;

    @Enumerated(EnumType.STRING)
    private YN repAddress;

    @Enumerated(EnumType.STRING)
    private YN billingAddress;

    @Embedded
    private NameEntity name;

    @Embedded
    private AddressEntity address;

    @Column(length = 30)
    private String countryCode;

    @Column(length = 20)
    private String phoneNo;

    @Column(length = 20)
    private String mobileNo;

    public Long getAddressBookNo() {
        return addressBookNo;
    }

    public void setAddressBookNo(Long addressBookNo) {
        this.addressBookNo = addressBookNo;
    }

    public MemberEntity getMember() {
        return member;
    }

    public void setMember(MemberEntity member) {
        this.member = member;
    }

    public String getAddressBookName() {
        return addressBookName;
    }

    public void setAddressBookName(String addressBookName) {
        this.addressBookName = addressBookName;
    }

    public YN getRepAddress() {
        return repAddress;
    }

    public void setRepAddress(YN repAddress) {
        this.repAddress = repAddress;
    }

    public YN getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(YN billingAddress) {
        this.billingAddress = billingAddress;
    }

    public NameEntity getName() {
        return name;
    }

    public void setName(NameEntity name) {
        this.name = name;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setAddress(AddressEntity address) {
        this.address = address;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

}
