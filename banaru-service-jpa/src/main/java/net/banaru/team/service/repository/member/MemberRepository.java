package net.banaru.team.service.repository.member;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import net.banaru.team.service.entity.MemberEntity;

/**
 * @author banssolin
 */
public interface MemberRepository
    extends JpaRepository<MemberEntity, Long>, QuerydslPredicateExecutor<MemberEntity>, CustomizedMemberRepository {

}
