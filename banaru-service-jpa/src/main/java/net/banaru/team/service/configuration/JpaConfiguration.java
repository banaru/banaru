package net.banaru.team.service.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author banssolin
 */
@Configuration
@ComponentScan("net.banaru.team.service")
@EntityScan("net.banaru.team.service.entity")
@EnableJpaRepositories("net.banaru.team.service.repository")
// Default Configuration on Spring Data JPA
// @EnableTransactionManagement
@Import({ServiceConfiguration.class})
public class JpaConfiguration {

}
