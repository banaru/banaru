package net.banaru.team.service.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QNameEntity is a Querydsl query type for NameEntity
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QNameEntity extends BeanPath<NameEntity> {

    private static final long serialVersionUID = -1122288745L;

    public static final QNameEntity nameEntity = new QNameEntity("nameEntity");

    public final StringPath name1 = createString("name1");

    public final StringPath name2 = createString("name2");

    public final StringPath name3 = createString("name3");

    public final StringPath name4 = createString("name4");

    public QNameEntity(String variable) {
        super(NameEntity.class, forVariable(variable));
    }

    public QNameEntity(Path<? extends NameEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QNameEntity(PathMetadata metadata) {
        super(NameEntity.class, metadata);
    }

}

