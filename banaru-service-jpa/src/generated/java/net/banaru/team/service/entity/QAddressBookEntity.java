package net.banaru.team.service.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QAddressBookEntity is a Querydsl query type for AddressBookEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAddressBookEntity extends EntityPathBase<AddressBookEntity> {

    private static final long serialVersionUID = 875525591L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QAddressBookEntity addressBookEntity = new QAddressBookEntity("addressBookEntity");

    public final QBaseEntity _super = new QBaseEntity(this);

    public final QAddressEntity address;

    public final StringPath addressBookName = createString("addressBookName");

    public final NumberPath<Long> addressBookNo = createNumber("addressBookNo", Long.class);

    public final EnumPath<net.banaru.team.service.constants.YN> billingAddress = createEnum("billingAddress", net.banaru.team.service.constants.YN.class);

    public final StringPath countryCode = createString("countryCode");

    public final QMemberEntity member;

    public final StringPath mobileNo = createString("mobileNo");

    public final QNameEntity name;

    public final StringPath phoneNo = createString("phoneNo");

    //inherited
    public final DateTimePath<java.util.Date> registerDate = _super.registerDate;

    public final EnumPath<net.banaru.team.service.constants.YN> repAddress = createEnum("repAddress", net.banaru.team.service.constants.YN.class);

    public QAddressBookEntity(String variable) {
        this(AddressBookEntity.class, forVariable(variable), INITS);
    }

    public QAddressBookEntity(Path<? extends AddressBookEntity> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QAddressBookEntity(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QAddressBookEntity(PathMetadata metadata, PathInits inits) {
        this(AddressBookEntity.class, metadata, inits);
    }

    public QAddressBookEntity(Class<? extends AddressBookEntity> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.address = inits.isInitialized("address") ? new QAddressEntity(forProperty("address")) : null;
        this.member = inits.isInitialized("member") ? new QMemberEntity(forProperty("member"), inits.get("member")) : null;
        this.name = inits.isInitialized("name") ? new QNameEntity(forProperty("name")) : null;
    }

}

