package net.banaru.team.service.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QMemberEntity is a Querydsl query type for MemberEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMemberEntity extends EntityPathBase<MemberEntity> {

    private static final long serialVersionUID = -232810970L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QMemberEntity memberEntity = new QMemberEntity("memberEntity");

    public final QBaseEntity _super = new QBaseEntity(this);

    public final ListPath<AddressBookEntity, QAddressBookEntity> addressBooks = this.<AddressBookEntity, QAddressBookEntity>createList("addressBooks", AddressBookEntity.class, QAddressBookEntity.class, PathInits.DIRECT2);

    public final DatePath<java.time.LocalDate> birthday = createDate("birthday", java.time.LocalDate.class);

    public final StringPath email = createString("email");

    public final EnumPath<net.banaru.team.service.enums.Gender> gender = createEnum("gender", net.banaru.team.service.enums.Gender.class);

    public final StringPath memberCountryCode = createString("memberCountryCode");

    public final EnumPath<net.banaru.team.service.enums.MemberGrade> memberGrade = createEnum("memberGrade", net.banaru.team.service.enums.MemberGrade.class);

    public final StringPath memberId = createString("memberId");

    public final NumberPath<Long> memberNo = createNumber("memberNo", Long.class);

    public final EnumPath<net.banaru.team.service.enums.MemberStatus> memberStatus = createEnum("memberStatus", net.banaru.team.service.enums.MemberStatus.class);

    public final StringPath mobileNo = createString("mobileNo");

    public final QNameEntity name;

    public final StringPath password = createString("password");

    public final StringPath phoneNo = createString("phoneNo");

    //inherited
    public final DateTimePath<java.util.Date> registerDate = _super.registerDate;

    public QMemberEntity(String variable) {
        this(MemberEntity.class, forVariable(variable), INITS);
    }

    public QMemberEntity(Path<? extends MemberEntity> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QMemberEntity(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QMemberEntity(PathMetadata metadata, PathInits inits) {
        this(MemberEntity.class, metadata, inits);
    }

    public QMemberEntity(Class<? extends MemberEntity> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.name = inits.isInitialized("name") ? new QNameEntity(forProperty("name")) : null;
    }

}

