package net.banaru.team.web.controller.member;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import net.banaru.team.web.RestApplication;
import net.banaru.team.web.controller.AbstractRestControllerTest;
import net.banaru.team.service.constants.Path;
import net.banaru.team.service.dto.member.MemberDto;
import net.banaru.team.service.dto.member.MemberRegisterDto;
import net.banaru.team.service.dto.member.MemberUpdateDto;
import net.banaru.team.test.utils.DummyBuilder;
import net.banaru.team.web.rest.connector.RestConnector;

/**
 * @author banssolin
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
    RestApplication.class
}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
// for rollback
@Transactional
public class MemberRestControllerTest extends AbstractRestControllerTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    // TODO
    // @Test
    public void registerMemberTest() {
        registerMember();
    }

    private Long registerMember() {
        final MemberRegisterDto target = DummyBuilder.buildMemberRegisterDto();
        final Long registeredMemberNo = RestConnector
            .post(testRestTemplate.getRestTemplate(), getLocalRestURL(Path.API_MEMBER_REGISTER), Long.class)
            .addTarget(target)
            .execute();
        logger.debug("Registered member number: {}", registeredMemberNo);

        MemberDto result = getMemberDto(registeredMemberNo);
        DummyBuilder.assertMemberRegisterDto(bCryptPasswordEncoder, target, result);

        return registeredMemberNo;
    }

    private MemberDto getMemberDto(Long memberNo) {
        return RestConnector
            .get(testRestTemplate.getRestTemplate(), getLocalRestURL(Path.API_MEMBER_GET), MemberDto.class)
            .addParam("memberNo", memberNo)
            .execute();
    }

    // @Test
    public void updateMemberTest() {
        Long registeredMemberNo = registerMember();

        MemberUpdateDto target = DummyBuilder.buildMemberUpdateDto(registeredMemberNo);
        RestConnector
            .post(testRestTemplate.getRestTemplate(), getLocalRestURL(Path.API_MEMBER_UPDATE), null)
            .addTarget(target)
            .execute();

        MemberDto result = getMemberDto(registeredMemberNo);
        DummyBuilder.assertMemberUpdateDto(target, result);
    }

}
