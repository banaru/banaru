package net.banaru.team.web.controller;

import org.springframework.boot.web.server.LocalServerPort;

/**
 * @author banssolin
 */
public class AbstractRestControllerTest {

    private static final String LOCAL_HOST_URL = "http://localhost";
    private static final String URL_SEPARATE_SYMBOL = "/";

    //@Value("${local.server.port}")
    @LocalServerPort // just a shortcut for upper line
    private int port;

    protected String getLocalRestURL(String path) {
        if (!path.startsWith(URL_SEPARATE_SYMBOL)) {
            path = URL_SEPARATE_SYMBOL.concat(path);
        }
        return getLocalHostURL().concat(path);
    }

    protected String getLocalHostURL() {
        return LOCAL_HOST_URL.concat(":").concat(String.valueOf(port));
    }

}
