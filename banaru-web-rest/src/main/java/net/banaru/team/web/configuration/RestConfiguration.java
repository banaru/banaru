package net.banaru.team.web.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import net.banaru.team.service.configuration.MybatisConfiguration;

/**
 * @author banssolin
 */
@Configuration
@Import({
        // JpaConfiguration.class,
        MybatisConfiguration.class
})
public class RestConfiguration {

}
