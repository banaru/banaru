package net.banaru.team.web.controller.member;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.banaru.team.service.constants.Path;
import net.banaru.team.service.dto.member.MemberDto;
import net.banaru.team.service.dto.member.MemberRegisterDto;
import net.banaru.team.service.dto.member.MemberUpdateDto;
import net.banaru.team.service.member.MemberService;

/**
 * @author banssolin
 */
@RestController
public class MemberRestController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MemberService memberService;

    @PostMapping(Path.API_MEMBER_REGISTER)
    public Long registerMember(@RequestBody MemberRegisterDto member) {
        logger.debug("Register Member RequestBody: {}", member.toJson());
        return memberService.registerMember(member);
    }

    @PostMapping(Path.API_MEMBER_UPDATE)
    public void updateMember(@RequestBody MemberUpdateDto member) {
        logger.debug("Update Member RequestBody: {}", member.toJson());
        memberService.updateMember(member);
    }

    @GetMapping(Path.API_MEMBER_GET)
    public MemberDto getMember(@RequestParam Long memberNo) {
        logger.debug("Get Member RequestParam: {}", memberNo);
        return memberService.getMember(memberNo);
    }

}
