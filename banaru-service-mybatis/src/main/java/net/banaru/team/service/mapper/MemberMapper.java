package net.banaru.team.service.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.banaru.team.service.dto.member.MemberDto;
import net.banaru.team.service.dto.member.MemberUpdateDto;

/**
 * @author banssolin
 */
@Mapper
public interface MemberMapper {

    void registerMember(MemberDto member);

    void updateMember(MemberUpdateDto member);

    MemberDto getMemberByMemberNo(Long memberNo);

    MemberDto getMemberByMemberId(String memberId);

    long getMemberCount();

    List<MemberDto> getMembers(@Param("offset") Long offset,
                               @Param("limit") int limit,
                               @Param("orderMap") Map<String, String> orderMap);

    boolean existMemberId(String memberId);

    void changePassword(@Param("memberNo") Long memberNo,
                        @Param("password") String password);

}
