package net.banaru.team.service.member;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.banaru.team.service.constants.MemberSort;
import net.banaru.team.service.dto.member.MemberDto;
import net.banaru.team.service.dto.member.MemberRegisterDto;
import net.banaru.team.service.dto.member.MemberUpdateDto;
import net.banaru.team.service.enums.MemberGrade;
import net.banaru.team.service.enums.MemberStatus;
import net.banaru.team.service.exception.runtime.DuplicatedDataException;
import net.banaru.team.service.exception.runtime.NotFoundDataException;
import net.banaru.team.service.mapper.MemberMapper;

/**
 * @author banssolin
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MemberMybatisService implements MemberService {

    private final MemberMapper mapper;

    // from serviceConfiguration
    private final BCryptPasswordEncoder passwordEncoder;

    public MemberMybatisService(MemberMapper mapper, BCryptPasswordEncoder passwordEncoder) {
        this.mapper = mapper;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Long registerMember(MemberRegisterDto registerDto) {
        if (existMemberId(registerDto.getMemberId())) {
            throw new DuplicatedDataException("Duplicated Member id: " + registerDto.getMemberId());
        }
        MemberDto member;
        {
            member = new MemberDto();

            // Set MemberRegisterDto's properties(4 properties).
            member.setMemberId(registerDto.getMemberId());
            member.setPassword(passwordEncoder.encode(registerDto.getPassword()));
            member.setMemberGrade(registerDto.getMemberGrade());
            member.setMemberStatus(registerDto.getMemberStatus());

            // Set MemberEditDto's properties(5 properties).
            member.setMemberCountryCode(registerDto.getMemberCountryCode());
            member.setEmail(registerDto.getEmail());
            member.setBirthday(registerDto.getBirthday());
            member.setGender(registerDto.getGender());
            member.setPhoneNo(registerDto.getPhoneNo());

            // Set NameDto's properties(4 properties).
            member.setName(registerDto.getName());

            setRegisterDefaultValue(member);
        }

        mapper.registerMember(member);
        return member.getMemberNo();
    }

    private void setRegisterDefaultValue(MemberDto member) {
        member.setRegisterDate(new Date());

        if (member.getMemberGrade() == null) {
            member.setMemberGrade(MemberGrade.GENERAL);
        }
        // MemberStatus memberStatus = policyProperties.isMailAuth() ? MemberStatus.WAIT
        // : MemberStatus.GENERAL;
        if (member.getMemberStatus() == null) {
            member.setMemberStatus(MemberStatus.GENERAL);
        }
    }

    @Override
    public void updateMember(MemberUpdateDto updateDto) {
        getMemberOrElseThrow(updateDto.getMemberNo());
        mapper.updateMember(updateDto);
    }

    private MemberDto getMemberOrElseThrow(Long memberNo) {
        MemberDto memberDto = mapper.getMemberByMemberNo(memberNo);
        if (memberDto == null) {
            throw new NotFoundDataException("Not found member by member number: " + memberNo);
        }
        return memberDto;
    }

    @Override
    public MemberDto getMember(Long memberNo) {
        return mapper.getMemberByMemberNo(memberNo);
    }

    @Override
    public MemberDto getMember(String memberId) {
        return mapper.getMemberByMemberId(memberId);
    }

    @Override
    public Page<MemberDto> getMembers(Pageable pageable) {

        Map<String, String> orderMap = new HashMap<>();
        for (Sort.Order order : pageable.getSort()) {
            MemberSort memberSort = MemberSort.valueOfByPropertyName(order.getProperty());
            orderMap.put(memberSort.getColumnName(), order.getDirection().name());
        }

        List<MemberDto> members = mapper.getMembers(pageable.getOffset(), pageable.getPageSize(), orderMap);
        long memberCount = mapper.getMemberCount();

        return new PageImpl<>(members, pageable, memberCount);
    }

    @Override
    public boolean existMemberId(String memberId) {
        return mapper.existMemberId(memberId);
    }

    @Override
    public void changePassword(Long memberNo, String password) {
        getMemberOrElseThrow(memberNo);
        String encodedPassword = passwordEncoder.encode(password);
        mapper.changePassword(memberNo, encodedPassword);
    }

}
