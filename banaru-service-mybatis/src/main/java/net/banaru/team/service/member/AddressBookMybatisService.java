package net.banaru.team.service.member;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.banaru.team.service.constants.YN;
import net.banaru.team.service.dto.member.AddressBookDto;
import net.banaru.team.service.dto.member.AddressBookRegisterDto;
import net.banaru.team.service.mapper.AddressBookMapper;

/**
 * @author banssolin
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AddressBookMybatisService implements AddressBookService {

    private final AddressBookMapper addressBookMapper;

    public AddressBookMybatisService(AddressBookMapper addressBookMapper) {
        this.addressBookMapper = addressBookMapper;
    }

    @Override
    public Long registerAddressBook(AddressBookRegisterDto registerDto) {
        AddressBookDto addressBook;
        {
            addressBook = new AddressBookDto();
            addressBook.setMemberNo(registerDto.getMemberNo());
            addressBook.setAddressBookName(registerDto.getAddressBookName());
            addressBook.setRepAddress(registerDto.getRepAddress() ? YN.Y : YN.N);
            addressBook.setBillingAddress(registerDto.getBillingAddress() ? YN.Y : YN.N);
            addressBook.setName(registerDto.getName());
            addressBook.setAddress(registerDto.getAddress());
            addressBook.setCountryCode(registerDto.getCountryCode());
            addressBook.setPhoneNo(registerDto.getPhoneNo());
            addressBook.setMobileNo(registerDto.getMobileNo());

            // TODO
            addressBook.setRegisterDate(new Date());
        }
        addressBookMapper.registerAddressBook(addressBook);
        return addressBook.getAddressBookNo();
    }

    @Override
    public AddressBookDto getAddressBook(Long addressBookNo) {
        return addressBookMapper.getAddressBook(addressBookNo);
    }

    @Override
    public List<AddressBookDto> getAddressBooks(Long memberNo) {
        return addressBookMapper.getAddressBooks(memberNo);
    }

    @Override
    public long getAddressBookCount(Long memberNo) {
        return addressBookMapper.getAddressBookCount(memberNo);
    }

    @Override
    public Page<AddressBookDto> getAddressBooks(Long memberNo, Pageable pageable) {
        long totalCount = addressBookMapper.getAddressBookCount(memberNo);
        List<AddressBookDto> addressBooks = addressBookMapper.getAddressBooksByPage(memberNo, pageable.getOffset(),
                pageable.getPageSize());

        return new PageImpl<>(addressBooks, pageable, totalCount);
    }

    @Override
    public void deleteAddressBook(Long addressBookNo) {
        addressBookMapper.deleteAddressBook(addressBookNo);
    }

}
