package net.banaru.team.service.configuration;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author banssolin
 */
@Configuration
@ComponentScan("net.banaru.team.service")
// 상위, 하위에 빈 패키지가 있으면 FileNotFoundException 발생, 예를 들면 mapper.member에 mapper
// class를 넣었을 경우, 상위 패키지인
// mapper패키지에서 스캔을 하다 exception이 발생하게 된다.
// mapper패키지에 모든 mapper를 몰아 넣는게 답일듯
@MapperScan(basePackages = "net.banaru.team.service.mapper")
@Import({
    ServiceConfiguration.class
})
// DataSourceTransactionManager Bean 을 Transaction Manager 로 사용
@EnableTransactionManagement
public class MybatisConfiguration {

    @Bean
    public SqlSessionFactory sqlSessionFactory(ApplicationContext applicationContext, DataSource dataSource)
        throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setMapperLocations(applicationContext.getResources("classpath*:/mapper/**/*"));
        factoryBean.setTypeAliasesPackage("net.banaru.team.service.dto");

        SqlSessionFactory factory = factoryBean.getObject();
        factory.getConfiguration().setMapUnderscoreToCamelCase(true);
        return factory;
    }

    @Bean
    public DataSourceTransactionManager transactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

}
