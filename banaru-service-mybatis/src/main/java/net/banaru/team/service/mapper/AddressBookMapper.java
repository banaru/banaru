package net.banaru.team.service.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.banaru.team.service.dto.member.AddressBookDto;

/**
 * @author banssolin
 */
@Mapper
public interface AddressBookMapper {

    void registerAddressBook(AddressBookDto addressBookDto);

    AddressBookDto getAddressBook(Long addressBookNo);

    List<AddressBookDto> getAddressBooks(Long memberNo);

    long getAddressBookCount(Long memberNo);

    List<AddressBookDto> getAddressBooksByPage(@Param("memberNo") Long memberNo, @Param("offset") Long offset,
            @Param("limit") int limit);

    void deleteAddressBook(Long addressBookNo);

}
