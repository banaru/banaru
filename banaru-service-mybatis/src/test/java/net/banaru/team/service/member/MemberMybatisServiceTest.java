package net.banaru.team.service.member;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import net.banaru.team.service.configuration.MybatisConfiguration;
import net.banaru.team.service.configuration.MybatisTestConfiguration;

/**
 * @author banssolin
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {MybatisTestConfiguration.class, MybatisConfiguration.class})
// for rollback
@Transactional
public class MemberMybatisServiceTest extends MemberServiceTest {

}
